package ca

import (
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/util"
	"gopkg.in/yaml.v2"
	"os"
	"path/filepath"
)

const (
	DefaultConfigFileName     = "ca.conf"
	DefaultCertFileName       = "ca.pem"
	DefaultAuditLogFileName   = "audit.log"
	DefaultAuditLogKeepCopies = 5
	DefaultDriverName         = "file"

	AuditLogRotationSuffix = ".%d"
)

var ErrNoDriver = errors.New("driver not found")

type Config struct {
	loadPath, relDir string

	Log struct {
		Path string
		/*
			KeepCopies is the number of past audit logs to keep for the CA.
			Use an integer value here, 0 for the default number, or -1 to not
			keep any copies.
		*/
		KeepCopies int `yaml:"keep_copies"`
	} `yaml:"log"`
	Certificate struct {
		Path string `yaml:"path"`
	} `yaml:"certificate"`
	Driver       string              `yaml:"driver"`
	DriverConfig UntypedDriverConfig `yaml:"driver_config"`
}

type UntypedDriverConfig struct {
	unmarshal func(interface{}) error
}

func (dc *UntypedDriverConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	dc.unmarshal = unmarshal
	return nil
}

func (dc *UntypedDriverConfig) UnmarshalDriverConfig(v interface{}) error {
	if dc.unmarshal == nil {
		return nil
	}
	return dc.unmarshal(v)
}

func loadConfig(path string) (*Config, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}

	loadedFromDir := stat.IsDir()

	path, err = filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	var relDir string
	if loadedFromDir {
		relDir = path
		path = filepath.Join(path, DefaultConfigFileName)
	} else {
		relDir = filepath.Dir(path)
	}

	config := &Config{
		relDir:   relDir,
		loadPath: path,
	}

	f, err := os.Open(path)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return config, nil
		}
		return nil, err
	}

	defer util.DeferClose(f.Close, &err)
	err = yaml.NewDecoder(f).Decode(config)
	if err != nil {
		return nil, fmt.Errorf("parsing CA config: %w", err)
	}

	return config, nil
}

func (c *Config) CertificateFilePath() string {
	return c.RelConfigPath(c.Certificate.Path, DefaultCertFileName)
}

func (c *Config) LogFilePath() string {
	return c.RelConfigPath(c.Log.Path, DefaultAuditLogFileName)
}

func (c *Config) LogFilesToKeep() int {
	n := c.Log.KeepCopies
	if n == 0 {
		return DefaultAuditLogKeepCopies
	}

	if n < 0 {
		return 0
	}

	return n
}

func (c *Config) DriverName() string {
	if c.Driver != "" {
		return c.Driver
	}

	return DefaultDriverName
}

func (c *Config) MakeDriver() (Driver, error) {
	dn := c.DriverName()
	df := GetDriverFactory(dn)
	if df == nil {
		return nil, fmt.Errorf("%q: %w", dn, ErrNoDriver)
	}

	dc := df.NewDriverConfig()
	if err := c.DriverConfig.UnmarshalDriverConfig(dc); err != nil {
		return nil, fmt.Errorf("parsing driver config: %w", err)
	}

	d, err := df.NewDriver(c, dc)
	if err != nil {
		return nil, err
	}

	return d, nil
}

func (c *Config) RelConfigPath(configValue, defaultValue string) string {
	value := configValue
	if value == "" {
		value = defaultValue
	}

	if filepath.IsAbs(value) {
		return value
	}

	return filepath.Join(c.relDir, value)
}
