package ca

import (
	"bytes"
	"context"
	"crypto"
	"crypto/ed25519"
	"encoding/pem"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/siglog"
	"gitlab.com/xianic/xca/util"
	"io"
	"os"
)

var (
	LogMessageTypeRoll  = siglog.MustMessageType("roll")
	LogMessageTypeIssue = siglog.MustMessageType("issue")
	LogMessageTypeNote  = siglog.MustMessageType("note")
)

func LogConfigForKey(key crypto.PublicKey) *siglog.LogStreamConfig {
	config := &siglog.LogStreamConfig{
		SequenceHash: crypto.SHA512_256,
	}

	if _, isEd25519 := key.(ed25519.PublicKey); isEd25519 {
		config.SignatureHash = crypto.Hash(0)
	} else {
		config.SignatureHash = crypto.SHA512
	}

	return config
}

func (c *CA) logOpen(ctx context.Context, auditor siglog.MessageChecker, allowCreate bool) (err error) {
	if err = c.loadCertificate(); err != nil {
		return
	}

	config := LogConfigForKey(c.certificate.PublicKey)

	c.log, err = siglog.NewLogStream(ctx, config, c.Config.LogFilePath(), allowCreate)
	if err != nil {
		return
	}

	switch state := c.log.State(); state {
	case siglog.NeedInit:
		if !allowCreate {
			return fmt.Errorf("audit log needs to be initialised")
		}
		if err = c.logWriteInit(); err != nil {
			return err
		}

		return nil
	case siglog.NeetVerification:
		break
	default:
		return fmt.Errorf("audit log in unknown state")
	}

	caValidator := &LogAuditor{}
	auditors := siglog.NewMultiMessageChecker(caValidator, auditor)
	if err = c.log.Verify(c.certificate.PublicKey, auditors); err != nil {
		return err
	}

	if err = caValidator.InitCA(c); err != nil {
		return err
	}

	return nil
}

func (c *CA) logGenRollMessage() ([]byte, error) {
	message := &bytes.Buffer{}
	pem.Encode(
		message,
		&pem.Block{
			Type:  cryptutil.PemTypeCertificate,
			Bytes: c.certificate.Raw,
		},
	)

	for _, pc := range c.pastCerts {
		message.WriteString(fmt.Sprintf("%s\n", pc.String()))
	}

	return message.Bytes(), nil
}

func (c *CA) logWriteInit() error {
	if err := c.loadSigner(); err != nil {
		return err
	}

	serial := c.certificate.SerialNumber.Bytes()
	message, err := c.logGenRollMessage()
	if err != nil {
		return err
	}

	if err := c.log.Init(LogMessageTypeRoll, message, serial); err != nil {
		return fmt.Errorf("initialising CA audit log: %w", err)
	}

	return nil
}

func (c *CA) LogRotate(oldLog io.Writer) (err error) {
	if err = c.loadSigner(); err != nil {
		return err
	}

	message, err := c.logGenRollMessage()
	if err != nil {
		return err
	}

	keepNum := c.Config.LogFilesToKeep()
	if keepNum == 0 {
		return c.log.Rotate(LogMessageTypeRoll, message, oldLog)
	}

	baseLogName := c.Config.LogFilePath()
	for i := keepNum; i > 1; i-- {
		fileName := baseLogName + fmt.Sprintf(AuditLogRotationSuffix, i)
		if i == keepNum {
			err = os.Remove(fileName)
			if err != nil && !errors.Is(err, os.ErrNotExist) {
				err = fmt.Errorf("deleting oldest log file %s: %w", fileName, err)
				return
			}
		}

		previousFileName := baseLogName + fmt.Sprintf(AuditLogRotationSuffix, i-1)
		err = os.Rename(previousFileName, fileName)
		if err != nil && !errors.Is(err, os.ErrNotExist) {
			err = fmt.Errorf("moving log files %s → %s: %w", previousFileName, fileName, err)
			return
		}
	}

	archiveName := baseLogName + fmt.Sprintf(AuditLogRotationSuffix, 1)
	var archive *os.File
	archive, err = os.OpenFile(archiveName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return
	}
	defer util.DeferClose(archive.Close, &err)

	if err = c.log.Rotate(LogMessageTypeRoll, message, archive); err != nil {
		err = fmt.Errorf("rotating audit log to %s: %w", archiveName, err)
		return
	}

	if oldLog != nil {
		if _, err = archive.Seek(0, 0); err != nil {
			return
		}

		if _, err = io.Copy(oldLog, archive); err != nil {
			return
		}
	}

	return
}

func (c *CA) logWriteMessage(mt siglog.MessageType, m []byte) error {
	if err := c.loadSigner(); err != nil {
		return err
	}

	if err := c.log.Append(mt, m); err != nil {
		return fmt.Errorf("writing to CA audit log: %w", err)
	}

	return nil
}

func (c *CA) logWriteIssuance(csrBytes, certBytes []byte) error {
	message := &bytes.Buffer{}
	pem.Encode(
		message,
		&pem.Block{
			Type:  cryptutil.PemTypeCertificate,
			Bytes: certBytes,
		},
	)
	pem.Encode(
		message,
		&pem.Block{
			Type:  cryptutil.PemTypeCsr,
			Bytes: csrBytes,
		},
	)

	return c.logWriteMessage(LogMessageTypeIssue, message.Bytes())
}

func (c *CA) LogNote(note []byte) error {
	return c.logWriteMessage(LogMessageTypeNote, note)
}
