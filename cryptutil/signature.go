package cryptutil

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/x509"
)

const (
	DefaultSignatureHash = crypto.SHA512
)

var (
	rsaSignatureAlgorithmTypes = map[crypto.Hash]x509.SignatureAlgorithm{
		crypto.MD5:    x509.MD5WithRSA,
		crypto.SHA1:   x509.SHA1WithRSA,
		crypto.SHA256: x509.SHA256WithRSA,
		crypto.SHA384: x509.SHA384WithRSA,
		crypto.SHA512: x509.SHA512WithRSA,
	}

	ecdsaSignatureAlgorithmTypes = map[crypto.Hash]x509.SignatureAlgorithm{
		crypto.SHA1:   x509.ECDSAWithSHA1,
		crypto.SHA256: x509.ECDSAWithSHA256,
		crypto.SHA384: x509.ECDSAWithSHA384,
		crypto.SHA512: x509.ECDSAWithSHA512,
	}
)

/*
SignatureAlgorithmName returns a textual name for a signature algorithm type
*/
func SignatureAlgorithmName(sa x509.SignatureAlgorithm) string {
	switch sa {
	case x509.MD2WithRSA:
		return "MD2WithRSA"
	case x509.MD5WithRSA:
		return "MD5WithRSA"
	case x509.SHA1WithRSA:
		return "SHA1WithRSA"
	case x509.SHA256WithRSA:
		return "SHA256WithRSA"
	case x509.SHA384WithRSA:
		return "SHA384WithRSA"
	case x509.SHA512WithRSA:
		return "SHA512WithRSA"
	case x509.DSAWithSHA1:
		return "DSAWithSHA1"
	case x509.DSAWithSHA256:
		return "DSAWithSHA256"
	case x509.ECDSAWithSHA1:
		return "ECDSAWithSHA1"
	case x509.ECDSAWithSHA256:
		return "ECDSAWithSHA256"
	case x509.ECDSAWithSHA384:
		return "ECDSAWithSHA384"
	case x509.ECDSAWithSHA512:
		return "ECDSAWithSHA512"
	case x509.PureEd25519:
		return "PureEd25519"
	}
	return "unknown"
}

/*
SignatureAlgorithmForKey returns a signature algorithm that will work for the given key. key can be a public or private
key of type RSA (crypto/rsa), EC (crypto/ecdsa), or Ed25519 (crypto/ed25519). For RSA and EC keys the appropriate
{RSA|ECDSA}WithSHA{1,256,384,512} value will be returned for the corresponding hash value. For Ed25519 keys
x509.PureEd25519 will always be returned regardless of the value of hash.

If an unusable hash or key of unknown type is passed in then x509.UnknownSignatureAlgorithm will be returned.
*/
func SignatureAlgorithmForKey(key any, hash crypto.Hash) x509.SignatureAlgorithm {
	switch typedKey := key.(type) {
	case *rsa.PublicKey:
		return mapSignatureAlgorithm(rsaSignatureAlgorithmTypes, hash)
	case *ecdsa.PublicKey:
		return mapSignatureAlgorithm(ecdsaSignatureAlgorithmTypes, hash)
	case ed25519.PublicKey:
		return x509.PureEd25519

	case *rsa.PrivateKey:
		return SignatureAlgorithmForKey(&typedKey.PublicKey, hash)
	case *ecdsa.PrivateKey:
		return SignatureAlgorithmForKey(&typedKey.PublicKey, hash)
	case ed25519.PrivateKey:
		return SignatureAlgorithmForKey(typedKey.Public(), hash)

	default:
		return x509.UnknownSignatureAlgorithm
	}
}

func mapSignatureAlgorithm(m map[crypto.Hash]x509.SignatureAlgorithm, hash crypto.Hash) x509.SignatureAlgorithm {
	if algo, found := m[hash]; found {
		return algo
	}

	return x509.UnknownSignatureAlgorithm
}
