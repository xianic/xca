package pemcrypt

import (
	"encoding/base64"
	"fmt"
	"gitlab.com/xianic/xca/prompt"
)

type OpType int

const (
	OpEncrypt OpType = iota
	OpDecrypt

	PemPrefix = "XCA ENCRYPTED "

	HeaderPrefix     = "XCA-"
	HeaderVersion    = HeaderPrefix + "Version"
	HeaderPassPhrase = HeaderPrefix + "Passphrase"
	HeaderKey        = HeaderPrefix + "Key"
	HeaderYkHMAC     = HeaderPrefix + "YK-HMAC"
)

type YubiKeySlot int

const (
	// YubikeyDisabled is a slot number used to prevent using a yubikey.
	YubikeyDisabled YubiKeySlot = -1

	// YubikeyAuto is a slot number indicates that a YubikeyShould be used for
	// decryption only if it was used for encryption. Encrypting with this
	// slot number will result in the Yubikey not being used at all. This
	// is the
	YubikeyAuto YubiKeySlot = 0
)

// CryptOptions provides options on how to encrypt or decrypt a PEM block.
// The options can be represented as a set of PEM headers or specified on the
// command line.
type CryptOptions struct {
	// PasswordParameters indicates how to derive the password key from the
	// prompt. A nil value for this structure indicate it is unconfigured and
	// a default set of values will be set if needed.
	PasswordParameters *PasswordParameters

	// PasswordSource is where to get the password from. See one of the
	// PromptFrom*() functions for the various options. If nil, encryption
	// will not prompt for a password whereas decryption will only prompt
	// if a password was used in the encryption process.
	//
	// The value PromptDisabled disables any prompt. Attempting to decrypt
	// A PEM block that requires a password with PromptDisabled will cause the
	// decryption to fail.
	PasswordSource prompt.PasswordPrompt

	// YkHMAC is configuration needed to include a Yubikey in the encryption /
	// decryption process.
	YkHMAC YkHmacOptions

	// Key is the encrypted master key.
	Key []byte
}

// ParseHeaders writes the data found in PEM headers into CryptOptions. It
// will overwrite values PasswordParameters and the Yubikey Salt if found in
// the headers.
// The password source is not modified, and the Yubikey slot is only modified
// if it is currently 0 and the headers indicate a slot to use.
func (op *CryptOptions) ParseHeaders(headers map[string]string) error {
	if headers == nil {
		return nil
	}

	if v := headers[HeaderVersion]; v != "v1" {
		return fmt.Errorf("version %q not supported", v)
	}

	var err error

	op.Key, err = base64.StdEncoding.DecodeString(headers[HeaderKey])
	if err != nil {
		return fmt.Errorf("decoding key header: %w", err)
	}

	if v, found := headers[HeaderPassPhrase]; found {
		op.PasswordParameters = new(PasswordParameters)
		err = op.PasswordParameters.ParsePasswordOptions(v)
		if err != nil {
			return fmt.Errorf("parsing password options: %w", err)
		}
	}

	if v, found := headers[HeaderYkHMAC]; found {
		err = op.YkHMAC.parseYkHmacOptions(v)
		if err != nil {
			return fmt.Errorf("parsing Yubikey HMAC options: %w", err)
		}
	}

	return nil
}

// SetHeaders writes the current options to a map that can be used in a PEM
// block. If headers is nil a new map will be allocated if the CryptOptions
// would write any headers.
func (op *CryptOptions) SetHeaders(headers *map[string]string, ot OpType) {
	if !op.EncryptionEnabled(ot) {
		return
	}

	if *headers == nil {
		*headers = make(map[string]string)
	}

	h := *headers
	h[HeaderVersion] = "v1"
	h[HeaderKey] = base64.StdEncoding.EncodeToString(op.Key)

	if op.WillUsePassword(ot) {
		op.PasswordParameters.SetDefaults()
		h[HeaderPassPhrase] = op.PasswordParameters.String()
	} else {
		delete(h, HeaderPassPhrase)
	}

	if op.WillUseYubikey() {
		h[HeaderYkHMAC] = op.YkHMAC.String()
	} else {
		delete(h, HeaderYkHMAC)
	}
}

func StripPemHeaders(headers map[string]string) {
	delete(headers, HeaderVersion)
	delete(headers, HeaderKey)
	delete(headers, HeaderPassPhrase)
	delete(headers, HeaderYkHMAC)
}

func (op *CryptOptions) WillUsePassword(ot OpType) bool {
	if op == nil {
		return false
	}

	if op.PasswordSource == prompt.PromptDisabled {
		return false
	}

	switch ot {
	case OpDecrypt:
		return op.PasswordParameters != nil

	case OpEncrypt:
		return op.PasswordSource != nil

	default:
		panic("unknown operation")
	}
}

func (op *CryptOptions) WillUseYubikey() bool {
	return op != nil && op.YkHMAC.willUseYubikey()
}

func (op *CryptOptions) EncryptionEnabled(ot OpType) bool {
	return op.WillUsePassword(ot) || op.WillUseYubikey()
}
