package cryptutil

import (
	"crypto/x509"
	"fmt"
	"sort"
	"strings"
)

const NoneName = "None"

var (
	keyUsageNames = []struct {
		Name string
		Bits x509.KeyUsage
	}{
		{"DigitalSignature", x509.KeyUsageDigitalSignature},
		{"ContentCommitment", x509.KeyUsageContentCommitment},
		{"KeyEncipherment", x509.KeyUsageKeyEncipherment},
		{"DataEncipherment", x509.KeyUsageDataEncipherment},
		{"KeyAgreement", x509.KeyUsageKeyAgreement},
		{"CertSign", x509.KeyUsageCertSign},
		{"CRLSign", x509.KeyUsageCRLSign},
		{"EncipherOnly", x509.KeyUsageEncipherOnly},
		{"DecipherOnly", x509.KeyUsageDecipherOnly},
	}

	extKeyUsagesName = []struct {
		Name  string
		Usage x509.ExtKeyUsage
	}{
		{"Any", x509.ExtKeyUsageAny},
		{"ServerAuth", x509.ExtKeyUsageServerAuth},
		{"ClientAuth", x509.ExtKeyUsageClientAuth},
		{"CodeSigning", x509.ExtKeyUsageCodeSigning},
		{"EmailProtection", x509.ExtKeyUsageEmailProtection},
		{"IPSECEndSystem", x509.ExtKeyUsageIPSECEndSystem},
		{"IPSECTunnel", x509.ExtKeyUsageIPSECTunnel},
		{"IPSECUser", x509.ExtKeyUsageIPSECUser},
		{"TimeStamping", x509.ExtKeyUsageTimeStamping},
		{"OCSPSigning", x509.ExtKeyUsageOCSPSigning},
		{"MicrosoftServerGatedCrypto", x509.ExtKeyUsageMicrosoftServerGatedCrypto},
		{"NetscapeServerGatedCrypto", x509.ExtKeyUsageNetscapeServerGatedCrypto},
		{"MicrosoftCommercialCodeSigning", x509.ExtKeyUsageMicrosoftCommercialCodeSigning},
		{"MicrosoftKernelCodeSigning", x509.ExtKeyUsageMicrosoftKernelCodeSigning},
	}

	extKeyUsageMap map[x509.ExtKeyUsage]string
)

func init() {
	extKeyUsageMap = make(map[x509.ExtKeyUsage]string)
	for _, u := range extKeyUsagesName {
		extKeyUsageMap[u.Usage] = u.Name
	}
}

func KeyUsageAllNames() []string {
	names := make([]string, 0, len(keyUsageNames))
	for _, ku := range keyUsageNames {
		names = append(names, ku.Name)
	}
	sort.Strings(names)
	return names
}

func KeyUsageNames(u x509.KeyUsage) []string {
	names := make([]string, 0)
	for _, un := range keyUsageNames {
		if (u & un.Bits) != 0 {
			u &= ^un.Bits
			names = append(names, un.Name)
		}
	}

	if u != 0 {
		names = append(names, fmt.Sprintf("remaining bits 0x%x", u))
	}

	return names
}

func KeyUsageFromNames(names string) (x509.KeyUsage, error) {
	bits := x509.KeyUsage(0)

	for _, name := range strings.Split(names, ",") {
		name = strings.TrimSpace(name)
		found := false
		for _, ku := range keyUsageNames {
			if strings.EqualFold(name, ku.Name) {
				bits |= ku.Bits
				found = true
			}
		}
		if !found && !strings.EqualFold(name, NoneName) {
			return 0, fmt.Errorf("unknown key usage name %q", name)
		}
	}

	return bits, nil
}

func ExtKeyUsageNames(usages []x509.ExtKeyUsage) []string {
	names := make([]string, 0, len(usages))
	for _, u := range usages {
		if name, found := extKeyUsageMap[u]; found {
			names = append(names, name)
		} else {
			names = append(names, fmt.Sprintf("unknown (%d)", u))
		}
	}

	return names
}

func ExtKeyUsageFromNames(names string) ([]x509.ExtKeyUsage, error) {
	usages := make([]x509.ExtKeyUsage, 0)

	for _, name := range strings.Split(names, ",") {
		name = strings.TrimSpace(name)
		found := false
		for _, eku := range extKeyUsagesName {
			if strings.EqualFold(name, eku.Name) {
				usages = append(usages, eku.Usage)
				found = true
			}
		}
		if !found && !strings.EqualFold(name, NoneName) {
			return nil, fmt.Errorf("unknown extended key usage name %q", name)
		}
	}

	return usages, nil
}

func ExtKeyUsageAllNames() []string {
	names := make([]string, 0, len(extKeyUsagesName))
	for _, ku := range extKeyUsagesName {
		names = append(names, ku.Name)
	}

	sort.Strings(names)

	return names
}
