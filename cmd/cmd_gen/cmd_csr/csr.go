package cmd_csr

import (
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

var (
	Cmd = &cobra.Command{
		Use:   "csr {--subject Subject|--host SubjectAltName} [--host SubjectAltName] key_file csr_file",
		Short: "Generate a CSR",
		Long: `Generate a Certificate Sign Request (CSR)

At least one of the --subject or --host flags must be supplied. If the
--subject flag is not used the first --host flag will be used for the subject`,
		Args: genCsrArgValidator,
		RunE: genCsrFunc,
	}

	csrFlags   cmd.CertFlags
	cryptFlags cmd.PemDecryptionFlags
)

func init() {
	cmd_gen.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	csrFlags.Init(fs, cmd.CertFlagsCsr)
	cryptFlags.Init(fs)
}

func genCsrArgValidator(cmd *cobra.Command, args []string) error {
	if csrFlags.Subject == "" && len(csrFlags.DnsSans) == 0 {
		return errors.New("at least one of --subject or --host must be specified")
	}

	return cobra.ExactArgs(2)(cmd, args)
}

func genCsrFunc(_ *cobra.Command, args []string) error {
	key, err := cryptutil.UnmarshalFile(
		args[0],
		"Enter PEM encoded CSR",
		cryptFlags.ParseForDecryption(),
	).ExpectPrivateKey()
	if err != nil {
		return fmt.Errorf("parsing private key: %w", err)
	}

	template := &x509.CertificateRequest{
		SignatureAlgorithm: cryptutil.SignatureAlgorithmForKey(key, cryptutil.DefaultSignatureHash),
	}
	if err = csrFlags.PopulateCsr(template); err != nil {
		return err
	}

	csrDer, err := x509.CreateCertificateRequest(rand.Reader, template, key)
	if err != nil {
		return fmt.Errorf("creating CSR: %w", err)
	}

	csrPem := pem.EncodeToMemory(&pem.Block{
		Type:  cryptutil.PemTypeCsr,
		Bytes: csrDer,
	})

	if err := util.WriteAll(args[1], csrPem, false, util.AllowStdout); err != nil {
		return fmt.Errorf("writing CSR: %w", err)
	}

	return nil
}
