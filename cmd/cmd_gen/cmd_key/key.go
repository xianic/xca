package cmd_key

import (
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/keytype"
	"gitlab.com/xianic/xca/pemcrypt"
	"gitlab.com/xianic/xca/util"
)

var (
	keyCmd = &cobra.Command{
		Use:   "key [flags] output_file",
		Short: "Generate a key",
		Long: `Generate an RSA or elliptic curve key. output_file may be '-' to write the
key to standard output.`,
		Args: cobra.RangeArgs(0, 1),
		RunE: keyCmdFunc,
	}

	keyType string

	encFLags cmd.PemEncryptionFlags
)

func init() {
	cmd_gen.Cmd.AddCommand(keyCmd)

	fs := keyCmd.Flags()

	fs.StringVarP(
		&keyType,
		"type",
		"t",
		"P-256",
		"RSA2048, RSA4096 etc… for an RSA key or one of P-224, P-256, P-384, P-521, or 25519 for an elliptic curve key",
	)
	encFLags.Init(fs)
}

func keyCmdFunc(cmd *cobra.Command, args []string) (err error) {
	for len(args) < 1 {
		args = append(args, "-")
	}

	var key crypto.PrivateKey
	kt := keytype.Type(strings.ToUpper(keyType))

	_, key, err = kt.New(rand.Reader)

	if err != nil {
		return fmt.Errorf("generating key: %w", err)
	}

	block := &pem.Block{
		Type: cryptutil.PemTypePrivateKey,
	}
	block.Bytes, err = x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return
	}

	var options *pemcrypt.CryptOptions
	options, err = encFLags.ParseForEncryption()
	if err != nil {
		return err
	}
	if options != nil {
		if err = pemcrypt.Encrypt(block, options, ""); err != nil {
			return err
		}
	}

	file := util.DelayOpenWrite(args[0], true, util.AllowStdout)
	defer util.DeferClose(file.Close, &err)

	err = pem.Encode(file, block)
	if err != nil {
		return
	}

	return nil
}
