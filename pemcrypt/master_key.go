package pemcrypt

import (
	"bytes"
	"crypto/sha512"
	"fmt"
	"gitlab.com/xianic/xca/prompt"
)

func getRollingKey(op *CryptOptions, userContext string, ot OpType) (rollingKey []byte, err error) {
	/*
		The rolling key is the SHA512_256 of one or two keys concatenated
		together. First is the key derived from the passphrase (if a passphrase is
		used), then there is the key derived from the Yubikey's HMAC-SHA1.
	*/
	rollingKeyHashStream := bytes.Buffer{}

	/*
		Usual behaviour users expect is to be prompted for the Yubikey button
		press for the HMAC-SHA1 immediately after entering their password. We
		achieve this by performing the argon2 key derivation on in another go
		routine once the prompt is successful and continuing to prompt for the
		HMAC-SHA1 while that is progress. A non-nil passwordChan signifies that
		a password has been prompted for.
	*/
	var passwordChan <-chan []byte

	if op.WillUsePassword(ot) {
		if op.PasswordSource == nil {
			op.PasswordSource = prompt.DefaultPasswordPrompt
		}

		var password []byte
		password, err = op.PasswordSource.Prompt(MakePromptArgs(userContext, ot))
		if err != nil {
			return nil, fmt.Errorf("fetching password: %w", err)
		}
		op.PasswordParameters.SetDefaults()
		passwordChan, err = op.PasswordParameters.key(password)
		if err != nil {
			return nil, fmt.Errorf("generating key from password: %w", err)
		}
	}

	var key2fa []byte
	if op.WillUseYubikey() {
		key2fa, err = op.YkHMAC.key()
		if err != nil {
			return nil, fmt.Errorf("getting HMAC from Yubikey: %w", err)
		}
	}

	if passwordChan != nil {
		keyPassword := <-passwordChan
		_, err = rollingKeyHashStream.Write(keyPassword)
		if err != nil {
			return nil, err
		}
	}

	if key2fa != nil {
		_, err = rollingKeyHashStream.Write(key2fa)
		if err != nil {
			return nil, err
		}
	}

	hash := sha512.Sum512_256(rollingKeyHashStream.Bytes())
	return hash[:], nil
}

func (op *CryptOptions) decryptMasterKey(userContext string) ([]byte, error) {
	rollingKey, err := getRollingKey(op, userContext, OpDecrypt)
	if err != nil {
		return nil, err
	}

	masterKey, err := decryptData(op.Key, rollingKey)
	if err != nil {
		return nil, err
	}

	return masterKey, nil
}

func (op *CryptOptions) encryptMasterKey(masterKey []byte, userContext string) error {
	rollingKey, err := getRollingKey(op, userContext, OpEncrypt)
	if err != nil {
		return err
	}

	mkEncrypted, err := encryptData(masterKey, rollingKey)
	if err != nil {
		return err
	}

	op.Key = mkEncrypted
	return nil
}
