package cmd

import (
	"bytes"
	"fmt"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd/cmd_ca"
	"gitlab.com/xianic/xca/util"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"os"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
)

var (
	Cmd = &cobra.Command{
		Use:   "note [flags] [message]",
		Short: "Add a note to the audit log",
		Long: `Add a note to the CA audit log. If the message is not provided on the command
line then it will be read from standard input
`,
		RunE: noteCmdFunc,
	}

	promptFlag cmd_ca.CaPrompt
)

func init() {
	cmd_ca.Cmd.AddCommand(Cmd)
	promptFlag.Init(Cmd.Flags())
}

func noteCmdFunc(_ *cobra.Command, args []string) (err error) {
	var message []byte

	switch len(args) {
	case 0:
		message, err = readMessage()
		if err != nil {
			err = fmt.Errorf("reading message: %w", err)
			return
		}
	case 1:
		message = []byte(args[0])
	default:
		message = []byte(strings.Join(args, " "))
	}

	var c *ca.CA
	c, err = cmd_ca.LoadCa(promptFlag.Prompt(), nil)
	if err != nil {
		return
	}
	defer util.DeferClose(c.Close, &err)

	err = c.LogNote(message)
	if err != nil {
		return
	}

	return
}

func readMessage() ([]byte, error) {
	if terminal.IsTerminal(syscall.Stdin) {
		_, _ = fmt.Fprintf(os.Stderr, "Reading message from standard input. type Ctrl-D on a new line to end.\n")
	}

	buf := new(bytes.Buffer)
	_, err := io.Copy(buf, os.Stdin)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
