package get

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
)

var (
	Cmd = &cobra.Command{
		Use:   "get command",
		Short: "get certificate, public key, or attestation form a Yubikey",
	}
)

func init() {
	ykpiv.Cmd.AddCommand(Cmd)
}
