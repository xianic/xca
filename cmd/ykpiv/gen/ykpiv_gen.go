package gen

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
)

var (
	Cmd = &cobra.Command{
		Use:   "gen [command]",
		Short: "generate keys, certificates and CSRs on a Yubikey",
	}
)

func init() {
	ykpiv.Cmd.AddCommand(Cmd)
}
