package pemstream

import (
	"encoding/base64"
	"encoding/pem"
	"io"
)

// PemBlock is an encoding/pem.Block with additional information about how it
// was parsed from the file it was found in. There is almost enough
// information to convert it back to the original byte stream. Information
// that is not captured:
// - The order of the PEM headers.
// - Any trailing white space.
type PemBlock struct {
	pem.Block

	// WinLineEndings is true if lines end in "\r\n", false if they end in "\n"
	WinLineEndings bool

	// The white space at the beginning of the -----BEGIN line. All other
	// lines must have exactly this white space or the parser errors. This
	// allows parsing of PEM block in config files such as YAML
	Indent []byte

	// Sometimes there is a blank line between the PEM headers and the encoded
	// data. If there is a blank line then PostHeaderBlankLine will be non nil.
	// If it is non nil the only values the parser will accept are a zero length
	// string or the same white space as the rest of the block.
	PostHeaderBlankLine []byte

	// B64LineLength is number of characters of base64 encoding on the first
	// line of encoded data. The parser wil ensure that this is consistent
	// across the block.
	B64LineLen int

	// StartLine and EndLine are the line numbers on which the -----BEGIN and
	// ----END lines reside. If known, will be greater than zero.
	StartLine, EndLine int
}

func (pb *PemBlock) WriteToStream(w io.Writer) (err error) {
	defer func() {
		switch t := recover(); u := t.(type) {
		case error:
			err = u
		case nil:
		default:
			panic(t)
		}
	}()

	multiWrite(w, pb.Indent, pemStartPrefix, []byte(pb.Type), pemLineSuffix, pb.nl())
	for k, v := range pb.Headers {
		multiWrite(w, pb.Indent, []byte(k), []byte(": "), []byte(v), pb.nl())
	}
	if len(pb.Headers) > 0 {
		multiWrite(w, pb.nl())
	}

	encoded := []byte(base64.StdEncoding.EncodeToString(pb.Bytes))
	for len(encoded) > 0 {
		line := encoded
		if len(line) > pb.B64LineLen && pb.B64LineLen >= 4 {
			line = line[0:pb.B64LineLen]
		}
		multiWrite(w, pb.Indent, line, pb.nl())
		encoded = encoded[len(line):]
	}

	multiWrite(w, pb.Indent, pemEndPrefix, []byte(pb.Type), pemLineSuffix, pb.nl())

	return
}

func (pb *PemBlock) nl() []byte {
	if pb.WinLineEndings {
		return []byte("\r\n")
	}

	return []byte("\n")
}

func multiWrite(w io.Writer, ivs ... []byte) {
	for _, iv := range ivs {
		_, err := w.Write(iv)
		if err != nil {
			panic(err)
		}
	}
}
