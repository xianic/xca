package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	SilenceUsage: true,
	Use:          "xca",
	Short:        "A simple certificate and CA tool",
	Long: `xca - Xianic Certificate Authority:

This tool is to create and sign certificates. For example to create a CA and
issue a leaf certificate run:
  xca ca init
  xca gen key leaf.key
  xca gen csr --host example.com leaf.key leaf.csr
  xca ca sign leaf.csr
`,
}

func Execute() {
	err := RootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
