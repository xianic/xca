//go:build ykpiv
// +build ykpiv

package main

import (
	_ "gitlab.com/xianic/xca/ca/yubikey"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/gen/csr"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/gen/key"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/gen/selfsigned"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/get/att"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/get/cert"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/get/pubkey"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/im/cert"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/im/key"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/list"
	_ "gitlab.com/xianic/xca/cmd/ykpiv/reset"
)
