package cmd_verify

import (
	"crypto/x509"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cryptutil"
)

var (
	Cmd = &cobra.Command{
		Use:   "verify [flags] [certificate...]",
		Short: "Verify certificates against criteria",
		Long: `Verify certificates are valid. For each certificate given as an argument an
attempt is made to build a chain back to a root, and the validity of that chain
is checked and printed. Either -r or -s must be used or no roots will be
available for building chains, and it is highly likely that -i will be needed
too. To verify a certificate against a domain ro a specific moment in time -d
and -w may be used. Standard input will be read if no files are provided
on the command line`,
		RunE: verifyCmdFunc,
	}

	domain        string
	when          string
	systemCerts   bool
	roots         []string
	intermediates []string
)

func init() {
	cmd.RootCmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	fs.StringVarP(&domain, "domain", "d", "", "domain to verify certificates against")
	fs.StringVarP(&when, "when", "w", "", "time at which to evaluate the certificates")
	fs.BoolVarP(&systemCerts, "system-roots", "s", false, "load system root certificates")
	fs.StringSliceVarP(&roots, "root", "r", []string{}, "root certificate")
	fs.StringSliceVarP(&intermediates, "intermediate", "i", []string{}, "intermediate certificate")
}

func verifyCmdFunc(cmd *cobra.Command, args []string) (err error) {
	if len(args) == 0 {
		args = []string{"-"}
	}

	options := x509.VerifyOptions{
		DNSName: domain,
	}

	if when != "" {
		options.CurrentTime, err = time.Parse(time.RFC3339, when)
		if err != nil {
			return err
		}
	}

	if systemCerts {
		options.Roots, err = x509.SystemCertPool()
		if err != nil {
			return err
		}
	} else {
		options.Roots = x509.NewCertPool()
	}

	err = loadCertPool(options.Roots, roots)
	if err != nil {
		return err
	}

	options.Intermediates = x509.NewCertPool()
	err = loadCertPool(options.Intermediates, intermediates)
	if err != nil {
		return err
	}

	anyCertErrors := false
	for _, file := range args {
		fileErr, certErrors := verifyFile(file, &options)
		if fileErr != nil {
			return fmt.Errorf("error in file %q: %w\n", file, fileErr)
		}
		if certErrors {
			anyCertErrors = true
		}
	}

	if anyCertErrors {
		return errors.New("certificate verification error")
	}

	return nil
}

func verifyFile(path string, options *x509.VerifyOptions) (fileErr error, certsErrored bool) {
	ps := cryptutil.UnmarshalFile(path, "Enter PEM encoded certificate", nil)
	for i := 1; ; i++ {
		cert, cerr := ps.ExpectCertificate()
		if cerr != nil {
			if i > 1 && errors.Is(cerr, cryptutil.ErrNoPemFound) {
				return
			}
			return
		}

		fmt.Printf("%s #%d %s: ", path, i, cert.Subject.String())
		if verr := verify(cert, options); verr != nil {
			fmt.Printf("%s\n", verr)
			certsErrored = true
		} else {
			fmt.Printf("valid\n")
		}
	}
}

func verify(cert *x509.Certificate, options *x509.VerifyOptions) error {
	chains, err := cert.Verify(*options)
	if err != nil {
		return err
	}

	if len(chains) == 0 {
		return errors.New("no certificate paths found")
	}

	return nil
}

func loadCertPool(pool *x509.CertPool, certFiles []string) error {
	for _, certFile := range certFiles {
		bytes, err := os.ReadFile(certFile)
		if err != nil {
			return err
		}

		ps := cryptutil.NewPemUnmarshaler(cryptutil.NewPemBuffer(bytes))
		for {
			cert, err := ps.ExpectCertificate()
			if err != nil {
				if errors.Is(err, cryptutil.ErrNoPemFound) {
					break
				}
				return err
			}
			pool.AddCert(cert)
		}
	}

	return nil
}
