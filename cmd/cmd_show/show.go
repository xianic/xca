package cmd_show

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/cryptutil/pemstream"
	"gitlab.com/xianic/xca/pemcrypt"
	"gitlab.com/xianic/xca/util"
)

// Cmd represents the show command
var (
	Cmd = &cobra.Command{
		Use:   "show {file|URL|\"-\"}...",
		Short: "Show details of certificates, keys, CSRs and other objects found.",
		Long: `Show details of crypto objects certificates, keys, CSRs and other objects
found when inspecting the files or URLs given on the command line. Multiple
arguments may eb given, all will be inspected in order. If any argument is "-"
or no argument are given then standard input will be read. E.g.

xca show file.pem otherfile.pem
xca show -
xca show example.com:443
xca show https://example.com
`,
		Args: util.CombineCmdArgsFuncs(util.StdInOnce, cobra.MinimumNArgs(1)),
		RunE: showCmdFunc,
	}

	dumpPems bool

	regexHost = regexp.MustCompile(`^[^/:]+$`)
	regexPort = regexp.MustCompile(`^\d+$`)
)

func init() {
	cmd.RootCmd.AddCommand(Cmd)
	Cmd.Flags().BoolVarP(&dumpPems, "dump-pems", "p", false, "write PEM files to standard output after describing them")
}

func showCmdFunc(_ *cobra.Command, args []string) (err error) {
	first := true
	for _, arg := range args {
		errShow := show(arg, &first)
		if errShow != nil {
			fmt.Printf("error: %s\n", errShow)
			if err == nil {
				err = errShow
			}
		}
	}
	return
}

func show(thing string, first *bool) error {
	if thing == "-" {
		return showStream(
			"standard input",
			util.DelayOpenRead("-", util.AllowStdInPrompt("Enter PEM encoded objects")),
			first,
		)
	}

	u, err := url.Parse(thing)
	if err != nil {
		return err
	}

	if regexHost.MatchString(u.Scheme) && regexPort.MatchString(u.Opaque) && u.Host == "" {
		u.Host = u.Scheme + ":" + u.Opaque
		u.Scheme = ""
		u.Opaque = ""
	}

	if u.Host != "" {
		return showHost(u, first)
	} else if u.Path != "" {
		return showFile(u.Path, first)
	}

	return fmt.Errorf("could not parse %q as a URL", thing)
}

func showHost(u *url.URL, first *bool) (err error) {
	var port int
	if portStr := u.Port(); portStr != "" {
		port, err = strconv.Atoi(portStr)
	} else if u.Scheme != "" {
		port, err = net.LookupPort("tcp", u.Scheme)
	} else {
		err = errors.New("neither port or schema specified")
	}
	if err != nil {
		return
	}

	conn, err := tls.Dial(
		"tcp",
		fmt.Sprintf("%s:%d", u.Hostname(), port),
		&tls.Config{
			InsecureSkipVerify: true,
		},
	)
	if conn != nil {
		defer util.DeferClose(conn.Close, &err)
	}
	if err != nil {
		return err
	}

	err = conn.Handshake()
	if err != nil {
		return
	}

	for i, cert := range conn.ConnectionState().PeerCertificates {
		if *first {
			*first = false
		} else {
			fmt.Println()
		}
		fmt.Printf(
			"===== %s:%d #%d =====\n",
			u.Hostname(),
			port,
			i+1,
		)

		showCertificate(cert)

		if dumpPems {
			_ = pem.Encode(os.Stdout, &pem.Block{
				Type:  cryptutil.PemTypeCertificate,
				Bytes: cert.Raw,
			})
		}
	}

	return nil
}

func showFile(path string, first *bool) (err error) {
	var file *os.File
	file, err = os.Open(path)
	if err != nil {
		return err
	}
	defer util.DeferClose(file.Close, &err)
	err = showStream(path, file, first)
	if err != nil {
		return fmt.Errorf("reading file %s: %w", path, err)
	}

	return nil
}

func showStream(name string, r io.Reader, first *bool) error {
	ps := pemstream.NewPemReader(r)

	blockCounts := make(map[string]*int)
	for {
		// get a pem block and show it
		block, err := ps.GetPemBlock()
		if err != nil {
			return err
		}
		if block == nil {
			return nil
		}

		blockCount := blockCounts[block.Type]
		if blockCount == nil {
			blockCount = new(int)
			*blockCount = 1
			blockCounts[block.Type] = blockCount
		} else {
			*blockCount++
		}
		if *first {
			*first = false
		} else {
			fmt.Println()
		}
		fmt.Printf(
			"===== %s #%d %s:%d-%d =====\n",
			block.Type,
			*blockCount,
			name,
			block.StartLine,
			block.EndLine,
		)

		switch block.Type {
		case cryptutil.PemTypeRsaPrivateKey:
			fallthrough
		case cryptutil.PemTypeECPrivateKey:
			fallthrough
		case cryptutil.PemTypePrivateKey:

			var key crypto.PrivateKey
			key, err = cryptutil.UnmarshalPrivateKey(block.Bytes)
			if err == nil {
				showPrivateKey(key)
			}

		case cryptutil.PemTypeRsaPublicKey:
			fallthrough
		case cryptutil.PemTypeECPublicKey:
			fallthrough
		case cryptutil.PemTypePublicKey:
			var key crypto.PublicKey
			key, err = x509.ParsePKIXPublicKey(block.Bytes)
			if err == nil {
				showPublicKey(key)
			}

		case cryptutil.PemTypeCsr:
			var csr *x509.CertificateRequest
			csr, err = x509.ParseCertificateRequest(block.Bytes)
			if err == nil {
				showCsr(csr)
			}

		case cryptutil.PemTypeCertificate:
			var cert *x509.Certificate
			cert, err = x509.ParseCertificate(block.Bytes)
			if err == nil {
				showCertificate(cert)
			}

		default:
			if strings.HasPrefix(block.Type, pemcrypt.PemPrefix) {
				showPemcrypt(&block.Block)
			} else {
				fmt.Printf("unknown PEM block type %q\n", block.Type)
			}
		}
		if err != nil {
			fmt.Printf("error decoding PEM block: %s\n", err)
		}

		if dumpPems {
			_ = pem.Encode(os.Stdout, &block.Block)
		}
	}
}

func showPemcrypt(block *pem.Block) {
	fmt.Printf("Type:\t\t\t%s\n", block.Type[len(pemcrypt.PemPrefix):])
	var options pemcrypt.CryptOptions
	if err := options.ParseHeaders(block.Headers); err != nil {
		fmt.Printf("\tErr:\t\t:%s\n", err)
		return
	}

	if options.PasswordParameters != nil {
		fmt.Printf(
			"Argon2 parameters:\n\tIteratons:\t%d\n\tMemory:\t\t%d MB\n\tThreads:\t%d\n",
			options.PasswordParameters.Time,
			options.PasswordParameters.Memory,
			options.PasswordParameters.Threads,
		)
	}

	if options.WillUseYubikey() {
		fmt.Printf("Yubikey slot:\t\t%d\n", options.YkHMAC.Slot)
	}
}

func showPrivateKey(key crypto.PrivateKey) {
	switch priv := key.(type) {
	case *rsa.PrivateKey:
		fmt.Printf("RSA private key for\n")
		showPublicKey(&priv.PublicKey)

	case *ecdsa.PrivateKey:
		fmt.Printf("EC private key for\n")
		showPublicKey(&priv.PublicKey)

	case ed25519.PrivateKey:
		fmt.Printf("EC25519 private key\n")
		showPublicKey(priv.Public())

	default:
		fmt.Printf("private key of unknown type %T\n", key)
	}
}

func showPublicKey(key crypto.PublicKey) {
	fp := cryptutil.PublicKeyFingerprint(key)

	switch pub := key.(type) {
	case *rsa.PublicKey:
		fmt.Printf(
			"RSA public key:\n\tSize:\t\t%d bits (e = %d)\n\tFingerprint:\t%X (SHA512_256 of PKCS1 encoding)\n",
			pub.Size()*8,
			pub.E,
			fp,
		)

	case *ecdsa.PublicKey:
		fmt.Printf(
			"EC public key:\n\tType:\t\t%s\n\tSize:\t\t%d bits\n\tFingerprint:\t%X\n",
			pub.Params().Name,
			pub.Params().BitSize,
			fp,
		)

	case ed25519.PublicKey:
		fmt.Printf(
			"ED25519 public key\n\tFingerprint:\t%X\n",
			fp,
		)

	default:
		fmt.Printf("public key of unknown type %T\n", pub)
	}
}

func showCsr(csr *x509.CertificateRequest) {
	showName("Subject", &csr.Subject)
	showSans(csr.DNSNames, csr.EmailAddresses, csr.IPAddresses, csr.URIs)
	fmt.Printf("Signature algorithm:\t%s\n", csr.SignatureAlgorithm)
	fmt.Printf("Signature:\t\t")
	if err := csr.CheckSignature(); err != nil {
		fmt.Printf("invalid: %v\n", err)
	} else {
		fmt.Printf("valid\n")
	}
	showPublicKey(csr.PublicKey)
}

func showSans(dnsNames []string, emailAddrs []string, ips []net.IP, uris []*url.URL) {
	fmt.Printf(
		"SANS:\t(%d total)\n",
		len(dnsNames)+len(emailAddrs)+len(ips)+len(uris),
	)
	for _, dnsName := range dnsNames {
		fmt.Printf("\tdns:\t\t%s\n", dnsName)
	}

	for _, emailAddr := range emailAddrs {
		fmt.Printf("\temail:\t\t%s\n", emailAddr)
	}

	for _, ip := range ips {
		fmt.Printf("\tip:\t\t%s\n", ip.String())
	}

	for _, uri := range uris {
		fmt.Printf("\turi:\t\t%s\n", uri.String())
	}
}

func showCertificate(cert *x509.Certificate) {
	fmt.Printf(
		"Fingerprint:\t\t%X (SHA256 of DER encoding)\n"+
			"Signature algorithm:\t%s\n"+
			"Self signed:\t\t%t\n"+
			"Serial no.\t\t%X\n",
		sha256.Sum256(cert.Raw),
		cryptutil.SignatureAlgorithmName(cert.SignatureAlgorithm),
		cryptutil.CertificateIsSelfSigned(cert),
		cert.SerialNumber,
	)
	showPublicKey(cert.PublicKey)
	fmt.Printf(
		"Version: \t\t%d\n",
		cert.Version,
	)
	showName("Issuer", &cert.Issuer)
	showName("Subject", &cert.Subject)
	showSans(cert.DNSNames, cert.EmailAddresses, cert.IPAddresses, cert.URIs)

	now := time.Now()
	fmt.Printf(
		"Validity:\n"+
			"\tNot before:\t%s (%s)\n"+
			"\tNot after:\t%s (%s)\n",
		cert.NotBefore.Format(time.RFC3339),
		humanize.RelTime(cert.NotBefore, now, "ago", "from now"),
		cert.NotAfter.Format(time.RFC3339),
		humanize.RelTime(cert.NotAfter, now, "ago", "from now"),
	)

	if cert.BasicConstraintsValid {
		fmt.Printf("\tIs CA cert:\t%v\n", cert.IsCA)
		fmt.Printf("\tMaxPathLen:\t")
		if cert.MaxPathLen < 0 || (cert.MaxPathLen == 0 && !cert.MaxPathLenZero) {
			fmt.Printf("unset\n")
		} else {
			fmt.Printf("%d\n", cert.MaxPathLen)
		}
	}

	showKeyUsage(cert.KeyUsage)
	showExtendedKeyUsage(cert.ExtKeyUsage)

	nscrit := "non-"
	if cert.PermittedDNSDomainsCritical {
		nscrit = ""
	}
	fmt.Printf(
		"Name constraints:\t%d %scritical\n",
		len(cert.PermittedDNSDomains)+len(cert.ExcludedDNSDomains)+
			len(cert.PermittedIPRanges)+len(cert.ExcludedIPRanges)+
			len(cert.PermittedEmailAddresses)+len(cert.ExcludedEmailAddresses)+
			len(cert.PermittedURIDomains)+len(cert.ExcludedURIDomains),
		nscrit,
	)
	showConstrains("DNS:\t", cert.PermittedDNSDomains, cert.ExcludedDNSDomains)
	showConstrains("IP Range:", ipRangeStr(cert.PermittedIPRanges), ipRangeStr(cert.ExcludedIPRanges))
	showConstrains("Email:\t", cert.PermittedEmailAddresses, cert.ExcludedEmailAddresses)
	showConstrains("URI Domain:", cert.PermittedURIDomains, cert.ExcludedURIDomains)

	if len(cert.SubjectKeyId) > 0 {
		fmt.Printf("Subject key ID:\t\t%X\n", cert.SubjectKeyId)
	}
	if len(cert.AuthorityKeyId) > 0 {
		fmt.Printf("AUthority key ID:\t%X\n", cert.AuthorityKeyId)
	}

	for _, ext := range cert.Extensions {
		if printer := extensionRegistry[ext.Id.String()]; printer != nil {
			printer(ext)
		}
	}
}

func showConstrains(key string, permit, exclude []string) {
	for _, p := range permit {
		fmt.Printf("\t%s\tpermit %q\n", key, p)
	}
	for _, e := range exclude {
		fmt.Printf("\t%s\texclude %q\n", key, e)
	}
}

func mapSlice[From, To any](s []From, f func(From) To) []To {
	t := make([]To, 0, len(s))
	for _, e := range s {
		t = append(t, f(e))
	}
	return t
}

func ipRangeStr(ipr []*net.IPNet) []string {
	return mapSlice(ipr, func(ip *net.IPNet) string {
		return ip.String()
	})
}

func showKeyUsage(usageBits x509.KeyUsage) {
	names := cryptutil.KeyUsageNames(usageBits)
	if len(names) == 0 {
		names = []string{cryptutil.NoneName}
	}
	fmt.Printf("Key usage:\t\t%s\n", strings.Join(names, ", "))
}

func showExtendedKeyUsage(usages []x509.ExtKeyUsage) {
	names := cryptutil.ExtKeyUsageNames(usages)
	if len(names) == 0 {
		names = []string{cryptutil.NoneName}
	}
	fmt.Printf("Extended key usage:\t%s\n", strings.Join(names, ", "))
}

func showName(title string, name *pkix.Name) {
	fmt.Printf("%s:\n", title)
	max := maxLen(
		name.Country,
		name.Organization,
		name.OrganizationalUnit,
		name.Locality,
		name.Province,
		name.StreetAddress,
		name.PostalCode,
	)

	fmt.Printf("\t%s\n", cryptutil.NameAsString(name))
	if name.CommonName != "" {
		fmt.Printf("\tCommon name:\t%s\n", name.CommonName)
	}
	for i := 0; i < max; i++ {
		if len(name.OrganizationalUnit) > i {
			fmt.Printf("\tOrg unit #%d:\t%s\n", i+1, name.OrganizationalUnit[i])
		}
		if len(name.Organization) > i {
			fmt.Printf("\tOrg      #%d:\t%s\n", i+1, name.Organization[i])
		}
		if len(name.StreetAddress) > i {
			fmt.Printf("\tStreet address #%d:\t%s\n", i+1, name.StreetAddress[i])
		}
		if len(name.Locality) > i {
			fmt.Printf("\tLocality #%d:\t%s\n", i+1, name.Locality[i])
		}
		if len(name.Province) > i {
			fmt.Printf("\tProvince #%d:\t%s\n", i+1, name.Province[i])
		}
		if len(name.PostalCode) > i {
			fmt.Printf("\tPostal code #%d:\t%s\n", i+1, name.PostalCode[i])
		}
		if len(name.Country) > i {
			fmt.Printf("\tCountry  #%d:\t%s\n", i+1, name.Country[i])
		}
	}
	if name.SerialNumber != "" {
		fmt.Printf("\tSerial number:\t%s\n", name.SerialNumber)
	}
}

func maxLen(things ...[]string) (maxLen int) {
	for _, thing := range things {
		tl := len(thing)
		if tl > maxLen {
			maxLen = tl
		}
	}

	return
}
