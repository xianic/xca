package cmd

import (
	"crypto/x509"
	"crypto/x509/pkix"
	"errors"
	"fmt"
	flag "github.com/spf13/pflag"
	"gitlab.com/xianic/xca/cryptutil"
	"net"
	"strings"
	"time"
)

type CertFlagsType int

const (
	CertFlagsCACert CertFlagsType = iota
	CertFlagsSelfSign
	CertFlagsSign
	CertFlagsCsr

	DefaultKeyUsageCA    = x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign | x509.KeyUsageCRLSign
	DefaultKeyUsageNonCA = x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment
)

var (
	DefaultExtKeyUsageCA    = make([]x509.ExtKeyUsage, 0)
	DefaultExtKeyUsageNonCA = []x509.ExtKeyUsage{
		x509.ExtKeyUsageServerAuth,
		x509.ExtKeyUsageClientAuth,
	}
)

type CertFlags struct {
	IsCa          bool
	MaxPathLength int

	Subject string

	Lifetime uint
	Predate  time.Duration

	KeyUsages    string
	ExtKeyUsages string

	// Subject Alternate Names
	DnsSans []string
	IpSans  []string

	// Name Constraints
	PermittedDNSDomains     []string
	ExcludedDNSDomains      []string
	PermittedIPRanges       []string
	ExcludedIPRanges        []string
	PermittedEmailAddresses []string
	ExcludedEmailAddresses  []string
	PermittedURIDomains     []string
	ExcludedURIDomains      []string
}

/*
Init creates flags for customising certificate generation to a command. Not all flags are valid in all
situations so CertFlagsType controls which flags will be created

	+-----------------------------+--------------------------------------------+
	| Value of ct (CertFlagsType) | Will create flags for adjusting a ...      |
	+-----------------------------+--------------------------------------------+
	| CertFlagsCACert             | ... CA certificate                         |
	| CertFlagsSelfSign           | ... self-signed certificate                |
	| CertFlagsSign               | ... certificate about to be signed by a CA |
	| CertFlagsCsr                | ... CSR being generated                    |
	+-----------------------------+--------------------------------------------+
*/
func (f *CertFlags) Init(fs *flag.FlagSet, ct CertFlagsType) {

	// choiceOfCa is true if the user can choose if cer is a CA. f.IsCa will be set to a default.
	choiceOfCa := ct == CertFlagsSign || ct == CertFlagsSelfSign
	f.IsCa = ct == CertFlagsCACert

	var defaultLifetime uint
	if f.IsCa || ct == CertFlagsSelfSign {
		defaultLifetime = 3650 // ~10 years
	} else {
		defaultLifetime = 180 // 180 days
	}

	if choiceOfCa {
		fs.BoolVarP(&f.IsCa, "ca", "", f.IsCa, "create CA certificate")
	}

	if choiceOfCa || f.IsCa {
		fs.IntVarP(&f.MaxPathLength, "max-path-length", "", -1, "Issuance chain length in addition to this cert. '2' would allow one intermediate CA")
		fs.UintVarP(&f.Lifetime, "lifetime", "", defaultLifetime, "lifetime of the certificate in days")
		fs.DurationVarP(&f.Predate, "predate", "", time.Duration(5)*time.Minute, "NotBefore = now - predate")
	}

	if ct != CertFlagsCsr {
		fs.StringVarP(
			&f.KeyUsages,
			"key-usage",
			"",
			"",
			"comma separated key usage names ("+joinWords(cryptutil.KeyUsageAllNames())+") or \""+cryptutil.NoneName+"\"",
		)
		fs.StringVarP(
			&f.ExtKeyUsages,
			"ext-key-usage",
			"",
			"",
			"comma separated extended key usage names ("+joinWords(cryptutil.ExtKeyUsageAllNames())+") or \""+cryptutil.NoneName+"\"",
		)
	}

	fs.StringVarP(&f.Subject, "subject", "", "", "Subject")
	fs.StringArrayVar(&f.DnsSans, "host", []string{}, "DNS Subject Alternate Name")
	fs.StringArrayVar(&f.IpSans, "ip", []string{}, "IP address Subject Alternate Name")

	if ct != CertFlagsCsr {
		fs.StringArrayVar(&f.PermittedDNSDomains, "permit-host", []string{}, "CA name constraint: permit hostname")
		fs.StringArrayVar(&f.ExcludedDNSDomains, "exclude-host", []string{}, "CA name constraint: exclude hostname")
		fs.StringArrayVar(&f.PermittedIPRanges, "permit-net", []string{}, "CA name constraint: permit subnet")
		fs.StringArrayVar(&f.ExcludedIPRanges, "exclude-net", []string{}, "CA name constraint: exclude subnet")
		fs.StringArrayVar(&f.PermittedEmailAddresses, "permit-email", []string{}, "CA name constraint: permit email address")
		fs.StringArrayVar(&f.ExcludedEmailAddresses, "exclude-email", []string{}, "CA name constraint: exclude email address")
		fs.StringArrayVar(&f.PermittedURIDomains, "permit-uri", []string{}, "CA name constraint: permit URI domain")
		fs.StringArrayVar(&f.ExcludedURIDomains, "exclude-uri", []string{}, "CA name constraint: exclude URI domain")
	}
}

func (f *CertFlags) CheckPopulate() error {
	if err := f.populateSubject(new(pkix.Name)); err != nil {
		return err
	}

	var ips []net.IP
	if err := parseIpAddrs(&ips, f.IpSans); err != nil {
		return err
	}

	var nets []*net.IPNet
	if err := parseIpCIRDs(&nets, f.PermittedIPRanges); err != nil {
		return err
	}

	if err := parseIpCIRDs(&nets, f.ExcludedIPRanges); err != nil {
		return err
	}

	if f.KeyUsages != "" {
		if _, err := cryptutil.KeyUsageFromNames(f.KeyUsages); err != nil {
			return err
		}
	}

	if f.ExtKeyUsages != "" {
		if _, err := cryptutil.ExtKeyUsageFromNames(f.ExtKeyUsages); err != nil {
			return err
		}
	}

	return nil
}

func (f *CertFlags) PopulateCert(cert *x509.Certificate) error {
	cert.IsCA = f.IsCa

	if err := f.populateSubject(&cert.Subject); err != nil {
		return err
	}

	now := time.Now()
	cert.NotBefore = now.Add(-f.Predate)
	cert.NotAfter = now.Add(time.Duration(f.Lifetime) * time.Duration(24) * time.Hour)
	cert.MaxPathLen = f.MaxPathLength
	cert.MaxPathLenZero = f.MaxPathLength == 0
	cert.BasicConstraintsValid = true

	if f.KeyUsages != "" {
		var err error
		cert.KeyUsage, err = cryptutil.KeyUsageFromNames(f.KeyUsages)
		if err != nil {
			return err
		}
	} else {
		if f.IsCa {
			cert.KeyUsage = DefaultKeyUsageCA
		} else {
			cert.KeyUsage = DefaultKeyUsageNonCA
		}
	}

	if f.ExtKeyUsages != "" {
		var err error
		cert.ExtKeyUsage, err = cryptutil.ExtKeyUsageFromNames(f.ExtKeyUsages)
		if err != nil {
			return err
		}
	} else {
		if f.IsCa {
			cert.ExtKeyUsage = DefaultExtKeyUsageCA
		} else {
			cert.ExtKeyUsage = DefaultExtKeyUsageNonCA
		}
	}

	if len(f.DnsSans) > 0 {
		cert.DNSNames = f.DnsSans
	}

	if err := parseIpAddrs(&cert.IPAddresses, f.IpSans); err != nil {
		return err
	}

	if err := f.populateConstraints(cert); err != nil {
		return err
	}

	return nil
}

func (f *CertFlags) PopulateCsr(csr *x509.CertificateRequest) error {
	if err := f.populateSubject(&csr.Subject); err != nil {
		return err
	}

	if len(f.DnsSans) > 0 {
		csr.DNSNames = f.DnsSans
	}

	if err := parseIpAddrs(&csr.IPAddresses, f.IpSans); err != nil {
		return err
	}

	return nil
}

func (f *CertFlags) populateSubject(name *pkix.Name) error {
	var subject string

	if f.Subject != "" {
		if f.Subject[0] == '/' {
			parsed, err := cryptutil.StringAsName(f.Subject)
			if err != nil {
				return fmt.Errorf("parsing subjectstring %q: %w", f.Subject, err)
			}
			*name = *parsed
			return nil
		} else {
			subject = f.Subject
		}
	}

	if subject == "" && len(f.DnsSans) > 0 {
		subject = f.DnsSans[0]
	}

	if subject == "" && len(f.IpSans) > 0 {
		parsed := net.ParseIP(f.IpSans[0])
		if parsed != nil {
			subject = parsed.String()
		}
	}

	if subject != "" {
		*name = pkix.Name{CommonName: subject}
	}

	return nil
}

func (f *CertFlags) populateConstraints(cert *x509.Certificate) error {
	if !f.hasConstraints() {
		return nil
	}

	if !f.IsCa {
		return errors.New("CA name constraints used on non-CA certificate")
	}

	cert.PermittedDNSDomainsCritical = true

	cert.PermittedDNSDomains = f.PermittedDNSDomains
	cert.ExcludedDNSDomains = f.ExcludedDNSDomains

	if err := parseIpCIRDs(&cert.PermittedIPRanges, f.PermittedIPRanges); err != nil {
		return err
	}

	if err := parseIpCIRDs(&cert.ExcludedIPRanges, f.ExcludedIPRanges); err != nil {
		return err
	}

	cert.PermittedEmailAddresses = f.PermittedEmailAddresses
	cert.ExcludedEmailAddresses = f.ExcludedEmailAddresses

	cert.PermittedURIDomains = f.PermittedURIDomains
	cert.ExcludedURIDomains = f.ExcludedURIDomains

	return nil
}

func (f *CertFlags) hasConstraints() bool {
	return len(f.PermittedDNSDomains) > 0 ||
		len(f.ExcludedDNSDomains) > 0 ||
		len(f.PermittedIPRanges) > 0 ||
		len(f.ExcludedIPRanges) > 0 ||
		len(f.PermittedEmailAddresses) > 0 ||
		len(f.ExcludedEmailAddresses) > 0 ||
		len(f.PermittedURIDomains) > 0 ||
		len(f.ExcludedURIDomains) > 0
}

func parseIpAddrs(ips *[]net.IP, text []string) error {
	if len(text) > 0 {
		*ips = make([]net.IP, 0, len(text))
		for _, addrStr := range text {
			if addr := net.ParseIP(addrStr); addr != nil {
				*ips = append(*ips, addr)
			} else {
				return fmt.Errorf("invalid IP address: %q", addrStr)
			}
		}
	}

	return nil
}

func parseIpCIRDs(ips *[]*net.IPNet, text []string) error {
	if len(text) > 0 {
		*ips = make([]*net.IPNet, 0, len(text))
		for _, addrStr := range text {
			_, mask, err := net.ParseCIDR(addrStr)
			if err != nil {
				return err
			}
			*ips = append(*ips, mask)
		}
	}

	return nil
}

func joinWords(words []string) string {
	return strings.Join(words, ", ")
}
