package cryptutil

import (
	"encoding/pem"
	"gitlab.com/xianic/xca/util"
	"io"
)

// PemBuffer is a sequence of PEM blocks stored in a buffer. It implements
// PemParser.
type PemBuffer struct {
	buffer   []byte
	loadFrom io.Reader
}

// NewPemBuffer stores be in a PemBuffer allowing it to be parsed. Modifying
// b after this call may affect the parser's results
func NewPemBuffer(b []byte) *PemBuffer {
	return &PemBuffer{
		buffer: b,
	}
}

// NewPemBufferFromStream stores stream and reads it to completion the first
// call to NextPem(). If the stream implements io.Closer it will
// be closed.
func NewPemBufferFromStream(stream io.Reader) *PemBuffer {
	return &PemBuffer{
		loadFrom: stream,
	}
}

func (pb *PemBuffer) load() (err error) {
	if pb.loadFrom == nil {
		return
	}

	if closable, ok := pb.loadFrom.(io.Closer); ok {
		defer util.DeferClose(closable.Close, &err)
	}

	pb.buffer, err = io.ReadAll(pb.loadFrom)
	pb.loadFrom = nil
	return
}

func (pb *PemBuffer) NextPem() (block *pem.Block, err error) {
	err = pb.load()
	if err != nil {
		return nil, err
	}

	if pb.buffer == nil {
		return nil, nil
	}

	block, pb.buffer = pem.Decode(pb.buffer)
	return
}

func (pb *PemBuffer) RemainingBytes() ([]byte, error) {
	err := pb.load()
	if err != nil {
		return nil, err
	}

	return pb.buffer, nil
}
