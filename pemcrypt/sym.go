package pemcrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
)

func encryptDataWithNewKey(plaintext []byte) (ciphertext, key []byte, err error) {
	key = make([]byte, 32)
	if _, err = rand.Read(key); err != nil {
		return nil, nil, err
	}

	ciphertext, err = encryptData(plaintext, key)
	if err != nil {
		return nil, nil, err
	}

	return
}

func makeCipher(key []byte) (cipher.AEAD, error) {
	if len(key) != 32 {
		// ensure we are using 256bit AES
		return nil, errors.New("wrong key length")
	}

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	return gcm, nil
}

func decryptData(ciphertext, key []byte) (plaintext []byte, err error) {
	var aead cipher.AEAD
	aead, err = makeCipher(key)
	if err != nil {
		return nil, err
	}

	nonceSize := aead.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}
	nonce := ciphertext[:nonceSize]
	ciphertext = ciphertext[nonceSize:]
	return aead.Open(ciphertext[:0], nonce, ciphertext, nil)
}

func encryptData(plaintext, key []byte) (ciphertext []byte, err error) {
	var aead cipher.AEAD
	aead, err = makeCipher(key)
	if err != nil {
		return nil, err
	}

	nonceSize := aead.NonceSize()
	nonce := make([]byte, nonceSize, nonceSize+len(plaintext)+aead.Overhead())
	if _, err = rand.Read(nonce); err != nil {
		return nil, err
	}

	ciphertext = aead.Seal(nonce, nonce, plaintext, nil)
	return
}
