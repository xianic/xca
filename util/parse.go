package util

import (
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
)

func ParseFields(text, separator string, minFields int, fields ...interface{}) (fieldsParsed int, err error) {
	pieces := strings.Split(text, separator)
	if minFields > 0 && len(pieces) < minFields {
		return 0, fmt.Errorf(
			"not enough fields in %q, have %d, require no less than %d",
			text,
			len(pieces),
			minFields,
		)
	}
	if len(pieces) > len(fields) {
		return 0, fmt.Errorf(
			"too many fields in %q, have %d, require no more than %d",
			text,
			len(pieces),
			minFields,
		)
	}

	for i, piece := range pieces {
		var err error
		var parsedUint uint64
		var parsedInt int64
		switch tf := fields[i].(type) {
		case *string:
			*tf = piece
		case *uint8:
			parsedUint, err = strconv.ParseUint(piece, 10, 8)
			*tf = uint8(parsedUint)
		case *uint32:
			parsedUint, err = strconv.ParseUint(piece, 10, 32)
			*tf = uint32(parsedUint)
		case *[]byte:
			*tf, err = base64.StdEncoding.DecodeString(piece)
		case *int:
			parsedInt, err = strconv.ParseInt(piece, 10, 32)
			*tf = int(parsedInt)
		default:
			return i, fmt.Errorf("cannot write to type %T: not implemented", tf)
		}

		if err != nil {
			return i, fmt.Errorf("parsing fird %d %q: %w", i, piece, err)
		}
	}

	return len(pieces), nil
}
