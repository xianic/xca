package att

import (
	"crypto/x509"
	"encoding/pem"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/get"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
	"os"
)

var (
	Cmd = &cobra.Command{
		Use:     "pubkey",
		Short:   "get key a public from Yubikey",
		Aliases: []string{"key"},
		RunE:    pubKeyCmd,
	}

	selectionFlags ykpiv.SelectionFlags
)

func init() {
	get.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
}

func pubKeyCmd(_ *cobra.Command, _ []string) (err error) {
	var yk *piv.YubiKey
	var slot piv.Slot
	yk, slot, err = selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	att, err := yk.Attest(slot)
	if err != nil {
		return err
	}

	pubKeyBytes, err := x509.MarshalPKIXPublicKey(att.PublicKey)
	if err != nil {
		return err
	}
	return pem.Encode(os.Stdout, &pem.Block{
		Type:  cryptutil.PemTypePublicKey,
		Bytes: pubKeyBytes,
	})
}
