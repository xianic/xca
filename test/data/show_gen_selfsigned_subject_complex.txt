===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		F28E470ED6E18828A75BD1A5CE022D4E37E13025BC877CF2719E8E1DCC8D20E3 (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		5FC12354D9A0EBDD
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	DB0221A6836E809CD11BE12400D31359BBCD50FEE3F33E7E576D58AB464931A4
Version: 		3
Issuer:
	/C=Country 1/ST=Province 1/L=Locality 1/streetAddress=Street 1/postalCode=Postcode 1/O=Org 1/OU=Org unit 1/CN=Common Name/serialNumber=5/userid=User ID
	Common name:	Common Name
	Org unit #1:	Org unit 1
	Org      #1:	Org 1
	Street address #1:	Street 1
	Locality #1:	Locality 1
	Province #1:	Province 1
	Postal code #1:	Postcode 1
	Country  #1:	Country 1
	Serial number:	5
Subject:
	/C=Country 1/ST=Province 1/L=Locality 1/streetAddress=Street 1/postalCode=Postcode 1/O=Org 1/OU=Org unit 1/CN=Common Name/serialNumber=5/userid=User ID
	Common name:	Common Name
	Org unit #1:	Org unit 1
	Org      #1:	Org 1
	Street address #1:	Street 1
	Locality #1:	Locality 1
	Province #1:	Province 1
	Postal code #1:	Postcode 1
	Country  #1:	Country 1
	Serial number:	5
SANS:	(0 total)
Validity:
	Not before:	2023-09-02T19:54:03Z (5 minutes ago)
	Not after:	2033-08-30T19:59:03Z (10 years from now)
	Is CA cert:	false
	MaxPathLen:	unset
Key usage:		DigitalSignature, KeyEncipherment
Extended key usage:	ServerAuth, ClientAuth
Name constraints:	0 non-critical
