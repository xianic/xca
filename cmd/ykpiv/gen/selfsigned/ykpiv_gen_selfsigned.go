package csr

import (
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"errors"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
	"math/big"
)

var (
	Cmd = &cobra.Command{
		Use:   "selfsigned [flags]",
		Short: "create a self signed certificate on a Yubikey using the stored key",
		RunE:  selfsignedCmd,
	}

	selectionFlags ykpiv.SelectionFlags
	pinFlags       ykpiv.PinFlag
	certFlags      cmd.CertFlags
	forceFlags     ykpiv.ForceFlag
)

func init() {
	gen.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
	pinFlags.Init(fs, ".")
	certFlags.Init(fs, cmd.CertFlagsSelfSign)
	forceFlags.Init(fs, "allow overwrite of existing certificate")
}

func selfsignedCmd(_ *cobra.Command, args []string) (err error) {
	yk, slot, err := selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	var pubKey crypto.PublicKey
	att, err := yk.Attest(slot)
	if err != nil {
		if !errors.Is(err, piv.ErrNotFound) {
			return fmt.Errorf("retrieving attestation from yubikey: %w", err)
		}
	} else {
		pubKey = att.PublicKey
	}

	oldCert, err := yk.Certificate(slot)
	if err != nil {
		if !errors.Is(err, piv.ErrNotFound) {
			return fmt.Errorf("retrieving existing certificate from yubikey: %w", err)
		}
	} else {
		if !forceFlags {
			return fmt.Errorf("refusing to overwrite existing certificate")
		} else if pubKey == nil {
			pubKey = oldCert.PublicKey
		}
	}

	if pubKey == nil {
		return fmt.Errorf("could not find public key in either an attestation or a certificate in slot %02x, generate or import a key first", slot.Key)
	}

	mk, pin, err := pinFlags.Unlock(yk)
	if err != nil {
		return err
	}
	signer, err := yk.PrivateKey(slot, pubKey, piv.KeyAuth{
		PIN:       pin,
		PINPolicy: piv.PINPolicyAlways,
	})

	serialBytes := make([]byte, 8)
	if _, err := rand.Read(serialBytes); err != nil {
		return fmt.Errorf("reading random data: %w", err)
	}

	signatureAlgorithm := cryptutil.SignatureAlgorithmForKey(pubKey, cryptutil.DefaultSignatureHash)
	template := &x509.Certificate{
		SerialNumber:       new(big.Int).SetBytes(serialBytes),
		SignatureAlgorithm: signatureAlgorithm,
	}
	if err = certFlags.PopulateCert(template); err != nil {
		return err
	}

	certDer, err := x509.CreateCertificate(rand.Reader, template, template, pubKey, signer)
	if err != nil {
		return fmt.Errorf("creating certificate: %w", err)
	}

	newCert, err := x509.ParseCertificate(certDer)
	if err != nil {
		return fmt.Errorf("parsing new certificate: %w", err)
	}

	err = yk.SetCertificate(mk, slot, newCert)
	if err != nil {
		return fmt.Errorf("saving new certificate to yubikey: %w", err)
	}

	return err
}
