package ca

import (
	"context"
	"crypto"
	"crypto/x509"
	"fmt"
	"gitlab.com/xianic/xca/prompt"
	"math/big"
	"os"
	"path/filepath"

	"gitlab.com/xianic/xca/siglog"
)

type CA struct {
	driver Driver
	Config *Config

	signer      crypto.Signer
	certificate *x509.Certificate
	log         *siglog.LogStream
	prompt      prompt.PasswordPrompt

	pastCerts []*CertTrace
}

func newCA(path string, p prompt.PasswordPrompt) (newCa *CA, err error) {
	config, err := loadConfig(path)
	if err != nil {
		return nil, err
	}

	driver, err := config.MakeDriver()
	if err != nil {
		return nil, err
	}

	newCa = &CA{
		Config:    config,
		driver:    driver,
		pastCerts: make([]*CertTrace, 0),
		prompt:    p,
	}

	return newCa, nil
}

/*
CreateCA creates a new CA object and initializes the key, certificate and audit
log on disk. If the key or certificate already exist they will be used as is.
The audit log must not exist prior to this call. keyCryptOptionscan be used to
customize how the key will be encrypted (if created) or decrypted (if already
existing).
*/
func CreateCA(ctx context.Context, path string, cust CertCustomiser, p prompt.PasswordPrompt) (*CA, error) {
	ca, err := newCA(path, p)
	if err != nil {
		return nil, err
	}

	pubKey, driverCustomiser, signer, err := ca.driver.InitPrivateKey(p)
	if err != nil {
		return nil, fmt.Errorf("initialising CA private private key: %w", err)
	}
	ca.signer = signer
	if ca.log != nil {
		ca.log.SetKey(signer)
	}

	if _, err := ca.initCert(pubKey, JoinCustomisers(cust, driverCustomiser), p); err != nil {
		return nil, err
	}

	if err := ca.loadCertificate(); err != nil {
		return nil, err
	}

	if err = ca.logOpen(ctx, nil, true); err != nil {
		return nil, err
	}

	return ca, nil
}

/*
LoadCA loads an existing CA. The certificate and audit log must already exist
and the audit log will be verified. auditor can be non-nil to perform
additional verification as the log is verified. keyCryptOptions can be non-nil
to customize how the key will be decrypted when/if it is loaded. The private
key for the CA does not need to exist for the CA to be loaded. An attempt to
load the private key will only be made if an operation which requires it is
performed upon the loaded CA.
*/
func LoadCA(ctx context.Context, path string, auditor siglog.MessageChecker, p prompt.PasswordPrompt) (*CA, error) {
	ca, err := newCA(path, p)
	if err != nil {
		return nil, err
	}

	if err = ca.loadCertificate(); err != nil {
		return nil, err
	}

	if err = ca.logOpen(ctx, auditor, false); err != nil {
		return nil, err
	}

	return ca, nil
}

/*
MaybeLoadCa will inspect the path and attempt to load the CA if it
looks like a CA config file or directory. It will return (nil, nil) if no
attempt is made to load the CA. The path should exist as either a file or a
directory.
*/
func MaybeLoadCa(ctx context.Context, path string, isDir bool, p prompt.PasswordPrompt) (*CA, error) {
	if !isDir {
		// path is a file
		if filepath.Base(path) == DefaultConfigFileName {
			return LoadCA(ctx, path, nil, nil)
		}

		return nil, nil
	}

	// path is a directory
	if fileExistsNoErr(path, DefaultConfigFileName) || fileExistsNoErr(path, DefaultCertFileName) && fileExistsNoErr(path, DefaultAuditLogFileName) {
		return LoadCA(ctx, path, nil, p)
	}

	return nil, nil
}

func fileExistsNoErr(path string, additions ...string) bool {
	stat, err := os.Stat(filepath.Join(append([]string{path}, additions...)...))
	if err != nil {
		return false
	}
	return stat != nil && (stat.Mode()&os.ModeType) == 0
}

func (c *CA) PublicKey() crypto.PublicKey {
	return c.certificate.PublicKey
}

func (c *CA) NextSerialNumber() *big.Int {
	return new(big.Int).SetBytes(c.log.NextSerial())
}

func (c *CA) Certificate() *x509.Certificate {
	return c.certificate
}

func (c *CA) Close() error {
	err := c.driver.Close()
	c.signer = nil

	if c.log != nil {
		if err2 := c.log.Close(); err == nil && err2 != nil {
			err = err2
		}
	}

	return err
}

func (c *CA) String() string {
	return c.Config.loadPath
}
