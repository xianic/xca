package cmd_sign

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_ca"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

var (
	Cmd = &cobra.Command{
		Use:   "sign [options] csr_file [cert_file]",
		Short: "Sign a CSR with a CA",
		Long: `Sign a CSR and create a certificate with a CA. The certificate will be entered
into the CA's audit log. If either csr_file or cert_file are '-' then standard
input or standard output will be used respectively, otherwise the given file
will be read/written. If cert_file is not specified then '.pem' will be
appended to csr_file to derive then name after any '.csr' suffix has been
removed, unless the CSR is read from standard input in which case the
certificate will be written to standard output.
`,
		Args: cobra.RangeArgs(1, 2),
		RunE: caSignCmdFunc,
	}

	signFlags  cmd.CertFlags
	promptFlag cmd_ca.CaPrompt
)

func init() {
	cmd_ca.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	signFlags.Init(fs, cmd.CertFlagsSign)
	promptFlag.Init(fs)
}

func caSignCmdFunc(cmd *cobra.Command, args []string) (err error) {
	var csr *x509.CertificateRequest
	csr, err = cryptutil.UnmarshalFile(args[0], "Enter PEM encoded CSR", nil).ExpectCSR()
	if err != nil {
		return
	}

	var c *ca.CA
	c, err = cmd_ca.LoadCa(promptFlag.Prompt(), nil)
	if err != nil {
		return err
	}
	defer util.DeferClose(c.Close, &err)

	// In future consider copying some extensions from the CSR to the cert
	template := &x509.Certificate{
		Subject:        csr.Subject,
		SerialNumber:   c.NextSerialNumber(),
		DNSNames:       csr.DNSNames,
		EmailAddresses: csr.EmailAddresses,
		IPAddresses:    csr.IPAddresses,
	}
	err = signFlags.PopulateCert(template)
	if err != nil {
		return
	}

	var certOutputFile string
	if len(args) < 2 {
		certOutputFile = args[0]
		if certOutputFile != "-" {
			if strings.HasSuffix(certOutputFile, ".csr") {
				certOutputFile = certOutputFile[0 : len(certOutputFile)-4]
			}
			certOutputFile += ".pem"
		}
	} else {
		certOutputFile = args[1]
	}

	var certOutputStream io.Writer
	if certOutputFile == "-" {
		certOutputStream = os.Stdout
	} else {
		var file *os.File
		file, err = os.OpenFile(certOutputFile, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			err = fmt.Errorf("opening certificate file: %w", err)
			return
		}
		defer util.DeferClose(file.Close, &err)
		certOutputStream = file
	}

	var newCert []byte
	newCert, err = c.Sign(template, csr)
	if err != nil {
		return
	}

	err = pem.Encode(
		certOutputStream,
		&pem.Block{
			Type:  cryptutil.PemTypeCertificate,
			Bytes: newCert,
		},
	)
	if err != nil {
		return
	}

	return
}
