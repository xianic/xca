package cert

import (
	"crypto/x509"
	"errors"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/im"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

var (
	Cmd = &cobra.Command{
		Use:     "certificate [certfile|-]",
		Short:   "import a certificate into a Yubikey",
		RunE:    imCertCmd,
		Aliases: []string{"cert"},
		Args:    cobra.RangeArgs(0, 1),
	}

	selectionFlags ykpiv.SelectionFlags
	pinFlags       ykpiv.PinFlag
	forceFlag      ykpiv.ForceFlag
)

func init() {
	im.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
	pinFlags.Init(fs, ".")
	forceFlag.Init(fs, "allow overwrite of existing certificate")
}

func imCertCmd(_ *cobra.Command, args []string) (err error) {
	if len(args) == 0 {
		args = []string{"-"}
	}

	var cert *x509.Certificate
	cert, err = cryptutil.UnmarshalFile(args[0], "certificate", nil).ExpectCertificate()
	if err != nil {
		return fmt.Errorf("loading certificate from %q: %w", args[0], err)
	}

	yk, slot, err := selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	oldCert, err := yk.Certificate(slot)
	if err != nil {
		if !errors.Is(err, piv.ErrNotFound) {
			return err
		}
	} else if cert != nil && !forceFlag {
		return fmt.Errorf("not overwriting existing certificate %s", oldCert.Subject.String())
	}

	mk, _, err := pinFlags.Unlock(yk)
	if err != nil {
		return err
	}

	err = yk.SetCertificate(mk, slot, cert)
	if err != nil {
		return fmt.Errorf("setting certificate: %w", err)
	}

	return nil
}
