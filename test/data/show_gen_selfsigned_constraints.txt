===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		EA6328691CAFC23323968FE59844CB92CA8DEE9D024DCAD2F8E11E6C9C8C282C (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		EE456AFE3D3230FE
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	882B6D685AF260FCB44C72FB702D6A318FF88EB12D2346102077CEFB6E7BB68F
Version: 		3
Issuer:
	/CN=Hello World
	Common name:	Hello World
Subject:
	/CN=Hello World
	Common name:	Hello World
SANS:	(0 total)
Validity:
	Not before:	2023-11-21T16:44:50Z (5 minutes ago)
	Not after:	2033-11-18T16:49:50Z (10 years from now)
	Is CA cert:	true
	MaxPathLen:	2
Key usage:		DigitalSignature, CertSign, CRLSign
Extended key usage:	None
Name constraints:	10 critical
	DNS:		permit "example.com"
	DNS:		exclude "test.example.com"
	IP Range:	permit "192.0.2.0/24"
	IP Range:	permit "2001:db8::/32"
	IP Range:	exclude "192.0.2.1/32"
	IP Range:	exclude "2001:db8::/40"
	Email:		permit "example.com"
	Email:		exclude "test@example.com"
	URI Domain:	permit "https://example.com/"
	URI Domain:	exclude "https://example.com/test"
Subject key ID:		E1D9FEE50F00D94496DC8DD7D9E6801DD0E372B2
