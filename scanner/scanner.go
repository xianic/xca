package scanner

import (
	"context"
	"fmt"
	"gitlab.com/xianic/xca/prompt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/scanner/pemblockfile"
	"gitlab.com/xianic/xca/scanner/pkimodel"
	"gitlab.com/xianic/xca/util"
)

func Scan(ctx context.Context, c *pkimodel.Collection, path string, errorStream chan<- error) error {
	absPath, err := filepath.Abs(path)
	if err != nil {
		return err
	}

	err = filepath.WalkDir(absPath, scannerFunc(ctx, c, errorStream))
	if err != nil {
		return err
	}

	return nil
}

func scannerFunc(ctx context.Context, c *pkimodel.Collection, errorStream chan<- error) fs.WalkDirFunc {
	return func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			if errorStream != nil {
				errorStream <- err
				err = nil
			}
			return err
		}

		err = ctx.Err()
		if err != nil {
			return err
		}

		if should, err := c.ShouldScan(path); err != nil || !should {
			return err
		}

		loadedCa, err := ca.MaybeLoadCa(ctx, path, d.IsDir(), prompt.PromptDisabled)
		if err != nil {
			if errorStream != nil {
				errorStream <- err
				err = nil
			}
		} else if loadedCa != nil {
			err = c.AddCA(loadedCa)
			if err != nil {
				if errorStream != nil {
					errorStream <- err
					err = nil
				} else {
					return err
				}
			}
			if d.IsDir() {
				return fs.SkipDir
			}
			return nil
		}

		if (d.Type() & fs.ModeType) == 0 {
			err = scanFile(ctx, c, path)
			if err != nil {
				if errorStream != nil {
					errorStream <- err
					return nil
				}
				return err
			}
		}

		return nil
	}
}

func scanFile(ctx context.Context, c *pkimodel.Collection, path string) (err error) {
	var hFile *os.File
	hFile, err = os.Open(path)
	if err != nil {
		return err
	}
	defer util.DeferClose(hFile.Close, &err)

	var pemFile *pemblockfile.PemBlockFile
	pemFile, err = pemblockfile.ParsePemBlockFile(hFile, path)

	if err != nil {
		return fmt.Errorf("parsing %q: %w", path, err)
	}

	return c.AddFile(pemFile)
}
