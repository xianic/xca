package cryptutil

import (
	"bytes"
	"crypto/x509"
	"strings"
)

/*
CertificateIsSelfSigned reports whether a certificate is self-signed. Subject/issuer key IDs are used if available or a
signature check is made if IDs are not available.
*/

func CertificateIsSelfSigned(cert *x509.Certificate) bool {
	if len(cert.SubjectKeyId) != 0 && len(cert.AuthorityKeyId) != 0 {
		return bytes.Equal(cert.SubjectKeyId, cert.AuthorityKeyId)
	}

	err := cert.CheckSignature(cert.SignatureAlgorithm,
		cert.RawTBSCertificate, cert.Signature)
	return err == nil
}

/*
DescribeCertificate produces a one line description for a human to read. It contains the certificate's subject and SANs.
*/
func DescribeCertificate(cert *x509.Certificate) string {
	b := strings.Builder{}
	b.WriteString(NameAsString(&cert.Subject))

	for _, dnsName := range cert.DNSNames {
		b.WriteString(" host:")
		b.WriteString(dnsName)
	}

	for _, emailAddr := range cert.EmailAddresses {
		b.WriteString(" email:")
		b.WriteString(emailAddr)
	}

	for _, ip := range cert.IPAddresses {
		b.WriteString(" ip:")
		b.WriteString(ip.String())
	}

	for _, uri := range cert.URIs {
		b.WriteString(" uri:")
		b.WriteString(uri.String())
	}

	return b.String()
}
