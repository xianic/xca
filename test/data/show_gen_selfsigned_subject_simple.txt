===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		E962A3E59D7E06040580EDE4E4048A6BCE84728554C409FBA1937881CED3A4AC (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		CBAC712401DBDF94
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	B464552E75AC0FF81CA4C7AE3B42973D0A12B6B7AC0ABD480CE973455B7D55CD
Version: 		3
Issuer:
	/CN=Hello World
	Common name:	Hello World
Subject:
	/CN=Hello World
	Common name:	Hello World
SANS:	(0 total)
Validity:
	Not before:	2023-08-29T07:56:07Z (5 minutes ago)
	Not after:	2033-08-26T08:01:07Z (10 years from now)
	Is CA cert:	false
	MaxPathLen:	unset
Key usage:		DigitalSignature, KeyEncipherment
Extended key usage:	ServerAuth, ClientAuth
Name constraints:	0 non-critical
