package ca

import (
	"crypto/rand"
	"crypto/x509"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"math/big"
)

func (c *CA) Sign(template *x509.Certificate, csr *x509.CertificateRequest) ([]byte, error) {
	if err := c.loadCertificate(); err != nil {
		return nil, err
	}

	if err := c.loadSigner(); err != nil {
		return nil, err
	}

	template.SerialNumber = new(big.Int).SetBytes(c.log.NextSerial())
	template.AuthorityKeyId = c.certificate.SubjectKeyId
	template.SubjectKeyId = cryptutil.SubjectKeyId(csr.PublicKey)
	template.SignatureAlgorithm = cryptutil.SignatureAlgorithmForKey(c.certificate.PublicKey, cryptutil.DefaultSignatureHash)

	certBytes, err := x509.CreateCertificate(rand.Reader, template, c.certificate, csr.PublicKey, c.signer)
	if err != nil {
		return nil, err
	}

	cert, err := x509.ParseCertificate(certBytes)
	if err != nil {
		return nil, err
	}

	if err := cert.CheckSignatureFrom(c.certificate); err != nil {
		return nil, fmt.Errorf("verifying issuance: %w", err)
	}

	err = c.logWriteIssuance(csr.Raw, certBytes)
	if err != nil {
		return nil, err
	}

	c.pastCerts = append(c.pastCerts, MakeTrace(cert))

	return certBytes, nil
}
