package pemstream

import (
	"errors"
	"io"
)

var (
	ErrNoB64       = errors.New("expected some base64 encoded data")
	ErrB64LenWrong = errors.New("wrong number of base64 bytes on line")
	ErrB64EndTwice = errors.New("base64 encoding ended more than once")
)

// sectionStream is a wrapper around PemReader. It will read a section
// and then stop. A section is either the gap between two PEM blocks or
// the bas64 encoded data contained within a block.
type sectionStream struct {
	ps  *PemReader
	err error
}

func (g *sectionStream) Read(p []byte) (n int, err error) {
	if g.err != nil {
		return 0, g.err
	}

	n, err = g.ps.read(p)
	if err != nil {
		g.detach(err)
	}

	return
}

func (g *sectionStream) Close() error {
	var err error
	defer g.detach(err)

	buf := make([]byte, 512)
	for err == nil {
		_, err = g.Read(buf)
	}

	if err == io.EOF {
		return nil
	}

	return err
}

// detach removes all links between the PemReader and the sectionStream
// allowing the sectionStream to be garbage collected and preventing it from
// interfering with the next section.
func (g *sectionStream) detach(err error) {
	if err == nil {
		g.err = io.EOF
	} else {
		g.err = err
	}

	if g.ps != nil {
		if g.ps.lastSectionStream == g {
			g.ps.lastSectionStream = nil
			g.ps.downstream = nil
		}
		g.ps = nil
	}
}
