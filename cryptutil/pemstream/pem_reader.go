package pemstream

import (
	"bytes"
	"io"

	"gitlab.com/xianic/xca/util"
)

const (
	// additional buffer space to allocate building a sequence of bytes
	bufferIncrement = 512
)

// PemReader wraps an io.Reader and reads PEM blocks from it. The
// input stream contains blocks and data between the blocks (called gaps here).
// gaps and blocks can be extracted from the stream alternately by calling
// GetPem*() and GetGap*() functions. A PemReader always starts with a gap
// even if it is zero bytes long.
//
// To tell if the end of the input stream has been reached, both a GetPem*()
// function and a GetGap*() will return nil.
type PemReader struct {
	// mode is the finite state machine of the parser. It is given the
	// opportunity to consume bytes that are read from upstream and prevent
	// them being available to the downstream reader. It may manipulate state
	// in this object
	mode parserMode

	// upstream is where the PEMs are read from.
	upstream io.Reader

	// downstream is the reader that is returned to the caller of
	// GetPemDataStream or GetGapDataStream. GetPemDataStream will only
	// return it if block is non-nil. GetGapDataStream will only return it if
	// block is nil. lastSectionStream holds the last created sectionStream,
	// allowing it to be detach()ed when a new one is created
	lastSectionStream *sectionStream
	downstream        io.ReadCloser
	block             *PemBlock

	// mode may indicate that it won't match any more data until
	// the end of the line. It is only called if matcherAligned is true. If
	// it is false then data read from upstream is passed to downstream as is
	// until a carriage return is detected in which case the stream is
	// matcherAligned again.
	matcherAligned bool

	// data from upstream may need to be captured before the parser mode can
	// decide if it needs it or not. It is held in buffer.
	buffer []byte

	// canKeep and matchLength indicate how much of the buffer a client can
	// have. Up to canKeep bytes will be returned in the next read() call
	// after which matchLength bytes will be discarded. If matchLength is
	// negative then that many more bytes need to be read from upstream into
	// the buffer before the matcher can make a decision.
	canKeep, matchLength int

	// If the matcher has returned io.EOF, this will be true to indicate that
	// once canKeep bytes have been returned, an io.EOF should also be
	// returned.
	matcherEof bool

	// Incremented for each line read from upstream. The code that consumes
	// the \n is responsible for incrementing this.
	lineNumber int

	// If set parsing has terminated with this error. It is a permanent
	// condition and all operations will fail with this error
	err error
}

// NewPemReader initializes a new PemReader to read from upstream
func NewPemReader(upstream io.Reader) *PemReader {
	ps := &PemReader{
		mode:           gapMode,
		upstream:       upstream,
		matcherAligned: true,
		lineNumber:     1,
	}
	ps.downstream = ps.newSectionStream()
	return ps
}

// GetGapDataStream returns an io.ReadCloser that will read all the bytes of the
// stream up to the beginning of the next PEM block or the input stream EOF.
// When this stream is Closed it will read and discard all remaining data.
// If the returned ReadCloser is nil then the PemReader is part way through
// reading a PEM block.
//
// Calling GetGapDataStream() repeatedly will return the same stream unmodified.
func (ps *PemReader) GetGapDataStream() (io.ReadCloser, error) {
	if err := ps.Err(); err != nil && err != io.EOF {
		return nil, err
	}

	if ps.block != nil {
		return nil, nil
	}

	return ps.downstream, nil
}

// GetPemDataStream finds the beginning of the next PEM block and returns
// information about it and an io.ReadCloser that will produce the decoded
// content of the PEM block. As such blockInfo.Bytes will be nil and
// blockInfo.EndLine will be zero until the io.ReadCloser has read the entire
// block.
//
// If no PEM block is available nil is returned for both the stream and the
// block. Calling GetPemDataStream() repeatedly will return the same stream.
func (ps *PemReader) GetPemDataStream() (blockInfo *PemBlock, blockData io.ReadCloser, err error) {
	err = ps.Err()
	if err != nil && err != io.EOF {
		return nil, nil, err
	}

	// skip to the end of the gap
	if ps.block == nil && ps.downstream != nil {
		err = ps.downstream.Close()
		if err != nil {
			return nil, nil, err
		}
	}

	err = ps.Err()
	if err != nil && err != io.EOF {
		return nil, nil, err
	}

	if ps.block != nil && ps.downstream != nil {
		return ps.block, ps.downstream, nil
	}

	return nil, nil, nil
}

// GetPemBlock reads, decodes and returns the nex PEM block. It discards all
// data prior to the PEM block. Calling GetPemBlock() repeatedly will return
// each PEM block in the upstream reader until there are no more in which
// nil will be returned.
func (ps *PemReader) GetPemBlock() (block *PemBlock, err error) {
	var stream io.ReadCloser
	block, stream, err = ps.GetPemDataStream()
	if stream == nil || err != nil {
		return nil, err
	}
	defer util.DeferClose(stream.Close, &err)

	if block != nil {
		block.Bytes, err = io.ReadAll(stream)
		if err != nil {
			return nil, err
		}
	}

	return block, nil
}

// Err returns the last error form the parser or underlying io.Reader.
func (ps *PemReader) Err() error {
	if ps.err == io.EOF && ps.buffer != nil && len(ps.buffer) > 0 {
		// we mask an upstream EOF when there is still data in the buffer to process
		return nil
	}

	return ps.err
}

func (ps *PemReader) newSectionStream() *sectionStream {
	if ps.lastSectionStream != nil {
		ps.lastSectionStream.detach(io.EOF)
	}

	ps.lastSectionStream = &sectionStream{
		ps: ps,
	}

	return ps.lastSectionStream
}

// read some data from the underlying input stream. This follows the same
// semantics as io.Reader.Read() but is not public as it is intended to be
// wrapped by a sectionStream. The PemReader object will be mutated by the
// parser during this call.
func (ps *PemReader) read(p []byte) (n int, err error) {
	for {
		err = ps.Err()
		if err != nil {
			return 0, err
		}

		if ps.buffer == nil {
			n, err = ps.upstream.Read(p)
			if err != nil {
				ps.err = err
				if err != io.EOF {
					return 0, err
				}
			}

			// How much of the data can the caller keep?
			err = ps.findUninterestingPointInBuffer(p[:n])
			if err != nil && err != io.EOF {
				ps.err = err
				return 0, err
			}

			if ps.matchLength != 0 {
				// we have found as much as can be kept. Save any that hasn't
				// been processed
				retain := ps.canKeep
				if ps.matchLength > 0 {
					retain += ps.matchLength
					ps.matchLength = 0
				}
				if retain < n {
					nc := copy(ps.makeBuffer(n-retain), p[retain:n])
					bufGrowLen(&ps.buffer, nc)
				}
			}

			if ps.canKeep > 0 || err != nil {
				n = ps.canKeep
				ps.canKeep = 0
				return n, err
			} else if ps.buffer == nil {
				continue
			}
		}

		if ps.canKeep > 0 {
			n = copy(p, ps.buffer[:ps.canKeep])
			bufConsume(&ps.buffer, n)
			ps.canKeep -= n
		} else {
			n = 0
		}

		if ps.canKeep == 0 && ps.matchLength > 0 {
			bufConsume(&ps.buffer, ps.matchLength)
			ps.matchLength = 0
		}

		if len(ps.buffer) == 0 {
			ps.buffer = nil
		}

		if n > 0 || ps.canKeep > 0 || ps.matcherEof {
			if ps.matcherEof && ps.canKeep == 0 {
				err = io.EOF
				ps.matcherEof = false
			} else {
				err = nil
			}
			return n, err
		}

		for ps.matchLength < 0 && ps.err == nil {
			// allocate more space if needed
			n, err = ps.upstream.Read(ps.makeBuffer(-ps.matchLength))
			bufGrowLen(&ps.buffer, n)
			ps.matchLength += n
			if ps.matchLength > 0 {
				ps.matchLength = 0
			}
			if err != nil {
				ps.err = err
				if err != io.EOF {
					return 0, err
				}
			}
		}

		if ps.buffer != nil {
			err = ps.findUninterestingPointInBuffer(ps.buffer)
			if err != nil {
				if err != io.EOF {
					ps.err = err
					return 0, err
				} else {
					ps.matcherEof = true
				}
			}
			if ps.err == io.EOF && ps.matchLength < 0 {
				return 0, io.EOF
			}
		}
	}
}

// makeBuffer ensures that there is at least size bytes available in ps.buffer
func (ps *PemReader) makeBuffer(size int) []byte {
	return bufExpand(&ps.buffer, size, bufferIncrement)
}

// findUninterestingPointInBuffer finds how much of a buffer can be skipped
// over and returned to the caller of read()
func (ps *PemReader) findUninterestingPointInBuffer(p []byte) (err error) {
	ps.canKeep = 0
	for {
		if ps.matcherAligned {
			atEof := ps.err == io.EOF
			ps.matchLength, err = ps.mode(ps, p[ps.canKeep:], atEof)

			if atEof && (err == nil || err == io.EOF) && ps.matchLength < 0 && ps.block != nil {
				// if we are at the upstream EOF and the parser wants more data
				// then the EOF is unexpected.
				err = io.ErrUnexpectedEOF
			}
			if err != nil {
				return
			}

			if ps.matchLength > 0 {
				for i := ps.canKeep; i < ps.canKeep+ps.matchLength; i++ {
					if p[i] == '\n' {
						ps.lineNumber++
					}
				}
			}

			if ps.matchLength != 0 {
				return
			}

			ps.matcherAligned = false
		}

		// can kee at least up to the next new line
		nl := bytes.IndexByte(p[ps.canKeep:], '\n')
		if nl == -1 {
			// there are no new lines in the data,
			// caller can keep it all
			ps.canKeep = len(p)
			return
		} else {
			// we have become matcher aligned
			ps.canKeep += nl + 1
			ps.matcherAligned = true
			ps.lineNumber++
		}
	}
}
