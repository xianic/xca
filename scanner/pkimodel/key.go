package pkimodel

import (
	"crypto"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/keytype"
)

type PrivateKeyInFile ObjectInFile[any]

type PrivateKey struct {
	inFile PrivateKeyInFile

	privateKey crypto.PrivateKey
	publicKey  crypto.PublicKey
	keyType    keytype.Type
	usedBy     []*Certificate
}

func NewPrivateKeyOnline(oif PrivateKeyInFile) (kif *PrivateKey, err error) {
	kif = &PrivateKey{
		inFile: oif,
		usedBy: make([]*Certificate, 0),
	}

	kif.privateKey, err = oif.GetObject()
	if err != nil {
		return nil, fmt.Errorf("parsing private key from %s: %w", oif.String(), err)
	}

	kif.keyType = keytype.TypeOf(kif.privateKey)
	if kif.keyType.IsUnknown() {
		return nil, fmt.Errorf(
			"parsed private key from %s: %T: %w",
			oif.String(),
			kif.privateKey,
			keytype.ErrUnknownType,
		)
	}

	kif.publicKey = cryptutil.PublicKeyFor(kif.privateKey)

	return
}

func NewPrivateKeyOffline(oif PrivateKeyInFile) (kif *PrivateKey, err error) {
	kif = &PrivateKey{
		inFile: oif,
	}

	kif.publicKey, err = oif.GetObject()
	if err != nil {
		err = fmt.Errorf("parsing private key from %s: %w", oif.String(), err)
		kif = nil
	}

	kif.keyType = keytype.TypeOf(kif.publicKey)
	if kif.keyType.IsUnknown() {
		return nil, fmt.Errorf(
			"parsed public key from %s: %T: %w",
			oif.String(),
			kif.privateKey,
			keytype.ErrUnknownType,
		)
	}

	return
}

func (c *PrivateKey) File() DataInFile {
	return c.inFile
}

func (c *PrivateKey) Parsed() crypto.PrivateKey {
	return c.privateKey
}

func (c *PrivateKey) Type() keytype.Type {
	return c.keyType
}

func (c *PrivateKey) PublicKey() crypto.PublicKey {
	return c.publicKey
}

func (c *PrivateKey) Fingerprint() []byte {
	return cryptutil.PublicKeyFingerprint(c.publicKey)
}

func (c *PrivateKey) FingerprintHex() string {
	return fmt.Sprintf("%X", c.Fingerprint())
}

func (c *PrivateKey) String() string {
	return fmt.Sprintf(
		"%s [%X] (%s)",
		c.keyType,
		c.Fingerprint()[:4],
		c.inFile.String(),
	)
}
