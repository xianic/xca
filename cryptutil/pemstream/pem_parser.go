package pemstream

import (
	"bytes"
	"encoding/pem"
	"errors"
	"io"
	"strings"
)

var (
	pemStartPrefix   = []byte("-----BEGIN ")
	pemEndPrefix     = []byte("-----END ")
	pemLineSuffix    = []byte("-----")
	pemLineSuffixLen = len(pemLineSuffix)

	ErrTrailingGarbage         = errors.New("additional garbage at end of the line")
	ErrInconsistentLineEndings = errors.New("a mix of Linux and Windows line endings has been detected")
	ErrInconsistentIndentation = errors.New("inconsistent indentation")
	ErrTooManyBlankLines       = errors.New("too many blank lines between PEM headers and block")
)

// parserMode tests a binary sequence against some criteria looking for
// a particular sequence. It may adjust the state of the PemReader.
//
// If io.EOF is returned then the current read from the PemReader will return
// io.EOF. This should be done when transitioning from a gap to a block or
// vice versa. Returning any other non-nil error will abort the read and any
// future parsing with that error.
//
// eof will be true when no more data is available. In this case, if a
// negative match length is returned the parser will return io.UnexpectedEOF.
//
// The parserMode should test p return a matchLength that may be:
//   - greater than zero to indicate that the first matchLength bytes match
//     in p have been consumed. The next call to the parserMode will see p
//     with those bytes removed from p.
//   - zero to indicate that no match can be found and that no amount of
//     additional data appended to p will change that. The PemReader will
//     begin calling the parserMode with p populate with data beginning after
//     the next carriage return found.
//   - less than zero, to indicate that at least this many bytes of data will
//     need to be appended to p to know if a match can be made. p will have
//     at least this many more bytes appended before the parserMode is called
//     again
type parserMode func(ps *PemReader, p []byte, eof bool) (matchLength int, err error)

// gapMode searches the space between PEM blocks looking for a -----BEGIN line
func gapMode(ps *PemReader, p []byte, _ bool) (matchLength int, err error) {
	// separate off the white space at the start of the line
	ws, p := cutMatch(p, isWhiteSpace)

	// check for "-----BEGIN "
	p, matchLength = cutPrefix(p, pemStartPrefix)
	if matchLength <= 0 {
		if matchLength < 0 {
			matchLength -= pemLineSuffixLen + 2
		}
		return
	}

	// separate off the block type
	blockType, p := cutMatch(p, func(b byte) bool {
		return (b >= 'A' && b <= 'Z') || (b >= '0' && b <= '9') || b == ' '
	})
	if len(blockType) == 0 || len(p) == 0 {
		return -1 - pemLineSuffixLen, nil
	}

	// check for "-----"
	p, matchLength = cutPrefix(p, pemLineSuffix)
	if matchLength <= 0 {
		if matchLength < 0 {
			matchLength -= 1
		}
		return
	}

	// allow the line to end with white space
	trailingWs, p := cutMatch(p, isWhiteSpace)

	// find the end of the line
	cr, crLen := findCr(p)

	switch cr {
	case -1:
		return -1, nil

	case 0:
		matchLength = bufLengths(ws, pemStartPrefix, blockType, pemLineSuffix, trailingWs) + crLen
		ps.block = &PemBlock{
			Block: pem.Block{
				Type: string(blockType),
			},
			Indent:         bytes.Clone(ws),
			StartLine:      ps.lineNumber,
			WinLineEndings: crLen == 2,
		}
		ps.mode = headerMode
		ps.lineNumber++

		setUpDecodingStream(ps)
		return matchLength, io.EOF

	default:
		return 0, ErrTrailingGarbage
	}
}

// Having found the BEGIN line, this searches for headers and a blank line.
// There may be non, in which case it transitions straight into looking for
// END line
func headerMode(ps *PemReader, p []byte, eof bool) (matchLength int, err error) {
	ws, line := cutMatch(p, isWhiteSpace)

	// we need the whole line to be able to parse it
	crOffset, crLen := findCr(line)
	if crOffset < 0 {
		matchLength = -1
		return
	}
	if ps.block.WinLineEndings && crLen != 2 {
		return 0, ErrInconsistentLineEndings
	}

	line = line[:crOffset]
	matchLength = len(ws) + len(line) + crLen

	if bytes.Compare(ws, ps.block.Indent) != 0 && (len(ws) != 0 || ps.block.PostHeaderBlankLine == nil) {
		return 0, ErrInconsistentIndentation
	}

	if len(line) == 0 {
		if ps.block.PostHeaderBlankLine != nil {
			err = ErrTooManyBlankLines
		} else {
			ps.block.PostHeaderBlankLine = bytes.Clone(ws)
		}
		return
	}

	sepIndex := bytes.IndexByte(line, ':')
	if sepIndex == -1 {
		ps.mode = makeBlockMode(
			ps.block.Indent,
			pemEndPrefix,
			[]byte(ps.block.Block.Type),
			pemLineSuffix,
		)
		return ps.mode(ps, p, eof)
	}

	if ps.block.Block.Headers == nil {
		ps.block.Block.Headers = make(map[string]string)
	}

	ps.block.Block.Headers[strings.TrimSpace(string(line[:sepIndex]))] =
		strings.TrimSpace(string(line[sepIndex+1:]))

	return
}

func makeBlockMode(expectedPieces ...[]byte) parserMode {
	expectedLength := bufLengths(expectedPieces...)

	// search for an an -----END line
	return func(ps *PemReader, p []byte, eof bool) (matchLength int, err error) {
		diffPoint := 0
		for _, ep := range expectedPieces {
			n := bufDivergesAt(p[diffPoint:], ep)
			diffPoint += n
			if n < len(ep) {
				break
			}
		}

		if diffPoint == len(p) && diffPoint < expectedLength {
			return -1, nil
		}

		if diffPoint < len(ps.block.Indent) {
			return 0, ErrInconsistentIndentation
		}

		if diffPoint < expectedLength {
			return 0, nil
		}

		p = p[diffPoint:]

		tws, p := cutMatch(p, isWhiteSpace)
		crOffset, crLen := findCr(p)
		if crOffset >= 0 {
			if crLen == 1 && ps.block.WinLineEndings {
				return 0, ErrInconsistentLineEndings
			}
			if crOffset > 0 {
				return 0, nil
			}
		}
		if crOffset == -1 && !eof {
			return -1, nil
		}

		ps.block.EndLine = ps.lineNumber
		ps.block = nil
		ps.lineNumber++

		ps.downstream = ps.newSectionStream()
		ps.mode = gapMode

		return expectedLength + len(tws) + crLen, io.EOF
	}
}
