package ca

import (
	"crypto"
	"crypto/x509"
	"gitlab.com/xianic/xca/prompt"
	"io"
	"sort"
	"sync"
)

type DriverFactory interface {
	String() string
	NewDriverConfig() any
	NewDriver(caConfig *Config, driverConfig any) (Driver, error)
}

type Driver interface {
	io.Closer

	// InitPrivateKey should create a new private key only if one does not
	// already exist. If one does exist it should return the public part and
	// a signer. This will be called once when a CA is created.
	InitPrivateKey(prompt prompt.PasswordPrompt) (publicKey crypto.PublicKey, certCustomiser CertCustomiser, signer crypto.Signer, err error)

	// GetSigner returns the signer that this driver stores. It may use the prompt
	// to get a pin or passphrase during the call to GetSigner, or after it has
	// returned during a call to methods on the returned signer.
	GetSigner(certificate *x509.Certificate, prompt prompt.PasswordPrompt) (crypto.Signer, error)

	// SetCertificate is called a single time on completion of the CA's
	// initialisation. If the driver stores certificates then this is the time
	// to save it. It does not need to store the certificate.
	SetCertificate(certificate *x509.Certificate, prompt prompt.PasswordPrompt) error
}

var (
	drivers   map[string]DriverFactory
	driverMux sync.Mutex
)

func AddDriver(name string, driver DriverFactory) {
	driverMux.Lock()
	defer driverMux.Unlock()

	if drivers == nil {
		drivers = make(map[string]DriverFactory)
	}

	drivers[name] = driver
}

func GetDriverFactory(name string) DriverFactory {
	driverMux.Lock()
	defer driverMux.Unlock()

	return drivers[name]
}

func Names() []string {
	driverMux.Lock()
	defer driverMux.Unlock()

	names := make([]string, 0, len(drivers))
	for k, _ := range drivers {
		names = append(names, k)
	}

	sort.Strings(names)
	return names
}
