package pemstream

import (
	"encoding/base64"
	"io"
)

type base64Decoder struct {
	upstream io.ReadCloser

	// when required buffer characters to be base64 decoded
	inBuf []byte

	// when required buffer decoded bytes before returning them to the caller
	outBuf []byte

	// if Read() is called with a buffer smaller than this then read into this
	// buffer instead.
	lineBuf []byte

	// true when a bas64 padding char is detected in the stream
	encodingEnded bool

	// the current parser state
	mode b64DecodeMode

	// The PEM block currently being parsed. The base64 line length has to be
	//set and enforced
	block *PemBlock

	// number of base64 characters read on this line so far.
	b64LenThisLine int

	// any error that will terminate all parsing.
	err error
}

func setUpDecodingStream(ps *PemReader) {
	ss := ps.newSectionStream()
	ps.downstream = &base64Decoder{
		upstream: ss,
		inBuf:    make([]byte, 0, 8),
		outBuf:   make([]byte, 0, 8),
		lineBuf:  make([]byte, 0, 80),
		mode:     b64DecodeStart,
		block:    ps.block,
	}
}

func (b *base64Decoder) Read(p []byte) (n int, err error) {
	if err = b.Err(); err != nil || len(p) == 0 {
		return
	}

	for {
		n, err = b.flushBuffers(p, b.err == io.EOF)
		if n != 0 || err != nil {
			return
		}

		var rdb *[]byte
		if len(b.lineBuf) != 0 {
			// If there is anything in the line buffer then send it through
			// the parser
			rdb = &b.lineBuf
		} else {
			// If p is small the decoding becomes really inefficient. Read
			// into b.lineBuf instead.
			if cap(b.lineBuf) > len(p) {
				n, err = b.upstream.Read(bufSpareCap(b.lineBuf))
				bufGrowLen(&b.lineBuf, n)
				rdb = &b.lineBuf
			} else {
				n, err = b.upstream.Read(p)
				q := p[:n]
				rdb = &q
			}
			if err != nil {
				b.err = err
				if err != io.EOF {
					return 0, err
				}
			}
		}

		rdOff := 0
		wrOff := 0
		eof := err == io.EOF
		err = nil
		for err == nil {
			err = b.mode(b, *rdb, p, &rdOff, &wrOff)
			if err == io.ErrUnexpectedEOF && !eof {
				err = nil
				break
			}
		}

		if rdb == &b.lineBuf {
			bufConsume(rdb, rdOff)
		}

		if err == errExpectedEOF ||
			(err == io.ErrUnexpectedEOF && (len(b.inBuf) != 0 || len(b.outBuf) != 0)) {
			err = nil
		}

		if err != nil {
			b.err = err
		}

		if wrOff > 0 || b.Err() != nil {
			return wrOff, b.Err()
		}
	}
}

func (b *base64Decoder) Close() error {
	return b.upstream.Close()
}

func (b *base64Decoder) Err() error {
	if b.err == io.EOF && (len(b.inBuf) != 0 || len(b.outBuf) != 0 || len(b.lineBuf) != 0) {
		return nil
	}

	return b.err
}

func (b *base64Decoder) flushBuffers(into []byte, force bool) (n int, err error) {
	// return the contents of the output buffer if there is any
	if len(b.outBuf) != 0 {
		n = copy(into, b.outBuf)
		bufConsume(&b.outBuf, n)
		return
	}

	// the output buffer is empty
	if len(b.inBuf) == 0 {
		// there is nothing to decode either
		return
	}

	// only decode the input buffer if it is full or force is true
	if len(b.inBuf) != cap(b.inBuf) && !force {
		return
	}

	decodeLen := base64.StdEncoding.DecodedLen(len(b.inBuf))
	if len(into) >= decodeLen {
		n, err = base64.StdEncoding.Decode(into, b.inBuf)
	} else {
		to := bufExpand(&b.outBuf, decodeLen, 8)
		n, err = base64.StdEncoding.Decode(to, b.inBuf)
		bufGrowLen(&b.outBuf, n)
		n = 0
	}
	if err == nil {
		b.inBuf = b.inBuf[:0]
	}

	return
}
