package im

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
)

var (
	Cmd = &cobra.Command{
		Use:     "import [command]",
		Short:   "import keys or certificates onto a Yubikey",
		Aliases: []string{"im"},
	}
)

func init() {
	ykpiv.Cmd.AddCommand(Cmd)
}
