package cmd_gen

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
)

var Cmd = &cobra.Command{
	Use:   "gen",
	Short: "Generate a key or CSR",
}

func init() {
	cmd.RootCmd.AddCommand(Cmd)
}
