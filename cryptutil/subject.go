package cryptutil

/*
Thanks to @tgo and @Trane9991 at
https://stackoverflow.com/questions/39125873/golang-subject-dn-from-x509-cert
for inspiration for the code in this file.
*/

import (
	"bufio"
	"crypto/x509/pkix"
	"encoding/asn1"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type nameOid struct {
	Oid     asn1.ObjectIdentifier
	Aliases []string
	SetName func(name *pkix.Name, value string)
}

func appendInit(slice *[]string, elements ...string) {
	if *slice == nil {
		*slice = make([]string, 0, len(elements))
	}

	*slice = append(*slice, elements...)
}

var (
	escapeRegex   = regexp.MustCompile(`([\\/=])`)
	unescapeRegex = regexp.MustCompile(`\\([\\/=])`)
	oidFileds     = []nameOid{
		{Oid: []int{2, 5, 4, 3}, Aliases: []string{"CN"}, SetName: func(n *pkix.Name, v string) { n.CommonName = v }},
		{Oid: []int{2, 5, 4, 4}, Aliases: []string{"SN"}},
		{Oid: []int{2, 5, 4, 5}, Aliases: []string{"serialNumber", "SERIALNUMBER"}, SetName: func(n *pkix.Name, v string) { n.SerialNumber = v }},
		{Oid: []int{2, 5, 4, 6}, Aliases: []string{"C"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.Country, v) }},
		{Oid: []int{2, 5, 4, 7}, Aliases: []string{"L"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.Locality, v) }},
		{Oid: []int{2, 5, 4, 8}, Aliases: []string{"ST"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.Province, v) }},
		{Oid: []int{2, 5, 4, 9}, Aliases: []string{"streetAddress", "STREET"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.StreetAddress, v) }},
		{Oid: []int{2, 5, 4, 10}, Aliases: []string{"O"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.Organization, v) }},
		{Oid: []int{2, 5, 4, 11}, Aliases: []string{"OU"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.OrganizationalUnit, v) }},
		{Oid: []int{2, 5, 4, 12}, Aliases: []string{"title"}},
		{Oid: []int{2, 5, 4, 17}, Aliases: []string{"postalCode", "POSTALCODE"}, SetName: func(n *pkix.Name, v string) { appendInit(&n.PostalCode, v) }},
		{Oid: []int{2, 5, 4, 42}, Aliases: []string{"GN"}},
		{Oid: []int{2, 5, 4, 43}, Aliases: []string{"initials"}},
		{Oid: []int{2, 5, 4, 44}, Aliases: []string{"generationQualifier"}},
		{Oid: []int{2, 5, 4, 46}, Aliases: []string{"dnQualifier"}},
		{Oid: []int{2, 5, 4, 65}, Aliases: []string{"pseudonym"}},
		{Oid: []int{0, 9, 2342, 19200300, 100, 1, 25}, Aliases: []string{"DC"}},
		{Oid: []int{1, 2, 840, 113549, 1, 9, 1}, Aliases: []string{"emailAddress"}},
		{Oid: []int{0, 9, 2342, 19200300, 100, 1, 1}, Aliases: []string{"userid"}},
	}
	fieldsByOid   map[string]*nameOid
	fieldsByAlias map[string]*nameOid
)

func init() {
	fieldsByOid = make(map[string]*nameOid)
	fieldsByAlias = make(map[string]*nameOid)
	for i, of := range oidFileds {
		fieldsByOid[of.Oid.String()] = &oidFileds[i]
		for _, alias := range of.Aliases {
			fieldsByAlias[alias] = &oidFileds[i]
		}
	}
}

/*
NameAsString renders a pkix.Name as a string in the form /OID=Value/…. The
first character will be a "/" and OID-value pairs will be separated with a
"/". The characters / = \ which are part of the OID or value will be escaped
by placing a \ before them. Whereas this function will generate a string
representation of the name for all names, the string value of attributes that
are not strings may not contain enough information to reverse the process.
*/
func NameAsString(name *pkix.Name) string {
	builder := strings.Builder{}
	for _, att := range name.Names {
		attName := att.Type.String()
		if field := fieldsByOid[attName]; field != nil && len(field.Aliases) > 0 {
			attName = field.Aliases[0]
		}
		var value string
		switch t := att.Value.(type) {
		case string:
			value = t
		case fmt.Stringer:
			value = t.String()
		default:
			t = fmt.Sprintf("%v", value)
		}

		builder.WriteString("/")
		builder.WriteString(EscapeName(attName))
		builder.WriteString("=")
		builder.WriteString(EscapeName(value))
	}

	return builder.String()
}

func EscapeName(name string) string {
	return escapeRegex.ReplaceAllString(name, "\\$1")
}

func UnescapeName(name string) string {
	return unescapeRegex.ReplaceAllString(name, "$1")
}

/*
PkixNameSplitter is a bufio.SplitFunc for use with bufio.Scanner which tokens
in the form "/OID" or "=value". Escaped characters will not cause a split. No
unesscaping is performed, the user of the Scanner is expected to inspect the
first character of the token, remove it, then unescape the rest with
UnescapeName().
*/
func PkixNameSplitter(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF {
		// if no more data is coming then what we have is the remaining token.
		if len(data) > 0 {
			return len(data), data, nil
		} else {
			return 0, nil, nil
		}
	}

	/*
		The first character of the token indicates its type and is to be returned
		as is. Skip it and search the remainder for an unescaped sequence
		terminator.
	*/

	i := 1
	lastEscape := -1
	foundTerminator := false
	for ; i < len(data) && !foundTerminator; i++ {
		switch data[i] {
		case '\\':
			lastEscape = i
		case '/':
			fallthrough
		case '=':
			if lastEscape != i-1 {
				foundTerminator = true
			}
		}
	}
	i--

	if !foundTerminator {
		// need more data
		return 0, nil, nil
	}

	return i, data[0:i], nil
}

func StringAsName(str string) (*pkix.Name, error) {
	scanner := bufio.NewScanner(strings.NewReader(str))
	scanner.Split(PkixNameSplitter)

	name := new(pkix.Name)
	var lastOidStr string
	for scanner.Scan() {
		token := scanner.Text()
		if token == "" {
			return nil, errors.New("empty token")
		}

		if lastOidStr == "" {
			if token[0] == '/' {
				lastOidStr = UnescapeName(token[1:])
				continue
			} else {
				return nil, errors.New("expecting /")
			}
		}

		if token[0] != '=' {
			return nil, errors.New("expecting =")
		}

		value := UnescapeName(token[1:])
		var oid asn1.ObjectIdentifier
		var field *nameOid
		if field = fieldsByAlias[lastOidStr]; field != nil {
			oid = field.Oid
		} else {
			var err error
			oid, err = parseOid(lastOidStr)
			if err != nil {
				return nil, err
			}
			field = fieldsByOid[oid.String()]
		}
		if field != nil && field.SetName != nil {
			field.SetName(name, value)
		} else {
			if name.ExtraNames == nil {
				name.ExtraNames = make([]pkix.AttributeTypeAndValue, 0, 1)
			}
			name.ExtraNames = append(name.Names, pkix.AttributeTypeAndValue{
				Type:  oid,
				Value: value,
			})
		}

		lastOidStr = ""
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return name, nil
}

func parseOid(str string) (asn1.ObjectIdentifier, error) {
	intStrs := strings.Split(str, ".")
	oid := make([]int, len(intStrs))
	for i, intStr := range intStrs {
		parsed, err := strconv.ParseUint(intStr, 10, 32)
		if err != nil {
			return nil, err
		}
		oid[i] = int(parsed)
	}

	return oid, nil
}
