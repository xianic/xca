package util

import (
	"errors"
	"fmt"
	"io"
	"os"

	"golang.org/x/crypto/ssh/terminal"
)

type DelayOpenReadOption func(dor *delayOpenFile)
type DelayOpenWriteOption func(dow *delayOpenFile)

type delayOpenFile struct {
	opened bool
	path   string
	mask   os.FileMode

	reader io.Reader
	writer io.Writer
	closer io.Closer

	prompt string
}

// AllowStdIn will cause the returned ReaderCloser to read from os.StdIn if
// the file name was "-". os.StdIn will not be closed when the ReadCloser is
// Close()d
func AllowStdIn(do *delayOpenFile) {
	if do.path == "-" {
		do.path = ""
		do.reader = os.Stdin
	}
}

// AllowStdInPrompt behaves like AllowStdIn with addition that if and only if
// os.Stdin is a terminal then the prompt is written to os.StdErr upon the
// first call to Read(). The prompt has the suffix instructing the user to
// finish with Ctrl-D
func AllowStdInPrompt(prompt string) DelayOpenReadOption {
	return func(do *delayOpenFile) {
		if do.path == "-" {
			do.path = ""
			do.reader = os.Stdin
			do.prompt = fmt.Sprintf("%s (end with Enter, Ctrl-D)\n", prompt)
		}
	}
}

// AllowStdout will cause the returned WriteCloser to write to os.StdOut if
// the file name was "-". os.StdOut will not be closed when the WriteCloser is
// Close()d
func AllowStdout(dow *delayOpenFile) {
	if dow.path == "-" {
		dow.path = ""
		dow.writer = os.Stdout
	}
}

func (d *delayOpenFile) Read(p []byte) (int, error) {
	if d.prompt != "" {
		if terminal.IsTerminal(int(os.Stdin.Fd())) {
			_, _ = os.Stderr.Write([]byte(d.prompt))
		}
		d.prompt = ""
	}

	if d.reader == nil {
		if d.opened {
			return 0, errors.New("read from closed stream")
		}

		d.opened = true
		file, err := os.Open(d.path)
		if err != nil {
			return 0, fmt.Errorf("open %s: %w", d.path, err)
		}

		d.reader = file
		d.closer = file
		d.path = ""

	}

	return d.reader.Read(p)
}

func (d *delayOpenFile) Write(b []byte) (int, error) {
	if d.writer == nil {
		if d.path == "" {
			return 0, errors.New("write to closed stream")
		}
		file, err := os.OpenFile(d.path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, d.mask)
		if err != nil {
			return 0, fmt.Errorf("open %s: %w", d.path, err)
		}

		d.writer = file
		d.closer = file
		d.path = ""
	}

	return d.writer.Write(b)
}

func (d *delayOpenFile) Close() (err error) {
	// don't allow it to be opened it isn't already open
	d.path = ""

	// close it if it is open
	if d.closer != nil {
		err = d.closer.Close()
	}

	d.closer = nil
	d.reader = nil
	d.writer = nil

	return nil
}

// DelayOpenRead returns an io.ReadCloser which will open the given file for
// reading upon the first call to Read().
func DelayOpenRead(path string, options ...DelayOpenReadOption) io.ReadCloser {
	dor := &delayOpenFile{path: path}
	for _, option := range options {
		option(dor)
	}
	return dor
}

// DelayOpenWrite returns an io.WriteCloser that will open the given file for
// writing upon the first call to Write(). The file will be created (or
// truncated if it already exists) and the mode will be 0666 or 0600 depending
// on whether sensitive is true. The process umask will likely convert that to
// something stricter.
func DelayOpenWrite(path string, sensitive bool, options ...DelayOpenWriteOption) io.WriteCloser {
	out := &delayOpenFile{
		path: path,
	}

	if sensitive {
		out.mask = 0600
	} else {
		out.mask = 0666
	}

	for _, option := range options {
		option(out)
	}

	return out
}

// ReadAll opens a file, reads all the contents, closes the file, and then
// returns the contents. This allows the use of DelayOpenReadOption(s) with
// io.ReadAll()
func ReadAll(path string, options ...DelayOpenReadOption) (data []byte, err error) {
	in := DelayOpenRead(path, options...)
	defer DeferClose(in.Close, &err)

	data, err = io.ReadAll(in)
	return
}

func WriteAll(path string, data []byte, sensitive bool, options ...DelayOpenWriteOption) (err error) {
	out := DelayOpenWrite(path, sensitive, options...)
	defer DeferClose(out.Close, &err)
	_, err = out.Write(data)
	return
}

func RemoveFileIfEmpty(path string, retErr *error) {
	stat, err := os.Stat(path)
	if err != nil {
		if retErr != nil && *retErr == nil {
			*retErr = err
		}
		return
	}

	if stat.Size() > 0 {
		return
	}

	err = os.Remove(path)
	if err != nil && retErr != nil && *retErr == nil {
		*retErr = err
	}

	return
}

func InitFile(path string, mode os.FileMode, creator func(w io.Writer) error) (created bool, err error) {
	defer RemoveFileIfEmpty(path, &err)

	var file *os.File
	file, err = os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_EXCL, mode)
	if errors.Is(err, os.ErrExist) {
		// key exists, do not create it again
		err = nil
		return
	}
	if err != nil {
		return
	}
	defer DeferClose(file.Close, &err)

	err = creator(file)
	created = true

	return
}
