package cmd

import (
	"fmt"
	"gitlab.com/xianic/xca/prompt"
	"os"
	"runtime"
	"strconv"
	"strings"

	"github.com/pbnjay/memory"
	flag "github.com/spf13/pflag"
	"gitlab.com/xianic/xca/pemcrypt"
	"gitlab.com/xianic/xca/util"
)

type PromptFlag string

func (f *PromptFlag) Init(fs *flag.FlagSet, longName, shortName, defaultValue, forDesc string) {
	fs.StringVarP(
		(*string)(f),
		longName,
		shortName,
		defaultValue,
		"where to get the "+forDesc+" from. "+
			"'-' for stdin (asked once), "+
			"'.' for terminal (asked twice), "+
			"'@' to disable prompting, "+
			"or a path to a file to use the first line of that file",
	)
}

func (f *PromptFlag) Prompt() prompt.PasswordPrompt {
	switch *f {
	case "":
		return nil
	case "-":
		return prompt.PromptFromStream(os.Stdin)
	case ".":
		return prompt.PromptFromTerminal()
	case "@":
		return prompt.PromptDisabled
	default:
		return prompt.PromptFromFile(string(*f))
	}
}

func (f *PromptFlag) IsSet() bool {
	return *f != "" && *f != "@"
}

type PemDecryptionFlags struct {
	KeyPassword PromptFlag
}

type PemEncryptionFlags struct {
	PemDecryptionFlags
	ArgonParams string
	YkSlot      int
}

func (f *PemDecryptionFlags) init(fs *flag.FlagSet, mode string) {
	f.KeyPassword.Init(fs, "password", "p", "", "password for pem file "+mode)
}

func (f *PemDecryptionFlags) Init(fs *flag.FlagSet) {
	f.init(fs, "decryption")
}

func (f *PemEncryptionFlags) Init(fs *flag.FlagSet) {
	f.init(fs, "encryption")

	fs.StringVarP(
		&f.ArgonParams,
		"argon2",
		"",
		"5s",
		"parameters for argon2 key derivation function. Format: iterations|time\"s\"[,memory in MB[,threads]]",
	)

	fs.IntVarP(
		&f.YkSlot,
		"ykslot",
		"s",
		0,
		"Yubikey slot. 1 or 2 to enable key encryption with HMAC-SHA1",
	)
}

func (f *PemDecryptionFlags) ParseForDecryption() *pemcrypt.CryptOptions {
	co := &pemcrypt.CryptOptions{
		PasswordSource: f.KeyPassword.Prompt(),
	}

	if co.PasswordSource == nil {
		co.PasswordSource = prompt.PromptFromTerminal()
	}

	return co
}

func (f *PemEncryptionFlags) ParseForEncryption() (*pemcrypt.CryptOptions, error) {
	if f.YkSlot < 0 || f.YkSlot > 2 {
		return nil, fmt.Errorf("yubikey slot number %d invalid, use 1 or 2", f.YkSlot)
	}

	options := new(pemcrypt.CryptOptions)

	if f.KeyPassword != "" {
		options.PasswordSource = f.KeyPassword.Prompt()
	}

	if options.PasswordSource == prompt.PromptDisabled {
		options.PasswordSource = nil
	}
	if options.PasswordSource != nil {
		if err := f.parseArgon2Params(options); err != nil {
			return nil, err
		}
	}

	if f.YkSlot != 0 {
		options.YkHMAC.Slot = pemcrypt.YubiKeySlot(f.YkSlot)
	}

	return options, nil
}

func (f *PemEncryptionFlags) parseArgon2Params(options *pemcrypt.CryptOptions) (err error) {
	defer util.DeferWrap(&err, "parsing argon2 parameters %q: %w", f.ArgonParams)

	if options.PasswordParameters == nil {
		options.PasswordParameters = new(pemcrypt.PasswordParameters)
	}

	params := options.PasswordParameters
	var timeStr string
	var fields int
	fields, err = util.ParseFields(
		f.ArgonParams,
		":",
		0,
		&timeStr,
		&params.Memory,
		&params.Threads,
	)
	if err != nil {
		return err
	}

	if fields > 0 {
		params.TimeInSeconds = strings.HasSuffix(timeStr, "s")
		if params.TimeInSeconds {
			timeStr = timeStr[0 : len(timeStr)-1]
		}
		var parsed uint64
		parsed, err = strconv.ParseUint(timeStr, 10, 32)
		if err != nil {
			return
		}
		params.Time = uint32(parsed)
	}

	if fields < 2 {
		params.Memory = uint32(memory.TotalMemory() / (16 * 1024 * 1024))
	}

	if fields < 3 {
		if cpus := runtime.NumCPU(); cpus < 128 {
			params.Threads = uint8(runtime.NumCPU() * 2)
		} else {
			params.Threads = 255
		}
	}

	params.SetDefaults()

	return
}
