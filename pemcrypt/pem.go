package pemcrypt

import (
	"encoding/pem"
	"errors"
	"strings"
)

func Encrypt(block *pem.Block, options *CryptOptions, userContext string) error {
	if !options.EncryptionEnabled(OpEncrypt) {
		return nil
	}

	ciphertext, masterKey, err := encryptDataWithNewKey(block.Bytes)
	if err != nil {
		return err
	}

	if err = options.encryptMasterKey(masterKey, userContext); err != nil {
		return err
	}

	block.Type = PemPrefix + block.Type
	options.SetHeaders(&block.Headers, OpEncrypt)
	block.Bytes = ciphertext

	return nil
}

func Decrypt(block *pem.Block, options *CryptOptions, userContext string) error {
	if !strings.HasPrefix(block.Type, PemPrefix) {
		return errors.New("PEM block not encrypted")
	}

	err := options.ParseHeaders(block.Headers)
	if err != nil {
		return err
	}

	masterKey, err := options.decryptMasterKey(userContext)
	if err != nil {
		return err
	}

	plaintext, err := decryptData(block.Bytes, masterKey)
	if err != nil {
		return err
	}

	block.Type = block.Type[len(PemPrefix):]
	StripPemHeaders(block.Headers)
	block.Bytes = plaintext

	return nil
}
