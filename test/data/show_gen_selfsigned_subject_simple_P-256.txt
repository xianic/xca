===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		2DF2F5920B8C06484390C32493AB818B65EF37D3988E67C2AFC55A61483E1CE7 (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		5683EFD402CE18D5
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	B446FB91CFFC6421C2FE4F6F478EA1AAC0FF800594A557894F5FAB3CD88E454C
Version: 		3
Issuer:
	/CN=Hello World
	Common name:	Hello World
Subject:
	/CN=Hello World
	Common name:	Hello World
SANS:	(0 total)
Validity:
	Not before:	2023-09-02T20:18:22Z (5 minutes ago)
	Not after:	2033-08-30T20:23:22Z (10 years from now)
	Is CA cert:	false
	MaxPathLen:	unset
Key usage:		DigitalSignature, KeyEncipherment
Extended key usage:	ServerAuth, ClientAuth
Name constraints:	0 non-critical
