package ca

import (
	"crypto"
	"crypto/rand"
	_ "crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/prompt"
	"io"
	"math/big"

	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

type CertCustomiser func(certificate *x509.Certificate) error

func JoinCustomisers(customisers ...CertCustomiser) CertCustomiser {
	return func(certificate *x509.Certificate) error {
		for _, c := range customisers {
			if c == nil {
				continue
			}

			if err := c(certificate); err != nil {
				return err
			}
		}

		return nil
	}
}

func (c *CA) initCert(pubKey crypto.PublicKey, cust CertCustomiser, p prompt.PasswordPrompt) (created bool, err error) {
	return util.InitFile(
		c.Config.CertificateFilePath(),
		0666,
		func(w io.Writer) error {
			if err := c.loadSigner(); err != nil {
				return err
			}

			sigAlgo := cryptutil.SignatureAlgorithmForKey(pubKey, cryptutil.DefaultSignatureHash)
			if sigAlgo == x509.UnknownSignatureAlgorithm {
				return fmt.Errorf("don't know how to create a signature using key of type %T", pubKey)
			}

			serial := make([]byte, 8)
			if _, err = rand.Read(serial[0:4]); err != nil {
				return err
			}

			skid := cryptutil.SubjectKeyId(pubKey)
			if len(skid) == 0 {
				return fmt.Errorf("failed to generate subject key ID")
			}

			csr := &x509.Certificate{
				SerialNumber: new(big.Int).SetBytes(serial),
				Subject: pkix.Name{
					CommonName: fmt.Sprintf("Example CA %X", serial[0:4]),
				},

				KeyUsage:           x509.KeyUsageDigitalSignature | x509.KeyUsageCRLSign | x509.KeyUsageCertSign,
				IsCA:               true,
				SignatureAlgorithm: sigAlgo,
				SubjectKeyId:       skid,
				AuthorityKeyId:     skid,
			}
			if cust != nil {
				if err = cust(csr); err != nil {
					return err
				}
			}

			certDer, err := x509.CreateCertificate(rand.Reader, csr, csr, pubKey, c.signer)
			if err != nil {
				return fmt.Errorf("creating CA certificate: %w", err)
			}

			c.certificate, err = x509.ParseCertificate(certDer)
			if err != nil {
				return fmt.Errorf("parsing generated certificate: %w", err)
			}

			if err = c.driver.SetCertificate(c.certificate, p); err != nil {
				return err
			}

			pemBytes := pem.EncodeToMemory(&pem.Block{
				Type:  cryptutil.PemTypeCertificate,
				Bytes: certDer,
			})

			if _, err = w.Write(pemBytes); err != nil {
				return err
			}

			return nil
		},
	)
}

func (c *CA) loadCertificate() error {
	if c.certificate != nil {
		return nil
	}

	cert, err := cryptutil.UnmarshalFile(c.Config.CertificateFilePath(), "", nil).ExpectCertificate()
	if err != nil {
		return err
	}

	if len(cert.SerialNumber.Bytes()) < 8 {
		return errors.New("serial number of CA certificate is too short (less tha 8 bytes)")
	}

	if !cert.IsCA {
		return errors.New("certificate is not a CA certificate")
	}

	c.certificate = cert

	return nil
}

func (c *CA) loadSigner() error {
	if c.signer != nil {
		if c.log != nil {
			c.log.SetKey(c.signer)
		}
		return nil
	}

	var err error
	c.signer, err = c.driver.GetSigner(c.certificate, c.prompt)
	if err != nil {
		c.signer = nil
		return fmt.Errorf("loading CA private key: %w", err)
	}

	if c.log != nil {
		c.log.SetKey(c.signer)
	}

	return nil
}
