package cmd_init

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_ca"
	"gitlab.com/xianic/xca/util"
)

var Cmd = &cobra.Command{
	Use:   "init name",
	Short: "Create a CA",
	Long: `Create a CA. The CA is created in several stages, and stages that are already
complete will be skipped. The stages are:

1. Read the config file if it exists. The file specified with -C, or a file
   named ca.conf in the directory specified with -C. An absent config file is
   the same as a config file with all of its values set to the default.
2. Create the CA private key. By default this is a Curve 25519 key. If a
   different kind of key is required then create the key file ahead of
   initialising the CA. The default name for the key is ca.key, and alternate
   keys can be crated with 'xca gen rsa' or 'xca gen ec'.
3. Create the CA certificate. This will be a root certificate, see options to
   customise its creation below. If an intermediate is required or the
   customisation are not sufficient, then:
     1. Create a CA key by hand,
     2. Create a CSR from that key,
     3. Sign that CSR and save the resulting certificate as 'ca.pem'.
4. Initialise the audit log. The log is signed with the CA key and so must
   be initialised. If the log exists and the CA is to be re-initialised, then
   delete the log first.
`,
	Args: func(_ *cobra.Command, _ []string) error {
		return certFlags.CheckPopulate()
	},
	RunE: caInitCmdFunc,
}

var (
	certFlags     cmd.CertFlags
	decryptPrompt cmd.PromptFlag
)

func init() {
	fs := Cmd.Flags()

	cmd.AddLateInit(func() {
		driverNames := ca.Names()
		Cmd.Long += "\nDRIVERS\n"
		for _, name := range driverNames {
			df := ca.GetDriverFactory(name)
			Cmd.Long += fmt.Sprintf("%-8s %s\n", name, df.String())
		}
	})

	cmd_ca.Cmd.AddCommand(Cmd)

	certFlags.Init(fs, cmd.CertFlagsCACert)
	decryptPrompt.Init(fs, "pass", "p", ".", "pin or passphrase to decrypt CA")
}

func caInitCmdFunc(cmd *cobra.Command, args []string) error {
	ctx, cancel := context.WithTimeout(context.Background(), cmd_ca.OptOpenTimeout)
	defer cancel()

	c, err := ca.CreateCA(ctx, cmd_ca.OptDir, certFlags.PopulateCert, decryptPrompt.Prompt())
	if err != nil {
		return fmt.Errorf("initialising CA: %w", err)
	}
	defer util.DeferClose(c.Close, &err)

	return nil
}
