package cmd_selfsigned

import (
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"math/big"

	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"

	"github.com/spf13/cobra"
)

var (
	Cmd = &cobra.Command{
		Use:   "selfsigned [flags] key_file certificate_file",
		Short: "Generate a self signed certificate",
		Long:  `Generate a self signed certificate`,
		Args:  selfsignedArgValidator,
		RunE:  selfsignedCmdFunc,
	}

	selfsignedCmdFlags cmd.CertFlags
	encFlags           cmd.PemDecryptionFlags
)

func init() {
	cmd_gen.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selfsignedCmdFlags.Init(fs, cmd.CertFlagsSelfSign)
	encFlags.Init(fs)
}

func selfsignedArgValidator(cmd *cobra.Command, args []string) error {
	if selfsignedCmdFlags.Subject == "" && len(selfsignedCmdFlags.DnsSans) == 0 {
		return errors.New("at least one of --subject or --host must be specified")
	}

	return cobra.ExactArgs(2)(cmd, args)
}

func selfsignedCmdFunc(cmd *cobra.Command, args []string) error {
	privKey, err := cryptutil.UnmarshalFile(args[0], "Enter PEM encoded private key", encFlags.ParseForDecryption()).ExpectPrivateKey()
	if err != nil {
		return err
	}

	pubKey := cryptutil.PublicKeyFor(privKey)
	if pubKey == nil {
		return fmt.Errorf("could not get public key for private key of type %T", privKey)
	}

	serialBytes := make([]byte, 8)
	if _, err := rand.Read(serialBytes); err != nil {
		return fmt.Errorf("reading random data: %w", err)
	}

	signatureAlgorithm := cryptutil.SignatureAlgorithmForKey(pubKey, cryptutil.DefaultSignatureHash)
	template := &x509.Certificate{
		SerialNumber:       new(big.Int).SetBytes(serialBytes),
		SignatureAlgorithm: signatureAlgorithm,
	}

	if err := selfsignedCmdFlags.PopulateCert(template); err != nil {
		return err
	}

	newCert, err := x509.CreateCertificate(rand.Reader, template, template, pubKey, privKey)
	if err != nil {
		return fmt.Errorf("genrating certificate: %w", err)
	}

	encoded := pem.EncodeToMemory(&pem.Block{
		Type:  cryptutil.PemTypeCertificate,
		Bytes: newCert,
	})

	if err := util.WriteAll(args[1], encoded, false, util.AllowStdout); err != nil {
		return fmt.Errorf("writing new certificate: %w", err)
	}

	return nil
}
