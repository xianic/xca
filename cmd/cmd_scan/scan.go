package cmd_scan

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"gitlab.com/xianic/xca/util"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/scanner"
	"gitlab.com/xianic/xca/scanner/pkimodel"
)

// Cmd represents the show command
var (
	Cmd = &cobra.Command{
		Use:   "scan [file|directory]...",
		Short: "Scan for certificates and keys, display and renew them",
		Long: `Scan searches each file or directory tree given on the command line for
certificates and keys. If no arguments are given then . is searched. After
the search an attempt is made to build chains out of the certificates found,
then a report is written to standard output.

REPORTS
leaves:       List all leaf certificates found along with their chain.
none:         Don't output anything. Useful for checking for errors during the
              scan.
all:          Show all certificates found in a list.
ca:           List all CAs that have issued something along with all the
              certificates each CA has issued.
expiring:     List certificates that will expire in less than -d days time.
orphan-ca:    List all the CAs that have not issued any certificates.
selfsigned:   List all self signed certificates.
`,
		RunE: scanCmdFunc,
	}

	scanTimeout     time.Duration
	ignoreErrors    bool
	report          string
	thresholdInDays int
)

func init() {
	cmd.RootCmd.AddCommand(Cmd)

	flags := Cmd.Flags()

	flags.DurationVarP(
		&scanTimeout,
		"scan-timeout",
		"t",
		time.Duration(5)*time.Minute,
		"timeout for scanning files and opening CAs. Set to zero to disable.",
	)
	flags.BoolVarP(&ignoreErrors, "ignore-errors", "i", false, "ignore errors where possible and continue")
	flags.StringVarP(&report, "report", "r", "leaves", "how to display the result of the scan, see REPORTS above")
	flags.IntVarP(&thresholdInDays, "days", "d", 30, "certificate with less than this number of days left will be considered expiring")
}

func scanCmdFunc(_ *cobra.Command, args []string) (err error) {
	reporter := reporters[report]
	if reporter == nil {
		return fmt.Errorf("no report named %q, see -h", report)
	}

	pc := pkimodel.NewCollection()
	defer util.DeferClose(pc.Close, &err)

	ctx := context.Background()
	if scanTimeout > 0 {
		var cancel func()
		ctx, cancel = context.WithTimeout(ctx, scanTimeout)
		defer cancel()
	}

	var errorStream chan error
	if ignoreErrors {
		errorStream = make(chan error)
		termSignal := sync.WaitGroup{}
		termSignal.Add(1)
		go func() {
			defer func() {
				termSignal.Done()
			}()
			for e := range errorStream {
				_, _ = fmt.Fprintf(os.Stderr, "ignored error: %s\n", e)
			}
		}()
		defer func() {
			close(errorStream)
			termSignal.Wait()
		}()
	}
	if len(args) == 0 {
		args = []string{"."}
	}
	for _, arg := range args {
		err = scanner.Scan(ctx, pc, arg, errorStream)
		if err != nil {
			return err
		}
	}

	if err := pc.BuildChains(errorStream); err != nil {
		return err
	}

	return reporter(pc)
}
