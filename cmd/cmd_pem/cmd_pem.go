package cmd_pem

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
)

var Cmd = &cobra.Command{
	Use:   "pem",
	Short: "PEM encoding and encryption commands",
}

func init() {
	cmd.RootCmd.AddCommand(Cmd)
}
