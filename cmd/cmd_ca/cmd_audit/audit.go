package cmd_audit

import (
	"crypto"
	"fmt"
	"gitlab.com/xianic/xca/cmd"
	"os"
	"strings"
	"time"

	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd/cmd_ca"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/siglog"
	"gitlab.com/xianic/xca/util"

	"github.com/spf13/cobra"
)

var (
	Cmd = &cobra.Command{
		Use:   "audit [-k key] [-q] [-s serial]... [files]",
		Short: "Verify audit logs",
		Long: `Read, verify, and print audit logs. If no files are given on the command line
then the audit log from the CA is verified. Otherwise each file on the command
line is checked in turn and each is also checked that it is a continuation
from the file before. To avoid errors, list the files in the order they were
produces, oldest first.

The key used to verify the audit log, by default, is loaded from the
certificate of the CA, however this can be overridden with the --key switch.
A certificate, public key, or private key can be supplied with this option
and the public key of the given object will be used. Audit logs are required
to contain the CA certificate as the first PEM block in the log, so if a log
only needs to be check for self consistency (and not verified against a key
held in another file) then the audit log itself can also be passed as an 
argument to --key.

If --key is specified and at least one file is specified on the command line
then no attempt will be made to load the CA.
`,
		RunE: auditCmdFunc,
	}

	keyFile   string
	quiet     bool
	serials   []string
	cryptOpts cmd.PemDecryptionFlags
)

func init() {
	cmd_ca.Cmd.AddCommand(Cmd)
	fs := Cmd.Flags()
	fs.StringVarP(&keyFile, "key", "k", "", "key to use if not using the CA's certificate")
	fs.BoolVarP(&quiet, "quiet", "q", false, "quiet - don't print any output")
	fs.StringSliceVarP(&serials, "serial", "s", make([]string, 0), "show only entry with given serial number")
	cryptOpts.Init(fs)
}

type auditPrinter struct {
	logRollMessageSeen bool
	currentFile        *string
	serialSet          map[string]struct{}
}

func (a *auditPrinter) CheckMessage(seq int, m *siglog.Message) (err error) {
	if len(a.serialSet) > 0 {
		serial := fmt.Sprintf("%X", m.Serial)
		if _, found := a.serialSet[serial]; !found {
			return nil
		}
	}

	fmt.Printf(
		"%04d %X %s ",
		seq,
		m.Serial,
		m.When.In(time.Local).Format(time.RFC3339),
	)

	switch m.What {
	case ca.LogMessageTypeRoll:
		err = a.checkRolled(seq, m)
	case ca.LogMessageTypeIssue:
		err = a.checkIssue(m)
	case ca.LogMessageTypeNote:
		err = a.checkNote(m)
	default:
		fmt.Printf("unknown type %q", m.What)
	}

	if err != nil {
		return err
	}

	if len(a.serialSet) > 0 {
		_, err = os.Stdout.Write(m.Payload)
		if err != nil {
			return err
		}
		if len(m.Payload) > 0 && m.Payload[len(m.Payload)-1] != '\n' {
			fmt.Println()
		}
	}

	return nil
}

func (a *auditPrinter) checkRolled(seq int, m *siglog.Message) error {
	if a.logRollMessageSeen {
		if seq != 0 {
			fmt.Printf("end of %s\n", *a.currentFile)
		} else {
			fmt.Printf("beginning of %s\n", *a.currentFile)
		}
		return nil
	}
	a.logRollMessageSeen = true

	cert, traces, err := ca.ParseLogRollMessage(m.Payload)
	if err != nil {
		return err
	}

	if a.currentFile != nil && *a.currentFile != "" {
		fmt.Printf(
			"log %s begins with root certificate %s\n",
			*a.currentFile,
			cryptutil.DescribeCertificate(cert),
		)
	} else {
		fmt.Printf(
			"log begins with root certificate %s\n",
			cryptutil.DescribeCertificate(cert),
		)
	}

	for _, trace := range traces {
		fmt.Printf("  %s\n", trace.Description)
	}

	return nil
}

func (a *auditPrinter) checkIssue(m *siglog.Message) error {
	cert, err := cryptutil.NewPemUnmarshaler(cryptutil.NewPemBuffer(m.Payload)).ExpectCertificate()
	if err != nil {
		return fmt.Errorf("parsing certificate from log message: %w", err)
	}

	fmt.Printf("issued %s\n", cryptutil.DescribeCertificate(cert))
	return nil
}

func (a *auditPrinter) checkNote(m *siglog.Message) error {
	fmt.Printf("note: ")
	message := strings.TrimSpace(string(m.Payload))
	if strings.Index(message, "\n") > 0 {
		fmt.Printf("\n")
	}
	fmt.Println(message)
	return nil
}

func auditCmdFunc(cmd *cobra.Command, args []string) (err error) {
	serialSet := make(map[string]struct{})
	for _, serial := range serials {
		serialSet[strings.ToUpper(serial)] = struct{}{}
	}

	var currentLogFile string
	var auditor siglog.MessageChecker
	if !quiet {
		auditor = &auditPrinter{
			currentFile: &currentLogFile,
			serialSet:   serialSet,
		}
	}

	var key crypto.PublicKey
	if keyFile != "" {
		key, err = loadAnyKey()
		if err != nil {
			return
		}
	}

	if key == nil || len(args) == 0 {
		var c *ca.CA
		if len(args) == 0 {
			c, err = cmd_ca.LoadCa(nil, auditor)
		} else {
			c, err = cmd_ca.LoadCa(nil, nil)
		}
		if err != nil {
			return
		}

		key = c.PublicKey()
	}

	auditor = siglog.NewMultiMessageChecker(&ca.LogAuditor{}, auditor)
	config := ca.LogConfigForKey(key)

	for _, file := range args {
		currentLogFile = file
		err = VerifyAuditFile(file, config, key, auditor)
		if err != nil {
			return
		}
	}

	return nil
}

func loadAnyKey() (crypto.PublicKey, error) {
	return cryptutil.UnmarshalFile(
		keyFile,
		"Enter PEM object containing a public key",
		cryptOpts.ParseForDecryption(),
	).ExpectPublicKeyExtractable()
}

func VerifyAuditFile(path string, config *siglog.LogStreamConfig, key crypto.PublicKey, auditor siglog.MessageChecker) (err error) {
	var file *os.File
	file, err = os.Open(path)
	if err != nil {
		return err
	}
	defer util.DeferClose(file.Close, &err)

	err = siglog.Verify(file, config, key, auditor)
	return
}
