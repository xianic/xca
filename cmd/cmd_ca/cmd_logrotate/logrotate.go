package cmd

import (
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd/cmd_ca"
	"gitlab.com/xianic/xca/util"
)

var (
	Cmd = &cobra.Command{
		Use:   "logrotate [copy_of_log]",
		Short: "Rotate audit log",
		Long: `Rotate the audit log by appending a message noting the rotation, writing the
full log to copy_of_log, truncating the current log, and finally writing a
message to the new log file.`,
		Args: cobra.RangeArgs(0, 1),
		RunE: caLogRotateCmdFunc,
	}

	promptFlag cmd_ca.CaPrompt
)

func init() {
	cmd_ca.Cmd.AddCommand(Cmd)
	promptFlag.Init(Cmd.Flags())
}

func caLogRotateCmdFunc(cmd *cobra.Command, args []string) (err error) {
	var c *ca.CA
	c, err = cmd_ca.LoadCa(promptFlag.Prompt(), nil)
	if err != nil {
		return fmt.Errorf("loading CA: %w", err)
	}

	var send io.WriteCloser
	if len(args) > 0 {
		send = util.DelayOpenWrite(args[0], false, util.AllowStdout)
		defer util.DeferClose(send.Close, &err)
	}

	if err = c.LogRotate(send); err != nil {
		return
	}

	return
}
