package prompt

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"

	"gitlab.com/xianic/xca/util"
	"golang.org/x/term"
)

// PasswordPrompt can retrieve a password from somewhere. userPrompt is some
// text that should be meaningful to the use i.e. a request for a password and
// path to a file being decrypted. if twice is true then the user will be
// prompted twice for the input and both values must batch or an error is
// returned. For prompt methods that do not involve a human the twice argument
// is ignored.
type PasswordPrompt interface {
	Prompt(userPrompt string, twice bool) ([]byte, error)
}

var (
	// DefaultPasswordPrompt is used if no prompt is specified on the command
	// line and one is required any of the options
	DefaultPasswordPrompt PasswordPrompt = PromptFromTerminal()

	ErrTwoDifferentPasswords = errors.New("entries do not match")
	ErrPromptDisabled        = errors.New("prompt disabled")

	// PromptDisabled prevents prompting for a password. May be useful
	// where there must be no user interaction.
	PromptDisabled PasswordPrompt = new(noPrompt)

	// terminalLock prevents reading two different terminal prompts attempting
	// to prompt the user at the same time
	terminalLock sync.Mutex
)

type noPrompt struct{}

func (_ *noPrompt) Prompt(_ string, _ bool) ([]byte, error) {
	return nil, ErrPromptDisabled
}

type streamPrompt struct {
	R io.Reader
}

func (s *streamPrompt) Prompt(_ string, _ bool) ([]byte, error) {
	return firstLineOfStream(s.R)
}

func PromptFromStream(r io.Reader) PasswordPrompt {
	return &streamPrompt{
		R: r,
	}
}

type filePrompt struct {
	path    string
	options []util.DelayOpenReadOption
}

func PromptFromFile(path string, options ...util.DelayOpenReadOption) PasswordPrompt {
	return &filePrompt{
		path:    path,
		options: options,
	}
}

func (fp *filePrompt) Prompt(_ string, _ bool) (password []byte, err error) {
	r := util.DelayOpenRead(fp.path, fp.options...)
	defer util.DeferClose(r.Close, &err)

	password, err = firstLineOfStream(r)
	return
}

func firstLineOfStream(r io.Reader) ([]byte, error) {
	lineReader := bufio.NewReader(r)
	line, err := lineReader.ReadString('\n')
	if err != nil && !(errors.Is(err, io.EOF) && len(line) > 0) {
		return nil, err
	}

	if strings.HasSuffix(line, "\n") {
		line = line[0 : len(line)-1]
		if strings.HasSuffix(line, "\r") {
			line = line[0 : len(line)-1]
		}
	}

	return []byte(line), nil
}

type terminalPrompt struct{}

func PromptFromTerminal() PasswordPrompt {
	return new(terminalPrompt)
}

func (_ *terminalPrompt) Prompt(text string, twice bool) (password []byte, err error) {
	terminalLock.Lock()
	defer terminalLock.Unlock()

	var file *os.File
	var closer func() error

	file, closer, err = findTerminal()
	if err != nil {
		return
	}
	if closer != nil {
		defer util.DeferClose(closer, &err)
	}

	_, _ = fmt.Fprintf(os.Stderr, "%s: ", text)
	password, err = term.ReadPassword(int(file.Fd()))
	_, _ = fmt.Fprintf(os.Stderr, "\n")
	if err != nil {
		return
	}

	if twice {
		var password2 []byte

		_, _ = fmt.Fprintf(os.Stderr, "%s (again): ", text)
		password2, err = term.ReadPassword(int(file.Fd()))
		_, _ = fmt.Fprintf(os.Stderr, "\n")

		if !bytes.Equal(password, password2) {
			return nil, ErrTwoDifferentPasswords
		}
	}

	return
}

func findTerminal() (file *os.File, closer func() error, err error) {
	file, err = os.Open("/dev/tty")
	if err == nil {
		if term.IsTerminal(int(file.Fd())) {
			closer = file.Close
			return
		}
	}
	err = nil

	if term.IsTerminal(0) {
		file = os.NewFile(0, "stdin")
		return
	}

	if term.IsTerminal(2) {
		file = os.NewFile(2, "stderr")
		return
	}

	return nil, nil, errors.New("no terminal found")
}

type BufferPrompt []byte

func PromptFromBuffer(b []byte) PasswordPrompt {
	p := new(BufferPrompt)
	*p = b
	return p
}

func PromptFromString(s string) PasswordPrompt {
	return PromptFromBuffer([]byte(s))
}

func (b *BufferPrompt) Prompt(_ string, _ bool) ([]byte, error) {
	return *b, nil
}
