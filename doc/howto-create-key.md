# How to create a public and private key pair

These instructions require [XCA to be installed](howto-get-xca.md).
Use a terminal to follow the examples on this page.

## How to create a key pair

To generate a key pair, run `xca gen key key.pem`,
this will create a key of the default type and save it in `key.pem`.
A file name of `-` can be used to write the key to standard output.
To generate a key of a particular type, add a `-t` switch:
`xca gen key -t rsa2048 key.pem` will generate a 2048-bit RSA key.
The type so key accepted are:

* **rsa«integer»** such as `rsa2048`, `rsa3072` and `rsa4096`, these will
  generate 2048-bit, 3072-bit, and 4096-bit RSA keys respectively.
  Any number of bits may be specified but not all numbers produce valid keys.
* **P-224**, **P-256**, **P-384**, **P-521** produce keys using the
  respective NIST curves.
* **25519** uses Curve25519

### Examples of Creating an Unencrypted Key Pair

`xca gen key key.pem` -
creates a key of the default type (P-256) and saves it to the file `key.pem`.

`xca gen key` or `xca gen key -` -
creates a key of the default type (P-256) and writes it to standard output.

`xca gen key -t rsa2048 key.pem` -
creates a 2048-bit RSA key and saves it to the file `key.pem`

## How to create an encrypted key

`xca` can encrypt the key saving it to a file or standard output.
This produces a PEM block specific to XCA and is not portable to other tools.
`xca` can use encrypted keys when with its own CA format or when creating
certificates.

Encryption is done with a password and/or a Yubikey using HMAC-SHA1.
If a password is to be used, use the `-p` switch with an argument of `.` to
read the password fromt the terminal (i.e. you type it)
If a YubiKey is to be used, use the `-s` switch.

Passwords are stretched with
[argon2](https://en.wikipedia.org/wiki/Argon2)
which uses significant resources and takes about 10-15 seconds by default.
The generated key pair is encrypted with AES256-GCM.

### Examples of Creating an Encrypted Key Pair

`xca gen key -p . key.pem` -
Ask for a password twice on the terminal and if the passwords match then
encrypt the generated key and write that to the file `key.pem`.
Echoing on the terminal will be disabled so that the password will not be
visible as it is typed.

`xca gen key -p - key.pem` -
Read one line from standard input and use that excluding the terminating
carriage return as the password. If standard in is the terminal the password
will be visible.

`xca gen key -p path/to/file key.pem` -
Read one line from the file `path/to/file` and use that excluding the
terminating carriage return as the password.

`xca gen key -s 1 key.pem` -
Use the program `ykchalresp` (which mast be installed) to query slot 1 of a
Yubikey, the result of which is used to encrypt the generated key pair.

`xca gen key -s 2 -p . key.pem` -
Ask for a password and use slot 2 of the Yubikey to encrypt the generated key
pair. Both the password and the yubikey will be needed to decrypt the key.
