package siglog

import (
	"bufio"
	"bytes"
	"context"
	"crypto"
	"errors"
	"fmt"
	"github.com/xianic/fslock"
	"io"
	"os"
	"time"
)

type LogStreamState int

const (
	NeedInit LogStreamState = iota
	NeetVerification
	Verified
	Closed

	MaxLogMessageSize = 10 * 1024 * 1024

	MessageTypeIssuance MessageType = "issuance"
	MessageTypeLogRoll  MessageType = "roll"
)

type LogStreamConfig struct {
	SignatureHash crypto.Hash
	SequenceHash  crypto.Hash
}

type LogStream struct {
	LogStreamConfig

	file  *os.File
	flock *fslock.Lock

	signer          crypto.Signer
	nextSerial      []byte
	lastMessageHash []byte
	state           LogStreamState
}

var (
	ErrNoKey       = errors.New("no log signing key")
	ErrClosed      = errors.New("log file closed")
	ErrNotVerified = errors.New("log not open or verified")
)

func NewLogStream(ctx context.Context, config *LogStreamConfig, path string, allowCreate bool) (l *LogStream, err error) {
	l = &LogStream{
		LogStreamConfig: *config,
	}

	flags := os.O_RDWR
	if allowCreate {
		flags |= os.O_CREATE
	}
	l.file, err = os.OpenFile(path, flags, 0666)
	if err != nil {
		return nil, fmt.Errorf("open log file: %w", err)
	}

	defer func() {
		if err != nil {
			_ = l.Close()
			l = nil
		}
	}()

	var isZeroLength bool
	isZeroLength, err = l.isZeroLength()
	if err != nil {
		return
	}
	if isZeroLength {
		l.state = NeedInit
	} else {
		l.state = NeetVerification
	}

	l.flock = fslock.New(path)

	if err = l.flock.LockWithContext(ctx); err != nil {
		return
	}

	return
}

func (l *LogStream) SetKey(s crypto.Signer) {
	l.signer = s
}

func (l *LogStream) Close() (err error) {
	l.state = Closed
	l.signer = nil

	if l.file == nil {
		return
	}

	err = l.file.Close()

	if err2 := l.flock.Unlock(); err == nil {
		err = err2
	}

	return
}

func (l *LogStream) Verify(pubKey any, checker MessageChecker) error {
	if _, err := l.file.Seek(0, 0); err != nil {
		return err
	}

	nextSerial, lastMesageHash, err := verify(l.file, &l.LogStreamConfig, pubKey, checker)
	if err != nil {
		return err
	}

	l.lastMessageHash = lastMesageHash
	l.nextSerial = nextSerial
	l.state = Verified
	return nil
}

func Verify(stream io.Reader, config *LogStreamConfig, pubKey any, checker MessageChecker) error {
	_, _, err := verify(stream, config, pubKey, checker)
	return err
}

func verify(stream io.Reader, config *LogStreamConfig, pubKey any, checker MessageChecker) (nextSerial, lastMessageHash []byte, err error) {
	br := bufio.NewReader(stream)
	sequenceNumber := 0
	var m *Message

	defer func() {
		if err != nil {
			if m == nil {
				err = fmt.Errorf("verifying log message %d: %w", sequenceNumber, err)
			} else {
				err = fmt.Errorf("verifying log message %d (serial %X): %w", sequenceNumber, m.Serial, err)
			}
		}
	}()

	for {
		m, err = ReadMessageFrom(br)
		if err != nil {
			return nil, nil, err
		}

		if m == nil {
			// no message returned with no error means end of log file on message boundary
			if lastMessageHash == nil {
				return nil, nil, errors.New("cannot verify empty log file")
			}

			return
		}

		if err = m.CheckSignature(pubKey, config.SignatureHash); err != nil {
			return nil, nil, fmt.Errorf("checking message signature: %w", err)
		}

		if m.Serial == nil {
			return nil, nil, errors.New("message has no serial number")
		}

		if nextSerial == nil {
			nextSerial = make([]byte, len(m.Serial))
			copy(nextSerial, m.Serial)
		} else {
			if !bytes.Equal(nextSerial, m.Serial) {
				return nil, nil, fmt.Errorf("expecting message with serial no %X but got %X", nextSerial, m.Serial)
			}
		}

		if lastMessageHash == nil {
			if m.LastMessageHash != nil {
				return nil, nil, errors.New("first message in log file references a previous message")
			}
		} else {
			if !bytes.Equal(lastMessageHash, m.LastMessageHash) {
				return nil, nil, errors.New("incorrect last message hash")
			}
		}

		incrementSerial(nextSerial)
		hasher := config.SequenceHash.New()
		_ = m.WriteMessageTo(hasher)
		lastMessageHash = hasher.Sum(nil)

		if checker != nil {
			if err := checker.CheckMessage(sequenceNumber, m); err != nil {
				return nil, nil, err
			}
		}

		sequenceNumber++
	}
}

func (l *LogStream) Init(t MessageType, message, serial []byte) error {
	if l.signer == nil {
		return ErrNoKey
	}

	if l.state != NeedInit {
		return errors.New("log file not ready to be initialised")
	}

	l.nextSerial = serial

	m := &Message{
		Serial:          nil,
		When:            time.Now(),
		What:            t,
		Length:          0,
		LastMessageHash: nil,
		Payload:         message,
		Signature:       nil,
	}

	m.Serial = make([]byte, len(l.nextSerial))
	copy(m.Serial, l.nextSerial)

	if l.lastMessageHash == nil {
		m.LastMessageHash = nil
	} else {
		m.LastMessageHash = make([]byte, len(l.lastMessageHash))
		copy(m.LastMessageHash, l.lastMessageHash)
	}

	if err := l.writeMessage(m); err != nil {
		return err
	}

	l.state = Verified
	incrementSerial(l.nextSerial)
	return nil
}

func (l *LogStream) Append(t MessageType, message []byte) error {
	if l.signer == nil {
		return ErrNoKey
	}

	if l.state != Verified {
		return ErrNotVerified
	}

	m := &Message{
		Serial:          nil,
		When:            time.Now(),
		What:            t,
		Length:          0,
		LastMessageHash: nil,
		Payload:         message,
		Signature:       nil,
	}

	m.Serial = make([]byte, len(l.nextSerial))
	copy(m.Serial, l.nextSerial)

	if l.lastMessageHash == nil {
		m.LastMessageHash = nil
	} else {
		m.LastMessageHash = make([]byte, len(l.lastMessageHash))
		copy(m.LastMessageHash, l.lastMessageHash)
	}

	if err := l.writeMessage(m); err != nil {
		return err
	}

	incrementSerial(l.nextSerial)
	return nil
}

func (l *LogStream) Rotate(t MessageType, message []byte, oldLog io.Writer) error {
	if l.state != Verified {
		return ErrNotVerified
	}

	m := &Message{
		Serial:          make([]byte, len(l.nextSerial)),
		When:            time.Now(),
		What:            t,
		Length:          0,
		LastMessageHash: l.lastMessageHash,
		Payload:         message,
		Signature:       nil,
	}
	copy(m.Serial, l.nextSerial)

	if err := l.writeMessage(m); err != nil {
		return err
	}

	if _, err := l.file.Seek(0, 0); err != nil {
		return err
	}

	if oldLog != nil {
		if _, err := io.Copy(oldLog, l.file); err != nil {
			return err
		}
	}

	if _, err := l.file.Seek(0, 0); err != nil {
		return err
	}

	if err := l.file.Truncate(0); err != nil {
		return err
	}

	l.lastMessageHash = nil
	m.LastMessageHash = nil
	if err := l.writeMessage(m); err != nil {
		return err
	}

	incrementSerial(l.nextSerial)
	return nil
}

func (l *LogStream) writeMessage(m *Message) error {
	if err := m.Sign(l.signer.(crypto.Signer), l.LogStreamConfig.SignatureHash); err != nil {
		return fmt.Errorf("signing log message: %w", err)
	}

	sequenceHasher := l.LogStreamConfig.SequenceHash.New()
	messageBuffer := &bytes.Buffer{}
	if err := m.WriteMessageTo(messageBuffer); err != nil {
		return fmt.Errorf("rendering log message: %w", err)
	}
	messageBytes := messageBuffer.Bytes()

	if _, err := l.file.Write(messageBytes); err != nil {
		return fmt.Errorf("writing to log file: %w", err)
	}

	sequenceHasher.Write(messageBytes)
	l.lastMessageHash = sequenceHasher.Sum(nil)

	return nil
}

func (l *LogStream) writeAll(buffers ...[]byte) error {
	for _, buf := range buffers {
		_, err := l.file.Write(buf)
		if err != nil {
			return err
		}
	}

	return nil
}

func incrementSerial(serial []byte) {
	if serial == nil {
		return
	}

	for i := len(serial) - 1; i >= 0; i-- {
		if serial[i] == 0xFF {
			serial[i] = 0x00
		} else {
			serial[i] += 1
			break
		}
	}
}

func (l *LogStream) NextSerial() []byte {
	if l.nextSerial == nil {
		return nil
	}

	n := make([]byte, len(l.nextSerial))
	copy(n, l.nextSerial)
	return n
}

func (l *LogStream) isZeroLength() (bool, error) {
	if l.file == nil {
		return false, ErrClosed
	}

	state, err := l.file.Stat()
	if err != nil {
		return false, err
	}

	return state.Size() == 0, nil
}

func (l *LogStream) State() LogStreamState {
	return l.state
}
