package cmd_scan

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/xianic/xca/scanner/pkimodel"
)

type reportFunc func(collection *pkimodel.Collection) error

var reporters map[string]reportFunc

func addReport(name string, r reportFunc) {
	if reporters == nil {
		reporters = make(map[string]reportFunc)
	}
	reporters[name] = r
}

func selectAndSortCerts(in []*pkimodel.Certificate, filter func(certificate *pkimodel.Certificate) bool, sorter func(a, b *pkimodel.Certificate) int) []*pkimodel.Certificate {
	out := make([]*pkimodel.Certificate, 0, len(in))
	for _, cert := range in {
		if filter == nil || filter(cert) {
			out = append(out, cert)
		}
	}

	if sorter != nil {
		slices.SortFunc(out, sorter)
	}

	return out
}

func sortById(a, b *pkimodel.Certificate) int {
	aName := a.File().String()
	bName := b.File().String()

	return strings.Compare(aName, bName)
}

func init() {
	addReport("none", func(_ *pkimodel.Collection) error { return nil })
	addReport("all", func(pc *pkimodel.Collection) error {
		for _, cert := range selectAndSortCerts(pc.Certificates, nil, sortById) {
			fmt.Println(cert)
		}
		return nil
	})
}
