package pemblockfile

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/cryptutil/pemstream"
	"gitlab.com/xianic/xca/scanner/pkimodel"
	"gitlab.com/xianic/xca/util"
)

type PemBlockFile struct {
	filePath string
	gaps     [][]byte
	pems     []*pemstream.PemBlock
	updated  bool
}

func ParsePemBlockFile(file io.Reader, path string) (*PemBlockFile, error) {
	parser := pemstream.NewPemReader(file)

	pemBlockFile := &PemBlockFile{
		filePath: path,
		gaps:     make([][]byte, 0),
		pems:     make([]*pemstream.PemBlock, 0),
	}

	for {
		gap, err := readGap(parser)
		if err != nil {
			return nil, err
		}
		pemBlockFile.gaps = append(pemBlockFile.gaps, gap)

		block, err := parser.GetPemBlock()
		if block == nil {
			break
		}
		if err != nil {
			return nil, err
		}

		pemBlockFile.pems = append(pemBlockFile.pems, block)
	}

	return pemBlockFile, nil
}

func readGap(parser *pemstream.PemReader) (buffer []byte, err error) {
	buf := &bytes.Buffer{}
	var gapStream io.ReadCloser
	gapStream, err = parser.GetGapDataStream()
	if err != nil {
		return nil, err
	}
	defer util.DeferClose(gapStream.Close, &err)

	_, err = io.Copy(buf, gapStream)
	return buf.Bytes(), err
}

func (p *PemBlockFile) String() string {
	return p.filePath
}

func (p *PemBlockFile) ParseCertificates() ([]*pkimodel.Certificate, error) {
	certs := make([]*pkimodel.Certificate, 0, len(p.pems))
	for _, pem := range p.pems {
		if pem.Type != cryptutil.PemTypeCertificate {
			continue
		}

		pif := &CertInFile{
			PemInFile: PemInFile{
				file: p,
				pem:  pem,
			},
		}
		cif, err := pkimodel.NewCertificate(pif)
		if err != nil {
			return nil, fmt.Errorf("parsing certificate from %q: %w", pif.String(), err)
		}
		certs = append(certs, cif)
	}

	return certs, nil
}

func (p *PemBlockFile) ParsePrivateKeys() ([]*pkimodel.PrivateKey, error) {
	keys := make([]*pkimodel.PrivateKey, 0, len(p.pems))
	for _, pem := range p.pems {
		switch pem.Type {
		default:
			continue
		case cryptutil.PemTypePrivateKey:
			fallthrough
		case cryptutil.PemTypeRsaPrivateKey:
			fallthrough
		case cryptutil.PemTypeECPrivateKey:
		}

		kif := &PrivateKeyInFile{
			PemInFile{
				file: p,
				pem:  pem,
			},
		}

		key, err := pkimodel.NewPrivateKeyOnline(kif)
		if err != nil {
			return nil, fmt.Errorf("parsing key from file %q, %w", kif.String(), err)
		}
		keys = append(keys, key)
	}

	return keys, nil
}

func (_ *PemBlockFile) TieBreakCertificates(target *pkimodel.Certificate, potential []*pkimodel.Certificate) *pkimodel.Certificate {
	return tieBreak(target, potential)
}

func (_ *PemBlockFile) TieBreakPrivateKeys(cert *pkimodel.Certificate, potential []*pkimodel.PrivateKey) *pkimodel.PrivateKey {
	return tieBreak(cert, potential)
}

func tieBreak[T pkimodel.IsInFile](target *pkimodel.Certificate, potential []T) T {
	targetPem := target.File().(*CertInFile).pem

	minValue := -1
	offsetOfMin := -1
	numFound := 0
	for i, p := range potential {
		pem := p.File().(*CertInFile).pem

		var linesBetween int
		if targetPem.StartLine > pem.EndLine {
			linesBetween = targetPem.StartLine - pem.EndLine
		} else {
			linesBetween = pem.StartLine - targetPem.EndLine
		}

		if minValue == -1 || linesBetween < minValue {
			minValue = linesBetween
			offsetOfMin = i
			numFound = 1
		} else if minValue == linesBetween {
			numFound++
		}
	}

	if numFound == 1 {
		return potential[offsetOfMin]
	}

	return *new(T)
}

func (p *PemBlockFile) MarkUpdated() {
	p.updated = true
}

func (p *PemBlockFile) Save() (anyChanges bool, err error) {
	if !p.updated {
		return false, nil
	}

	defer util.DeferWrap(&err, "writing changes back to %q: %w", p.filePath)

	of, err := os.OpenFile(p.filePath, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return true, err
	}
	defer util.DeferClose(of.Close, &err)

	for i := 0; i < len(p.gaps) && i < len(p.pems); i++ {
		if i < len(p.gaps) {
			if _, err := of.Write(p.gaps[i]); err != nil {
				return true, err
			}
		}
		if i < len(p.pems) {
			if err := p.pems[i].WriteToStream(of); err != nil {
				return true, err
			}
		}
	}

	return true, nil
}
