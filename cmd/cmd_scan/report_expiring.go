package cmd_scan

import (
	"fmt"
	"slices"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/xianic/xca/scanner/pkimodel"
)

type expiringChain struct {
	When time.Time
	Leaf *pkimodel.Certificate
}

func init() {
	addReport("expiring", reportExpiring)
}

func (e *expiringChain) Print() {
	tp := newTreePrinter("")
	tp.AddLevel(false)
	printLeafTree(tp, e.Leaf, func(c *pkimodel.Certificate) string {
		if c.Parsed() == nil {
			return ""
		}
		return fmt.Sprintf("at %s (%s)", c.Parsed().NotAfter.Format(time.RFC3339), humanize.Time(c.Parsed().NotAfter))
	})
}

func selectExpiringCertificates(pc *pkimodel.Collection) []*expiringChain {
	threshold := time.Now().Add(time.Duration(thresholdInDays*24) * time.Hour)
	expiringCerts := make([]*expiringChain, 0)

	for _, c := range pc.Certificates {
		if len(c.Issued()) != 0 || c.Parsed().IsCA {
			continue
		}

		var exp *expiringChain

		for w := c; w != nil; w = w.Issuer() {
			if c.Parsed() == nil {
				continue
			}
			if w.Parsed().NotAfter.Before(threshold) {
				if exp == nil {
					exp = &expiringChain{
						When: w.Parsed().NotAfter,
						Leaf: c,
					}
				} else if w.Parsed().NotAfter.Before(exp.When) {
					exp.When = w.Parsed().NotBefore
				}
			}
		}

		if exp != nil {
			expiringCerts = append(expiringCerts, exp)
			exp = nil
		}
	}

	slices.SortFunc(expiringCerts, func(a, b *expiringChain) int {
		return a.When.Compare(b.When)
	})

	return expiringCerts
}

func reportExpiring(pc *pkimodel.Collection) error {
	expiringCerts := selectExpiringCertificates(pc)

	tz := time.Now().Location()
	lastExpireDate := ""

	for _, e := range expiringCerts {
		localWhen := e.When.In(tz)
		expireDate := time.Date(localWhen.Year(), localWhen.Month(), localWhen.Day(), 0, 0, 0, 0, tz).Format("Mon Jan 2 2006 MST")

		if expireDate != lastExpireDate {
			if lastExpireDate != "" {
				fmt.Println()
			}
			fmt.Printf("%s (%s)\n", expireDate, humanize.Time(localWhen))
			lastExpireDate = expireDate
		}

		expireTime := localWhen.Format("15:04:05")
		fmt.Printf("at %s\n", expireTime)
		e.Print()
	}

	return nil
}
