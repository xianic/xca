package pemcrypt

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"math"
	"time"

	"gitlab.com/xianic/xca/util"
	"golang.org/x/crypto/argon2"
)

const (
	MinTime    = 4  // Seconds
	MinMemory  = 64 // MB
	MinThreads = 4
)

func MakePromptArgs(filePath string, ot OpType) (promptText string, twice bool) {
	text := "passphrase to "
	switch ot {
	case OpEncrypt:
		text += "encrypt "
	case OpDecrypt:
		text += "decrypt "
	}
	text += filePath
	return text, ot == OpEncrypt
}

type PasswordParameters struct {
	Salt          []byte
	TimeInSeconds bool
	Time          uint32
	Memory        uint32
	Threads       uint8
}

func (po *PasswordParameters) ParsePasswordOptions(value string) error {
	po.SetDefaults()

	_, err := util.ParseFields(
		value,
		":",
		4,
		&po.Time,
		&po.Memory,
		&po.Threads,
		&po.Salt,
	)

	if err != nil {
		return err
	}

	return nil
}

func (p *PasswordParameters) String() string {
	return fmt.Sprintf(
		"%d:%d:%d:%s",
		p.Time,
		p.Memory,
		p.Threads,
		base64.StdEncoding.EncodeToString(p.Salt),
	)
}

// SetDefaults sets the default values for the current machine if the values
// are out of acceptable ranges. This function may be called on a nil pointer
// in which case a p will be set to a newly allocated struct.
func (p *PasswordParameters) SetDefaults() {
	if p == nil {
		*p = PasswordParameters{}
	}

	if !p.TimeInSeconds && p.Time < MinTime {
		p.Time = MinTime
	}

	if p.Threads < MinThreads {
		p.Threads = MinThreads
	}

	if p.Memory < MinMemory {
		p.Memory = MinMemory
	}
}

func (p *PasswordParameters) key(password []byte) (<-chan []byte, error) {
	if p.Salt == nil {
		p.Salt = make([]byte, 16)
		if _, err := rand.Read(p.Salt); err != nil {
			return nil, err
		}
	}

	if len(password) == 0 {
		return nil, errors.New("empty password")
	}

	resultChan := make(chan []byte, 1)

	go func() {
		resultChan <- p.argon2(password)
		close(resultChan)
	}()

	return resultChan, nil
}

func (p *PasswordParameters) argon2(password []byte) (key []byte) {
	if !p.TimeInSeconds {
		key, _ = p.timeArgon2(password)
		return
	}

	p.TimeInSeconds = false

	/*
		The aim is to find a value for p.Time such that the argon2 key derivation
		takes target time. The minimum acceptable value is minTime, so we start
		there and improve our guessed from there. Guesses will improve in accuracy
		while to acceptance window widens so this algorithm should terminate
		quickly with a reasonably good result.
	*/
	target := time.Duration(p.Time) * time.Second
	threePercent := time.Duration(float64(target) * 0.03)
	upperBound := target + threePercent
	lowerBound := target - threePercent

	p.Time = uint32(MinTime)
	var lastDelay time.Duration

	for {
		key, lastDelay = p.timeArgon2(password)

		// To analyse the guesses uncomment the following
		//pcErr := math.Abs(float64(lastDelay-target)) / float64(target)
		//pcWindow := float64(upperBound-target) / float64(target)
		//fmt.Printf("rounds: %3d, %%err: %4.1f, acceptable %%err: %4.1f\n", p.Time, pcErr*100.0, pcWindow*100.0)

		if lastDelay > lowerBound && lastDelay < upperBound {
			return key
		}

		nextGuess := guess(p.Time, lastDelay, target)
		if nextGuess < MinTime {
			return key
		}

		p.Time = nextGuess
		lowerBound -= threePercent
		upperBound += threePercent
	}
}

func guess(iterations uint32, delay, target time.Duration) uint32 {
	guess := float64(target * time.Duration(iterations) / delay)
	return uint32(math.Round(guess))
}

func (p *PasswordParameters) timeArgon2(password []byte) (key []byte, delay time.Duration) {
	start := time.Now()
	key = argon2.IDKey(password, p.Salt, p.Time, p.Memory*1024, p.Threads, 32)
	end := time.Now()
	delay = end.Sub(start)
	return
}
