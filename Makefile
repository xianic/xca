GOSRC=$(shell find -type f -name '*.go')

# The version of xca that does not include Yubikey PIV code is built in a
# container so that it cannot accidentally depend on host libraries and be
# linked against PIV libraries.
GOLANG_CONTAINER := golang:1.21.6
DOCKER_RUN := docker run \
	-v "$(CURDIR):/code:rw" \
	-v "$(CURDIR)/.cache:/.cache:rw" \
	--user $(shell id -u):$(shell id -g) \
	-i --rm

# By default we only build the noykpiv version so that `make` out of the box
# will work.
.PHONY: default all clean test test_unit_noykpiv test_unit_ykpiv test_integration
default: xca-noykpiv
all: xca-noykpiv xca-ykpiv
test: test_unit_noykpiv test_unit_ykpiv test_integration
clean:
	rm -f xca-noykpiv xca-ykpiv

xca-noykpiv: $(GOSRC)
	$(DOCKER_RUN) $(GOLANG_CONTAINER) bash -c 'cd /code && CGO_ENABLED=0 go build -buildvcs=false -o $@'

xca-ykpiv: $(GOSRC)
	go build -tags ykpiv -o $@

test_unit_noykpiv:
	$(DOCKER_RUN) $(GOLANG_CONTAINER) bash -c 'cd /code && find -path ./cmd/ykpiv -prune -o -type f -name "*.go" -print | xargs -r grep -LF "github.com/go-piv/piv-go" | sed "s|/[^/]*\$$||" | sort -u | xargs go test'

test_unit_ykpiv:
	go test -tags ykpiv ./...


test_integration: xca-noykpiv
	$(DOCKER_RUN) bats/bats test

.PHONY: install
install: xca-ykpiv test
	sudo install xca-ykpiv /usr/local/bin/xca
