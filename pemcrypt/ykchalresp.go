package pemcrypt

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/xianic/xca/util"
)

type YkHmacOptions struct {
	// The salt is passed through the Yubikey's HMAC-SHA1 to produce a value
	// include in computing the master key.
	Salt []byte

	// Slot is which Yubikey slot to use in HMAC-SHA1 mode. If set to 0 or
	// -1 a Yubikey will not be included in the master key generation. If set
	// to 0 then the slit number will be updated from the
	Slot YubiKeySlot
}

func (ykOp *YkHmacOptions) willUseYubikey() bool {
	return ykOp.Slot > 0
}

func (ykOp *YkHmacOptions) parseYkHmacOptions(value string) error {
	var slot int
	_, err := util.ParseFields(value, ":", 2, &slot, &ykOp.Salt)
	if err != nil {
		return err
	}

	if ykOp.Slot == 0 {
		ykOp.Slot = YubiKeySlot(slot)
	}

	return nil
}

func (ykOp *YkHmacOptions) String() string {
	return fmt.Sprintf("%d:%s", ykOp.Slot, base64.StdEncoding.EncodeToString(ykOp.Salt))
}

func (ykOp *YkHmacOptions) key() ([]byte, error) {
	if !ykOp.willUseYubikey() {
		return nil, nil
	}

	if ykOp.Salt == nil {
		ykOp.Salt = make([]byte, 32)
		if _, err := rand.Read(ykOp.Salt); err != nil {
			return nil, err
		}
	}

	args := []string{"ykchalresp", "-x", fmt.Sprintf("-%d", ykOp.Slot), "-i", "-"}
	path, err := exec.LookPath(args[0])
	if err != nil {
		return nil, err
	}

	hexSalt := make([]byte, hex.EncodedLen(len(ykOp.Salt)))
	hex.Encode(hexSalt, ykOp.Salt)

	stdin := bytes.NewBuffer(hexSalt)
	stdout := &bytes.Buffer{}

	cmd := exec.Cmd{
		Path:   path,
		Args:   args,
		Stdin:  stdin,
		Stdout: stdout,
		Stderr: os.Stderr,
	}

	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("ykchalresp: %w", err)
	}

	hexResponse := string(stdout.Bytes())
	if offset := strings.Index(hexResponse, "\n"); offset >= 0 {
		hexResponse = hexResponse[:offset]
	}

	response, err := hex.DecodeString(hexResponse)
	if err != nil {
		return nil, fmt.Errorf("output from ykchalresp is not hex: %w", err)
	}

	if len(response) != 20 {
		return nil, fmt.Errorf("only got %d byte response from ykchalresp, expecting 32", len(response))
	}

	return response, nil
}
