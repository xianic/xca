package yubikey

import (
	"crypto/x509"
	"errors"
	"fmt"
	"strings"

	"github.com/go-piv/piv-go/piv"
	"gitlab.com/xianic/xca/util"
)

type CardInfo struct {
	Serial          uint32
	Version         piv.Version
	AttestationCert *x509.Certificate
	Slots           map[string]*SlotInfo
}

type SlotInfo struct {
	Attestation, Certificate *x509.Certificate
}

func CardSupported(name string) bool {
	return strings.Contains(strings.ToLower(name), "yubikey")
}

func GetCardInfo(name string) (info *CardInfo, err error) {
	if !CardSupported(name) {
		return nil, fmt.Errorf("%q is not a yubikey", name)
	}

	defer util.DeferWrap(&err, "error with card %q: %w", name)

	var yk *piv.YubiKey
	if yk, err = piv.Open(name); err != nil {
		return nil, fmt.Errorf("\terror opening card: %s\n", err)
	}
	defer util.DeferClose(yk.Close, &err)

	info = &CardInfo{
		Slots: map[string]*SlotInfo{},
	}

	info.Serial, err = yk.Serial()
	if err != nil {
		return
	}

	info.Version = yk.Version()

	info.AttestationCert, err = yk.AttestationCertificate()
	if err != nil {
		return nil, err
	}

	for _, slot := range Slots() {
		slotInfo, err := GetSlotInfo(yk, slot)
		if err != nil {
			return nil, err
		}

		info.Slots[SlotName(slot)] = slotInfo
	}

	return info, nil
}

func GetSlotInfo(yk *piv.YubiKey, slot piv.Slot) (slotInfo *SlotInfo, err error) {
	slotInfo = &SlotInfo{}

	slotInfo.Attestation, err = yk.Attest(slot)
	/*if err != nil {
		if errors.Is(err, piv.ErrNotFound) {
			slotInfo.Attestation = nil
			err = nil
		} else {
			return nil, err
		}
	}*/

	slotInfo.Certificate, err = yk.Certificate(slot)
	if err != nil {
		if errors.Is(err, piv.ErrNotFound) {
			slotInfo.Certificate = nil
			err = nil
		} else {
			return nil, err
		}
	}

	return slotInfo, nil
}

func GetCardByName(searchName string) (*piv.YubiKey, error) {
	cards, err := piv.Cards()
	if err != nil {
		return nil, err
	}

	if len(cards) == 0 {
		return nil, fmt.Errorf("no PIV cards or Yubikeys with PIV enabled found")
	}

	if searchName == "" && len(cards) == 1 {
		return piv.Open(cards[0])
	}

	found := -1
	for i, cardName := range cards {
		if strings.Contains(cardName, searchName) {
			if found != -1 {
				if searchName == "" {
					return nil, errors.New("more than one Yubikey found")
				}
				return nil, fmt.Errorf("more than one card found matching %q", searchName)
			}
			found = i
		}
	}
	if found == -1 {
		return nil, fmt.Errorf("no card found matching %q", searchName)
	}

	if !CardSupported(cards[found]) {
		return nil, fmt.Errorf("card is not a yubikey")
	}

	return piv.Open(cards[found])
}

func GetCardBySerial(serial uint32) (*piv.YubiKey, error) {
	cards, err := piv.Cards()
	if err != nil {
		return nil, err
	}

	for _, cardName := range cards {
		if !CardSupported(cardName) {
			continue
		}

		card, err := piv.Open(cardName)
		if err != nil {
			continue
		}

		foundSerial, err := card.Serial()
		if err != nil {
			continue
		}

		if foundSerial == serial {
			return card, nil
		}

		err = card.Close()
		if err != nil {
			return nil, err
		}
	}

	return nil, fmt.Errorf("no supported card with serial %d found", serial)
}
