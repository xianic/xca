package cmd_pubkey

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/cmd_gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

var (
	Cmd = &cobra.Command{
		Use:   "pubkey [flags] [input_file [output_file]] ",
		Short: "extract public key from objects",
		Long: `Read key(s) certificate(s), and csr(s) and extract the public key. For each
PEM encoded object read a PEM encoded public key will be emitted. input_file
or output_file may be '-' or omitted to use standard input or standard output
respectively.`,
		Args: cobra.MaximumNArgs(2),
		RunE: pubKeyCmdFunc,
	}

	encFlags cmd.PemDecryptionFlags
)

func init() {
	cmd_gen.Cmd.AddCommand(Cmd)

	encFlags.Init(Cmd.Flags())
}

func pubKeyCmdFunc(cmd *cobra.Command, args []string) (err error) {
	for len(args) < 2 {
		args = append(args, "-")
	}

	out := util.DelayOpenWrite(args[1], false, util.AllowStdout)
	defer util.DeferClose(out.Close, &err)

	ps := cryptutil.UnmarshalFile(args[0], "Enter PEM encoded cryptographic object", encFlags.ParseForDecryption())
	foundBlock := false
	for {
		var pubKey crypto.PublicKey
		pubKey, err = ps.ExpectPublicKeyExtractable()
		if err != nil {
			if errors.Is(err, cryptutil.ErrNoPemFound) {
				break
			}
			return err
		}

		foundBlock = true

		block := &pem.Block{
			Type: cryptutil.PemTypePublicKey,
		}
		block.Bytes, err = x509.MarshalPKIXPublicKey(pubKey)
		if err != nil {
			return err
		}

		if err := pem.Encode(out, block); err != nil {
			return err
		}
	}

	if !foundBlock {
		return fmt.Errorf("no PEM blocks found in input")
	}

	return nil
}
