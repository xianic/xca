package cryptutil

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"slices"
	"strings"

	"gitlab.com/xianic/xca/pemcrypt"
)

var ErrNoPemFound = errors.New("no PEM block found")

type ExpectingBlockError struct {
	expected []string
	found    string
}

func (e ExpectingBlockError) Error() string {
	eMsg := "a PEM block"
	switch len(e.expected) {
	case 0:
		eMsg += "of any type"
	case 1:
		eMsg += "of type " + e.expected[0]
	default:
		eMsg += "of type " + strings.Join(
			e.expected[:len(e.expected)-1],
			", ",
		) + " or " + e.expected[len(e.expected)-1]
	}

	var fMsg string
	if e.found == "" {
		fMsg = "no PEM block at all"
	} else {
		fMsg = "a PEM block of type " + e.found
	}

	return "expecting " + eMsg + " but got " + fMsg
}

func (e ExpectingBlockError) Is(err error) bool {
	if err == ErrNoPemFound {
		return e.found == ""
	}

	return false
}

type PemUnmarshaler struct {
	parser       PemParser
	cryptOptions *pemcrypt.CryptOptions
	userContext  string
}

type PemUnmarshalerOption func(unmarshaler *PemUnmarshaler)

func WithDecryption(decryptionOptions *pemcrypt.CryptOptions) PemUnmarshalerOption {
	return func(unmarshaler *PemUnmarshaler) {
		unmarshaler.cryptOptions = decryptionOptions
	}
}

func WithUserContext(userContext string) PemUnmarshalerOption {
	return func(unmarshaler *PemUnmarshaler) {
		unmarshaler.userContext = " in " + userContext
	}
}

func WithNoDecryption() PemUnmarshalerOption {
	return func(unmarshaler *PemUnmarshaler) {
		unmarshaler.cryptOptions = nil
	}
}

func NewPemUnmarshaler(parser PemParser, options ...PemUnmarshalerOption) *PemUnmarshaler {
	u := &PemUnmarshaler{
		parser:       parser,
		cryptOptions: new(pemcrypt.CryptOptions),
	}

	for _, opt := range options {
		opt(u)
	}

	return u
}

func (u *PemUnmarshaler) NextPem() (*pem.Block, error) {
	return u.parser.NextPem()
}

// Expect returns a PEM block of the expected type or an error. If the block
// is encrypted and decryption is enabled then an encrypted block of the
// correct type(s) will be decrypted before being returned. If pemTypes has a
// length greater than zero then only these types of PEM blocks will be
// accepted, if an unacceptable block is found an error will be returned.
func (u *PemUnmarshaler) Expect(pemTypes []string) (*pem.Block, error) {
	block, err := u.parser.NextPem()
	if err != nil {
		return nil, err
	}
	if block == nil {
		return nil, io.EOF
	}

	blockType := block.Type
	if u.cryptOptions != nil && strings.HasPrefix(block.Type, pemcrypt.PemPrefix) {
		blockType = blockType[len(pemcrypt.PemPrefix):]
	}

	if len(pemTypes) > 0 && !slices.Contains(pemTypes, blockType) {
		return nil, ExpectingBlockError{
			expected: pemTypes,
			found:    block.Type,
		}
	}

	if blockType != block.Type {
		userContext := blockType + u.userContext
		if err := pemcrypt.Decrypt(block, u.cryptOptions, userContext); err != nil {
			return nil, err
		}
	}

	return block, nil
}

// ExpectCertificate parses and returns a certificate
func (u *PemUnmarshaler) ExpectCertificate() (*x509.Certificate, error) {
	block, err := u.Expect([]string{PemTypeCertificate})
	if err != nil {
		return nil, err
	}

	return x509.ParseCertificate(block.Bytes)
}

// ExpectCSR parses and returns a CSR
func (u *PemUnmarshaler) ExpectCSR() (*x509.CertificateRequest, error) {
	block, err := u.Expect([]string{})
	if err != nil {
		return nil, err
	}

	return x509.ParseCertificateRequest(block.Bytes)
}

// ExpectPrivateKey parses and returns a private key
func (u *PemUnmarshaler) ExpectPrivateKey() (crypto.PrivateKey, error) {
	block, err := u.Expect(PemTypesPrivateKeys)
	if err != nil {
		return nil, err
	}

	return UnmarshalPrivateKey(block.Bytes)
}

// ExpectPublicKeyExtractable parses any type of block that is an object that
// contains a public key, then returns the public key from that object.
func (u *PemUnmarshaler) ExpectPublicKeyExtractable() (crypto.PublicKey, error) {
	block, err := u.Expect(PemTypesPublicKeyExtractable)
	if err != nil {
		return nil, err
	}

	var thing any
	switch block.Type {
	case PemTypePrivateKey:
		fallthrough
	case PemTypeECPrivateKey:
		fallthrough
	case PemTypeRsaPrivateKey:
		thing, err = UnmarshalPrivateKey(block.Bytes)

	case PemTypePublicKey:
		fallthrough
	case PemTypeECPublicKey:
		fallthrough
	case PemTypeRsaPublicKey:
		thing, err = UnmarshalPublicKey(block.Bytes)

	case PemTypeCsr:
		thing, err = x509.ParseCertificateRequest(block.Bytes)

	case PemTypeCertificate:
		thing, err = x509.ParseCertificate(block.Bytes)

	default:
		return nil, fmt.Errorf("extracting public key from PEM block of type %s not implemented", block.Type)
	}

	return PublicKeyFor(thing), nil
}
