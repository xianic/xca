package pkimodel

import (
	"crypto/x509"
	"gitlab.com/xianic/xca/ca"
)

type caAdapter struct {
	authority *ca.CA
}

func (c *caAdapter) MarkUpdated() {
}

func (c *caAdapter) Save() (anyChanges bool, err error) {
	return false, nil
}

func newCaAdapter(authority *ca.CA) *caAdapter {
	return &caAdapter{authority: authority}
}

func (c *caAdapter) String() string {
	return c.authority.String()
}

func (c *caAdapter) ParseCertificates() ([]*Certificate, error) {
	cert, err := NewCertificate(&caCertAdapter{ad: c})
	if err != nil {
		return nil, err
	}

	return []*Certificate{cert}, nil
}

func (c *caAdapter) ParsePrivateKeys() ([]*PrivateKey, error) {
	key, err := NewPrivateKeyOffline(&caKeyAdapter{ad: c})
	if err != nil {
		return nil, err
	}

	return []*PrivateKey{key}, nil
}

func (c *caAdapter) TieBreakCertificates(_ *Certificate, _ []*Certificate) *Certificate {
	// A CA adapter will only ever contain one certificate so if there are
	// enough to need a tie-break then something is wrong
	panic("unreachable tie break")
}

func (c *caAdapter) TieBreakPrivateKeys(_ *Certificate, _ []*PrivateKey) *PrivateKey {
	// A CA adapter will only ever contain one key so if there are
	// enough to need a tie-break then something is wrong
	panic("unreachable tie break")
}

type caCertAdapter struct {
	ad *caAdapter
}

func (c *caCertAdapter) SetRawObject(bytes []byte) {
	panic("cannot change certificate authority's certificate: no implemented")
}

func (c *caCertAdapter) GetObject() (*x509.Certificate, error) {
	return c.ad.authority.Certificate(), nil
}

func (c *caCertAdapter) SetObject(certificate *x509.Certificate) error {
	panic("cannot change certificate authority's certificate: no implemented")
}

func (c *caCertAdapter) File() PkiFile {
	return c.ad
}

func (c *caCertAdapter) GetRawObject() []byte {
	return c.ad.authority.Certificate().Raw
}

func (c *caCertAdapter) String() string {
	return c.ad.String() + "#cert"
}

type caKeyAdapter struct {
	ad *caAdapter
}

func (c *caKeyAdapter) SetRawObject(bytes []byte) {
	panic("cannot change certificate authority's certificate: no implemented")
}

func (c *caKeyAdapter) GetObject() (any, error) {
	return c.ad.authority.Certificate().PublicKey, nil
}

func (c *caKeyAdapter) SetObject(_ any) error {
	panic("cannot change certificate authority's certificate: no implemented")
}

func (c *caKeyAdapter) File() PkiFile {
	return c.ad
}

func (c *caKeyAdapter) GetRawObject() []byte {
	panic("not implemented")
}

func (c *caKeyAdapter) String() string {
	return c.ad.String() + "#cert"
}
