package main

import (
	_ "gitlab.com/xianic/xca/ca/file"
	"gitlab.com/xianic/xca/cmd"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca/cmd_audit"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca/cmd_init"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca/cmd_logrotate"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca/cmd_note"
	_ "gitlab.com/xianic/xca/cmd/cmd_ca/cmd_sign"
	_ "gitlab.com/xianic/xca/cmd/cmd_gen"
	_ "gitlab.com/xianic/xca/cmd/cmd_gen/cmd_csr"
	_ "gitlab.com/xianic/xca/cmd/cmd_gen/cmd_key"
	_ "gitlab.com/xianic/xca/cmd/cmd_gen/cmd_pubkey"
	_ "gitlab.com/xianic/xca/cmd/cmd_gen/cmd_selfsigned"
	_ "gitlab.com/xianic/xca/cmd/cmd_pem"
	_ "gitlab.com/xianic/xca/cmd/cmd_pem/cmd_decode"
	_ "gitlab.com/xianic/xca/cmd/cmd_pem/cmd_encode"
	_ "gitlab.com/xianic/xca/cmd/cmd_scan"
	_ "gitlab.com/xianic/xca/cmd/cmd_show"
	_ "gitlab.com/xianic/xca/cmd/cmd_verify"
)

func main() {
	cmd.RunLateInit()
	cmd.Execute()
}
