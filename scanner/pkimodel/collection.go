package pkimodel

import (
	"errors"
	"path/filepath"

	"gitlab.com/xianic/xca/ca"
)

type IsInFile interface {
	File() DataInFile
}

type Collection struct {
	CAs          []*ca.CA
	Files        []PkiFile
	Certificates []*Certificate
	PrivateKeys  []*PrivateKey
	DontScan     map[string]struct{}
}

func NewCollection() *Collection {
	return &Collection{
		CAs:          make([]*ca.CA, 0),
		Files:        make([]PkiFile, 0),
		Certificates: make([]*Certificate, 0),
		PrivateKeys:  make([]*PrivateKey, 0),
		DontScan:     make(map[string]struct{}),
	}
}

func (c *Collection) AddCA(authority *ca.CA) error {
	err := c.AddDontScan(
		authority.Config.LogFilePath(),
		authority.Config.CertificateFilePath(),
	)
	if err != nil {
		return err
	}

	c.CAs = append(c.CAs, authority)

	return c.AddFile(newCaAdapter(authority))
}

func (c *Collection) AddFile(f PkiFile) error {
	certs, err := f.ParseCertificates()
	if err != nil {
		return err
	}

	keys, err := f.ParsePrivateKeys()
	if err != nil {
		return err
	}

	if len(certs) == 0 && len(keys) == 0 {
		return nil
	}

	c.Files = append(c.Files, f)
	c.Certificates = append(c.Certificates, certs...)
	c.PrivateKeys = append(c.PrivateKeys, keys...)

	return nil
}

func (c *Collection) AddDontScan(paths ...string) error {
	full := make([]string, len(paths))
	for i, path := range paths {
		var err error
		full[i], err = filepath.Abs(path)
		if err != nil {
			return err
		}
	}

	for _, path := range full {
		c.DontScan[path] = struct{}{}
	}

	return nil
}

func (c *Collection) ShouldScan(path string) (bool, error) {
	abs, err := filepath.Abs(path)
	if err != nil {
		return false, err
	}

	_, shouldnt := c.DontScan[abs]
	return !shouldnt, nil
}

// Close closes all the CAs in the collection
func (c *Collection) Close() error {
	var err error

	for _, ca := range c.CAs {
		err2 := ca.Close()
		if err2 != nil {
			err = errors.Join(err, err2)
		}
	}

	return err
}
