# Copyright 2022 Xian Stannard as part of XCA https://gitlab.com/xianic/xca
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

#######################################################################
# These macros use the xca tool to create keys, certificates, and CAs #
#######################################################################

# This is the xca binary that will be called in the macros in this file. If
# xca is not in the path, or you need to override which binary is used, set
# this variable before including this file.
XCA_TOOL ?= xca

# XCA_KEY generates a cryptographic key for use with x509 Certificates.
# $(1) file name without .key extension
# $(2) (optional) key type
# $(3) (optional) additional argument to key generation
# Depend on $(1).key to cause key to be created.
define XCA_KEY

$(1).key:
ifeq "$(2)" ""
	$(XCA_TOOL) gen key $(3) "$(1).key"
else
	$(XCA_TOOL) gen key $(3) --type "$(2)" "$(1).key"
endif

.PHONY: clean_$(1).key
clean_$(1).key:
	rm -f "$(1).key"

clean: clean_$(1).key
endef

# XCA_ROOT_CA create a root certificate authority.
# $(1) directory name of CA
# $(2) CA cert subject
# $(3) Key type, may be "" for default
# $(4) Extra arguments to `xca ca init`
# $(5) (optional) Extra arguments to key generation
# Depend on $(1)/ca.pem to cause the CA to be created.
# Also creates $(1)/ca.key and $(1)/audit.log .
define XCA_ROOT_CA

$(1):
	mkdir -p "$(1)"

$(call XCA_KEY,$(1)/ca,$(3),$(5))
$(1)/ca.key: | $(1)

$(1)/ca.pem: $(1)/ca.key
	rm -f "$(1)/audit.log" "$(1)/ca.pem"
	$(XCA_TOOL) ca -C "$(1)" init --subject "$(2)" $(4)

.PHONY: clean_$(1)
clean_$(1): clean_$(1)/ca.key
	rm -rf "$(1)"

clean: clean_$(1)

endef

# XCA_INT_CA create an intermediate certificate authority.
# $(1) directory name of this CA
# $(2) directory name of the CA to sign this one
# $(3) CA cert subject
# $(4) Key type, may be "" for default
# $(5) (optional) Extra arguments to `xca ca sign` used when signing this CA's
#      certificate
# $(6) (optional) Extra arguments to `xca gen key`
# Depend on $(1)/ca.pem to cause the CA to be created.
# Depends on $(2)/ca.pem .
# Also creates $(1)/ca.key , $(1)/ca.csr and $(1)/audit.log .
define XCA_INT_CA

$(1):
	mkdir -p "$(1)"

$(call XCA_KEY,$(1)/ca,$(4),$(6))
$(1)/ca.key: | $(1)

$(1)/ca.csr: $(1)/ca.key
	$(XCA_TOOL) gen csr --subject "$(3)" "$(1)/ca.key" "$(1)/ca.csr"

$(1)/ca.pem: $(1)/ca.csr $(2)/ca.pem
	rm -f "$(1)/audit.log" "$(1)/ca.pem"
	$(XCA_TOOL) ca -C "$(2)" sign --ca $(5) "$(1)/ca.csr" "$(1)/ca.pem"
	$(XCA_TOOL) ca -C "$(1)" init

.PHONY: clean_$(1)
clean_$(1): clean_$(1)/ca.key
	rm -rf "$(1)"

clean: clean_$(1)

endef

# XCA_LEAF_CERT creates a certificate that is signed by a CA.
# $(1) base name for certificate, CSR, and key files without the .pem,
#      .csr, or .key suffix.
# $(2) CA to sign with (arg 1 for XCA_ROOT_CA)
# $(3) options to `xca gen csr`. This should include at least one of
#       --subject, or one of the SAN generation switches such as --host.
# $(4) (optional) type of key to generate.
# $(5) (optional) switches to pass to `xca ca sign`
# $(6) (optional) Extra arguments to `xca gen key`
# Depend on $(1).pem to cause the certificate to be created.
# Depends on $(2)/ca.pem .
# Also creates $(1).key and $(1).csr .
define XCA_LEAF_CERT

$(call XCA_KEY,$(1),$(4),$(6))

$(1).csr: $(1).key
	$(XCA_TOOL) gen csr $(3) "$(1).key" "$(1).csr"

$(1).pem: $(1).csr $(2)/ca.pem
	$(XCA_TOOL) ca -C "$(2)" sign $(5) "$(1).csr" "$(1).pem"

.PHONY: clean_$(1)_cert
clean_$(1)_cert:
	rm -f "$(1).csr" "$(1).pem"

clean: clean_$(1)_cert

endef

# XCA_SELF_SIGNED creates a certificate that is signed by itself.
# $(1) base name for certificate and key files without the .key or .pem
#      extensions.
# $(2) options to `xca gen selfsignd` command. This should include at
#      least one of --subject, or one of the SAN generation switches
#      such as --host.
# $(3) (optional) key type
# $(4) (optional) Extra arguments to `xca gen key`
# Depend on $(1).pem to cause the certificate to be created.
# Also creates $(1).key .
define XCA_SELF_SIGNED

$(call XCA_KEY,$(1),$(3),$(4))

$(1).pem: $(1).key
	$(XCA_TOOL) gen selfsigned $(2) "$(1).key" "$(1).pem"

.PHONY: clean_$(1).pem
clean_$(1).pem:
	rm -f "$(1).pem"

clean: clean_$(1).pem

endef
