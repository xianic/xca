# XCA - Xianic Certificate Authority

This project is a tool to inspect and create certificates. Sensible defaults
are used so that easy things are simple and complex things are possible.

## HOW TOs

* [How to get the XCA tool](doc/howto-get-xca.md).
* [How to create a public and private key pair](doc/howto-create-key.md).

## Developing with XCA

For use in a golang program see the
[code documentation](https://pkg.go.dev/gitlab.com/xianic/xca). To set up a
PKI environment [`make`](https://www.gnu.org/software/make/) e.g. for testing another program see the example [Makefile](make/Makefile).
