package pemstream

import (
	"encoding/base64"
	"errors"
	"io"
)

// errExpectedEOF is used to terminate the parsing loop. It is not an error
// that is returned to the caller of base64.Read() because a stream ending
// where it is supposed to is not a problem.
var errExpectedEOF = errors.New("expected EOF")

// b64DecodeMode is a function that will read and write some bytes from a
// buffer and may modify the base64Decoder. Data is to be read from
// rdb[*rdOdd:len(rdb)] and written to wrb[*wrOff:len(wrb)]. rdb and wrb may
// be the same buffer so *wrOff should never exceed *rdOff.
// Given that decoding base64 always results in less output than input,
// *wrOff can remain less than *rdOff, though a small amount of data may need
// to be buffered temporarily.
//
// The parser will call b.mode repeatedly until an error is returned. The mode
// may return nil to restart itself (or the next mode if it has modified
// b.mode).
//
// If *rdOff == len(rdb) all input data in the buffer has been consumed.
// In this state at least one of the modes should return an error
// (errExpectedEOF and io.ErrUnexpectedEOF are good options) so that the
// parsing loop will terminate.
//
// If io.ErrUnexpectedEOF is returned and the parser is not actually at the end
// of the upstream, then the parsing loop will terminate and resume with
// fresh data in rdb and rdOff and wrOff reset accordingly.
type b64DecodeMode func(b *base64Decoder, rd, wr []byte, rdOff, wrOff *int) error

// This is the mode before any data has been read on a line. An EOF here is
// allowed.
func b64DecodeStart(b *base64Decoder, rdb, _ []byte, rdOff, _ *int) error {
	if *rdOff == len(rdb) {
		return errExpectedEOF
	}

	b.mode = b64DecodeWsStart
	return nil
}

// Consume the white space from the beginning of a line.
func b64DecodeWsStart(b *base64Decoder, rdb, _ []byte, rdOff, _ *int) error {
	if b.block.B64LineLen > 0 && b.b64LenThisLine < b.block.B64LineLen {
		return ErrB64LenWrong
	} else {
		b.b64LenThisLine = 0
	}

	*rdOff += findMatchLength(rdb[*rdOff:], isWhiteSpace)
	if *rdOff == len(rdb) {
		return io.ErrUnexpectedEOF
	}

	b.mode = b64DecodeText
	return nil
}

// Decode some bas64 characters
func b64DecodeText(b *base64Decoder, rdb, wrb []byte, rdOff, wrOff *int) (err error) {
	if *rdOff == len(rdb) {
		return io.ErrUnexpectedEOF
	}

	b64Start := *rdOff

	// find the end of the bas64 characters
	b64End := findMatchLength(rdb[b64Start:], isBase64) + *rdOff

	// and find any padding
	b64EndNoted := false
	for b64End < len(rdb) && rdb[b64End] == '=' {
		if !b64EndNoted && b.encodingEnded && b64Start != *rdOff {
			return ErrB64EndTwice
		}
		b.encodingEnded = true
		b64EndNoted = true
		b64End++
	}
	b64Len := b64End - b64Start

	defer func() {
		b.b64LenThisLine += *rdOff - b64Start
		if err != nil {
			return
		}

		if *wrOff == len(wrb) {
			// technically this is a write buffer about to overflow event
			// but io.ErrUnexpectedEOF terminates the parsing loop without
			// allowing the input to end here.
			err = io.ErrUnexpectedEOF
			return
		}

		if *rdOff == b64End {
			if *rdOff == len(rdb) {
				err = io.ErrUnexpectedEOF
			} else {
				b.mode = b64DecodeTrailer
			}
		}
	}()

	if b64Len == 0 {
		return
	}

	for len(b.inBuf) != 0 || len(b.outBuf) != 0 {
		var n int
		// empty the output buffer as much as possible
		n, err = b.flushBuffers(wrb[*wrOff:min(*rdOff, len(wrb))], false)
		*wrOff += n
		if err != nil {
			return
		}

		if len(b.outBuf) != 0 && *wrOff == len(wrb) {
			// write buffer is full
			return
		}

		// Fill the input buffer if it is either partly filled or if there
		// was not enough space to write out all of the out buffer.
		if len(b.inBuf) != 0 || len(b.outBuf) != 0 {
			n = copy(bufSpareCap(b.inBuf), rdb[*rdOff:b64End])
			bufGrowLen(&b.inBuf, n)
			*rdOff += n
			if len(b.inBuf) < cap(b.inBuf) {
				// input buffer could not be completely filled because
				// we don't have enough input
				return
			}
		}
	}

	// base64.Encoding.Decode() has a fast path for decoding where
	// len(src) >= 4 and len(dst) >= 4  and an even faster path for
	// len(src) >= 8 and len(dst) >= 8. Where we can we make use of this.
	// We find the maximum number of bytes that we can decode into the output
	// buffer and then round down to the nearest 4 bytes, then decode this
	// amount.
	fastPathLen := min(
		b64End-*rdOff,           // amount of data left to read
		((len(wrb)-*wrOff)*4)/3, //amount of data that will fill the buffer
	) & ^0x3
	if fastPathLen != 0 {
		var n int
		n, err = base64.StdEncoding.Decode(wrb[*wrOff:], rdb[*rdOff:*rdOff+fastPathLen])
		if err != nil {
			return
		}

		*rdOff += fastPathLen
		*wrOff += n
	}

	// if there is any undecoded data remaining then copy it into inBuff
	// to be concatenated with another chunk later and decoded
	if *rdOff < b64End {
		n := copy(bufSpareCap(b.inBuf), rdb[*rdOff:b64End])
		*rdOff += n
		bufGrowLen(&b.inBuf, n)
	}

	return nil
}

// Consume white space at the end of a line
func b64DecodeTrailer(b *base64Decoder, rdb, _ []byte, rdOff, _ *int) error {
	if b.b64LenThisLine == 0 {
		return ErrNoB64
	}

	if b.block.B64LineLen == 0 {
		b.block.B64LineLen = b.b64LenThisLine
	} else if b.block.B64LineLen < b.b64LenThisLine {
		return ErrB64LenWrong
	}

	*rdOff += findMatchLength(rdb[*rdOff:], isWhiteSpace)
	if *rdOff <= len(rdb) {
		if b.block.WinLineEndings {
			b.mode = b64DecodeWinEOL
		} else {
			b.mode = b64DecodeUnixEOL
		}
	}

	return nil
}

func b64DecodeWinEOL(b *base64Decoder, rdb, _ []byte, rdOff, _ *int) error {
	if *rdOff == len(rdb) {
		return io.ErrUnexpectedEOF
	}

	if rdb[*rdOff] != '\r' {
		if rdb[*rdOff] == '\n' {
			return ErrInconsistentLineEndings
		}

		return ErrTrailingGarbage
	}

	*rdOff++
	b.mode = b64DecodeUnixEOL
	return nil
}

func b64DecodeUnixEOL(b *base64Decoder, rdb, _ []byte, rdOff, _ *int) error {
	if *rdOff == len(rdb) {
		return io.ErrUnexpectedEOF
	}

	if rdb[*rdOff] != '\n' {
		return ErrTrailingGarbage
	}

	*rdOff++
	b.mode = b64DecodeStart
	return nil
}
