package cryptutil

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"fmt"
)

func UnmarshalPrivateKey(data []byte) (crypto.PrivateKey, error) {
	var key crypto.PrivateKey
	var errPKCS1, errPKCS8, errEC error

	key, errPKCS1 = x509.ParsePKCS1PrivateKey(data)
	if errPKCS1 == nil {
		return key, nil
	}

	key, errPKCS8 = x509.ParsePKCS8PrivateKey(data)
	if errPKCS8 == nil {
		return key, nil
	}

	key, errEC = x509.ParseECPrivateKey(data)
	if errEC == nil {
		return key, nil
	}

	return nil, fmt.Errorf(
		"could not pares private key as PKCS1 [%v], as PKCS8 [%v], or as EC [%v]",
		errPKCS1,
		errPKCS8,
		errEC,
	)
}

func UnmarshalPublicKey(data []byte) (crypto.PublicKey, error) {
	var key crypto.PublicKey
	var errPKCS1, errPKiX error

	key, errPKCS1 = x509.ParsePKCS1PublicKey(data)
	if errPKCS1 == nil {
		return key, nil
	}

	key, errPKiX = x509.ParsePKIXPublicKey(data)
	if errPKiX == nil {
		return key, nil
	}

	return nil, fmt.Errorf(
		"could not pares private key as PKCS1 [%v], or as PKIX [%v]",
		errPKCS1,
		errPKiX,
	)
}

func PublicKeyFor(obj any) crypto.PublicKey {
	switch key := obj.(type) {

	// private keys
	case *rsa.PrivateKey:
		return &key.PublicKey
	case *ecdsa.PrivateKey:
		return &key.PublicKey
	case ed25519.PrivateKey:
		return key.Public()

	// public keys (return as is)
	case *rsa.PublicKey:
		return key
	case *ecdsa.PublicKey:
		return key
	case ed25519.PublicKey:
		return key

	case *x509.Certificate:
		return key.PublicKey

	case *x509.CertificateRequest:
		return key.PublicKey

	default:
		return nil
	}
}

func pubKeyHash(key any, isId bool) (data []byte) {
	if pubkey := PublicKeyFor(key); pubkey != nil {
		key = pubkey
	}

	switch pub := key.(type) {
	case *rsa.PublicKey:
		if isId {
			data, _ = asn1.Marshal(*pub)
		} else {
			data = x509.MarshalPKCS1PublicKey(pub)
		}

	default:
		data, _ = x509.MarshalPKIXPublicKey(key)
	}

	if len(data) > 0 {
		if isId {
			d := sha1.Sum(data)
			data = d[:]
		} else {
			d := sha256.Sum256(data)
			data = d[:]
		}
	}

	return
}

/*
SubjectKeyId is used to generate a SHA1 fingerprint of a public key and is compatible with the algorithm defined in
RFC5280. This is the value of the Subject Key ID extension in certificates issued. Unless compatibility with RFC5280 is
required, use PublicKeyFingerprint() instead as this uses SHA512_256 which is both faster and more secure.
*/
func SubjectKeyId(key any) []byte {
	return pubKeyHash(key, true)
}

/*
PublicKeyFingerprint returns a SHA256 hash of a repeatable encoding of the given key. If a private key is passed,
only the public part is considered. On error a zero length byte slice is returned.
*/
func PublicKeyFingerprint(key any) []byte {
	return pubKeyHash(key, false)
}

func KeysMatch(a, b any) bool {
	afp := PublicKeyFingerprint(a)
	bfp := PublicKeyFingerprint(b)
	return a != nil && b != nil && bytes.Equal(afp, bfp)
}
