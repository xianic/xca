package siglog

import (
	"bufio"
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

/*
MessageType is a string that is easy to parse: it may only contain ASCII letters, numbers and hyphen. Function
MakeMessageType can be used to ensure that a string is valid.
*/
type MessageType string

type Message struct {
	Serial          []byte
	When            time.Time
	What            MessageType
	Length          uint64
	LastMessageHash []byte
	Payload         []byte
	Signature       []byte
}

func MakeMessageType(s string) (MessageType, error) {
	for _, c := range s {
		if (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') {
			return "", fmt.Errorf("illegal rune %c", c)
		}
	}

	return MessageType(s), nil
}

func MustMessageType(s string) MessageType {
	m, err := MakeMessageType(s)
	if err != nil {
		panic(err)
	}

	return m
}

/*
ParseMessageHeader takes a line of text populates all the fields in the message except for the message itself. After
parsing the length of the message will be known and so the following Message.Length bytes can be read to populate the
message field.
*/
func ParseMessageHeader(in string) (*Message, error) {
	sc := bufio.NewScanner(strings.NewReader(in))
	sc.Split(bufio.ScanWords)

	m := new(Message)
	fieldsParsed := 0
	var err error

	for sc.Scan() {
		switch fieldsParsed {
		case 0:
			// serial
			m.Serial, err = hex.DecodeString(sc.Text())
		case 1:
			// time
			m.When, err = time.Parse(time.RFC3339, sc.Text())
		case 2:
			// type
			m.What = MessageType(sc.Text())
		case 3:
			// payload length
			m.Length, err = strconv.ParseUint(sc.Text(), 10, 32)
		case 4:
			// previous message hash
			lmh := sc.Text()
			if lmh == "-" {
				m.LastMessageHash = nil
			} else {
				m.LastMessageHash, err = base64.StdEncoding.DecodeString(sc.Text())
			}
		case 5:
			// message signature
			m.Signature, err = base64.StdEncoding.DecodeString(sc.Text())
		default:
			err = errors.New("unexpected extra field in log header")
		}

		fieldsParsed++
		if err != nil {
			return nil, err
		}
	}

	if fieldsParsed < 6 {
		return nil, errors.New("not enough fields in log message header")
	}

	return m, err
}

func ReadMessageFrom(br *bufio.Reader) (*Message, error) {
	headerLine, err := br.ReadString('\n')
	if err != nil {
		// This is the only place where an EOF is acceptable: at the boundary between messages
		if errors.Is(err, io.EOF) && len(headerLine) == 0 {
			return nil, nil
		}
		return nil, fmt.Errorf("reading log message header: %w", err)
	}

	m, err := ParseMessageHeader(headerLine)
	if err != nil {
		return nil, fmt.Errorf("parsing message header: %w", err)
	}

	if m.Length > MaxLogMessageSize {
		return nil, fmt.Errorf(
			"log message of size %d greater than allowable maximum of %d",
			m.Length,
			MaxLogMessageSize,
		)
	}

	m.Payload = make([]byte, m.Length)
	_, err = io.ReadFull(br, m.Payload)
	if err != nil {
		return nil, fmt.Errorf("reading message payload: %w", err)
	}

	if len(m.Payload) > 0 && m.Payload[len(m.Payload)-1] != '\n' {
		discard := make([]byte, 1)
		_, err = io.ReadFull(br, discard)
		if err != nil {
			return nil, fmt.Errorf("reading additional line feed: %w", err)
		}

		if discard[0] != '\n' {
			return nil, errors.New("expected line feed missing")
		}
	}

	//TODO: check message signature

	return m, nil
}

func (m *Message) String() string {
	return fmt.Sprintf(
		"%s %s %s %d %s %s",
		hexOrDash(m.Serial),
		m.When.UTC().Format(time.RFC3339),
		m.What,
		m.Length,
		b64OrDash(m.LastMessageHash),
		b64OrDash(m.Signature),
	)
}

func (m *Message) WriteMessageTo(out io.Writer) (err error) {
	defer recoverPanicToError(&err)

	writeOrPanic(out, []byte(m.String()))
	writeOrPanic(out, []byte("\n"))
	if m.Payload != nil {
		writeOrPanic(out, m.Payload)
		if len(m.Payload) > 0 && m.Payload[len(m.Payload)-1] != '\n' {
			writeOrPanic(out, []byte("\n"))
		}
	}

	return
}

func (m *Message) writeSignableBytes(w io.Writer) (err error) {
	originalSig := m.Signature
	defer func() {
		m.Signature = originalSig
	}()
	m.Signature = nil

	defer recoverPanicToError(&err)

	writeOrPanic(w, []byte(m.String()))
	writeOrPanic(w, []byte("\n"))
	if m.Payload != nil {
		writeOrPanic(w, m.Payload)
	}

	return
}

func writeOrPanic(w io.Writer, p []byte) {
	_, err := w.Write(p)
	if err != nil {
		panic(err)
	}
}

func recoverPanicToError(pe *error) {
	switch t := recover().(type) {
	case error:
		if *pe == nil {
			*pe = t
		}
	case nil:
		return
	default:
		panic(t)
	}
}

func hexOrDash(bin []byte) string {
	if len(bin) == 0 {
		return "-"
	}

	return fmt.Sprintf("%X", bin)
}

func b64OrDash(bin []byte) string {
	if len(bin) == 0 {
		return "-"
	}

	return base64.StdEncoding.EncodeToString(bin)
}

func (m *Message) tbsData(hashType crypto.Hash) ([]byte, error) {
	m.Length = uint64(len(m.Payload))

	buffer := bytes.Buffer{}
	if err := m.writeSignableBytes(&buffer); err != nil {
		return nil, err
	}

	var hash []byte
	if hashType == crypto.Hash(0) {
		hash = buffer.Bytes()
	} else {
		hasher := hashType.New()
		hasher.Write(buffer.Bytes())
		hash = hasher.Sum(nil)
	}

	return hash, nil
}

func (m *Message) Sign(signer crypto.Signer, hashType crypto.Hash) error {
	tbs, err := m.tbsData(hashType)
	if err != nil {
		return err
	}

	switch signerType := signer.(type) {
	case *rsa.PrivateKey:
		m.Signature, err = rsa.SignPSS(rand.Reader, signerType, hashType, tbs, nil)
	default:
		m.Signature, err = signer.Sign(rand.Reader, tbs, hashType)
	}
	return err
}

func (m *Message) CheckSignature(pubKey any, hashType crypto.Hash) error {
	tbs, err := m.tbsData(hashType)
	if err != nil {
		return err
	}

	switch key := pubKey.(type) {
	case ed25519.PublicKey:
		if !ed25519.Verify(key, tbs, m.Signature) {
			return errors.New("signature check failed")
		}
		return nil

	case *rsa.PublicKey:
		return rsa.VerifyPSS(key, hashType, tbs, m.Signature, nil)

	case *ecdsa.PublicKey:
		if !ecdsa.VerifyASN1(key, tbs, m.Signature) {
			return errors.New("signature check failed")
		}
		return nil

	default:
		return fmt.Errorf("signature check for public signer type %T not implemented", key)
	}
}
