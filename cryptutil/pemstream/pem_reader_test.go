package pemstream

import (
	"encoding/pem"
	"fmt"
	"io"
	"math/rand"
	"strings"
	"testing"
)

type testCase struct {
	file   string
	gaps   []string
	blocks []PemBlock
	err    error
}

var (
	testCases = []*testCase{
		// Case 0: The most basic (no CR at EOF)
		{
			file: "-----BEGIN TEST-----\n" +
				"SGVsbG8gV\n" +
				"29ybGQ=\n" +
				"-----END TEST-----",
			gaps: []string{
				"",
			},
			blocks: []PemBlock{
				{
					Block: pem.Block{
						Type:  "TEST",
						Bytes: []byte("Hello World"),
					},
					Indent:     []byte{},
					B64LineLen: 9,
					StartLine:  1,
					EndLine:    4,
				},
			},
		},
		// Case 1: Indented (with CR at EOF)
		{
			file: "\t-----BEGIN TEST-----\n" +
				"\tSGVsbG8gV\n" +
				"\t29ybGQ=\n" +
				"\t-----END TEST-----\n",
			gaps: []string{
				"",
			},
			blocks: []PemBlock{
				{
					Block: pem.Block{
						Type:  "TEST",
						Bytes: []byte("Hello World"),
					},
					Indent:     []byte{'\t'},
					B64LineLen: 9,
					StartLine:  1,
					EndLine:    4,
				},
			},
		},
		// Case 2: With a header and a blank line after
		{
			file: "This is an 8096 bit public key\n" +
				"-----BEGIN PUBLIC KEY-----\n" +
				"Comment: this is a comment header\n" +
				"\n" +
				"ISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZ\n" +
				"WltcXV5fYGFiY2RlZmdoaWprbG1ubwoiIyQlJicoKSorLC0uLzAxMjM0NTY3ODk6Ozw9Pj9AQUJD\n" +
				"REVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2BhYmNkZWZnaGlqa2xtbm9wCiMkJSYnKCkqKywt\n" +
				"Li8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVm\n" +
				"Z2hpamtsbW5vcHEKJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9Q\n" +
				"UVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcgolJicoKSorLC0uLzAxMjM0NTY3ODk6\n" +
				"Ozw9Pj9AQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2BhYmNkZWZnaGlqa2xtbm9wcXJz\n" +
				"CiYnKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaW1xd\n" +
				"Xl9gYWJjZGVmZ2hpamtsbW5vcHFyc3QKJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZH\n" +
				"SElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dQooKSorLC0uLzAx\n" +
				"MjM0NTY3ODk6Ozw9Pj9AQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2BhYmNkZWZnaGlq\n" +
				"a2xtbm9wcXJzdHV2CikqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNU\n" +
				"VVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dncKKissLS4vMDEyMzQ1Njc4OTo7PD0+\n" +
				"P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3\n" +
				"eAorLC0uLzAxMjM0NTY3ODk6Ozw9Pj9AQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2Bh\n" +
				"YmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5CiwtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpL\n" +
				"TE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXoKLS4vMDEyMzQ1\n" +
				"Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1u\n" +
				"b3BxcnN0dXZ3eHl6ewouLzAxMjM0NTY3OA==\n" +
				"-----END PUBLIC KEY-----\n" +
				"And some text at the end too\n",
			gaps: []string{
				"This is an 8096 bit public key\n",
				"And some text at the end too\n",
			},
			blocks: []PemBlock{
				{
					Block: pem.Block{
						Type: "PUBLIC KEY",
						Headers: map[string]string{
							"Comment": "this is a comment header",
						},
						Bytes: []byte("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmno\n" +
							"\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnop\n" +
							"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopq\n" +
							"$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqr\n" +
							"%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrs\n" +
							"&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrst\n" +
							"'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstu\n" +
							"()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuv\n" +
							")*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvw\n" +
							"*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwx\n" +
							"+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxy\n" +
							",-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz\n" +
							"-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{\n" +
							"./012345678",
						),
					},
					WinLineEndings:      false,
					Indent:              []byte{},
					PostHeaderBlankLine: []byte{},
					B64LineLen:          76,
					StartLine:           2,
					EndLine:             24,
				},
			},
		},
	}
)

// pathologicalReader wraps another reader and reduces the amount of data
// read on each Read() call by a random amount. It is designed to exercise
// all the code paths of the parsers my making them cope with awkward buffer
// boundaries
type pathologicalReader struct {
	random   *rand.Rand
	upstream io.Reader
}

func newPathologicalReader(upstream io.Reader, seed int64) io.ReadCloser {
	return &pathologicalReader{
		random:   rand.New(rand.NewSource(seed)),
		upstream: upstream,
	}
}

func (pr *pathologicalReader) Read(p []byte) (n int, err error) {
	if len(p) > 0 {
		p = p[:pr.random.Intn(len(p)+1)]
	}

	return pr.upstream.Read(p)
}

func (pr *pathologicalReader) Close() error {
	if c, ok := pr.upstream.(io.Closer); ok {
		return c.Close()
	}

	return nil
}

// PemReader() returns a PemReader for the test case. If the seed is non-zero
// a pathologicalReader is installed
func (tc *testCase) PemReader(seed int64) *PemReader {
	var r io.Reader
	r = strings.NewReader(tc.file)
	if seed > 0 {
		r = newPathologicalReader(r, seed)
	}
	return NewPemReader(r)
}

func TestPemReader(t *testing.T) {
	testPemReaderWithFuzz(t, 0)
}

func FuzzPemReader(f *testing.F) {
	for _, seed := range []int64{0, 29, 55} {
		f.Add(seed)
	}
	f.Fuzz(testPemReaderWithFuzz)
}

func testPemReaderWithFuzz(t *testing.T, seed int64) {
	t.Logf("seed %d", seed)

	for i, c := range testCases {
		t.Logf("case %d", i)
		err := test(t, c, seed)
		if err != c.err {
			expErrStr := "none"
			if c.err != nil {
				expErrStr = c.err.Error()
			}

			gotErrStr := "none"
			if err != nil {
				gotErrStr = err.Error()
			}
			t.Errorf("test case %d expected error %s, but got %s", i, expErrStr, gotErrStr)
		}
	}
}

func test(t *testing.T, tc *testCase, seed int64) error {
	ps := tc.PemReader(seed)

	for i := 0; i <= max(len(tc.gaps), len(tc.blocks)); i++ {
		var gap *string
		if i < len(tc.gaps) {
			gap = &tc.gaps[i]
		}
		if err := testGap(t, gap, ps); err != nil {
			return fmt.Errorf("gap %d: %w", i, err)
		}

		var block *PemBlock
		if i < len(tc.blocks) {
			block = &tc.blocks[i]
		}
		if err := testBlock(t, block, ps); err != nil {
			return fmt.Errorf("block %d: %w", i, err)
		}
	}

	return nil
}

func testGap(t *testing.T, expected *string, ps *PemReader) error {
	gapStream, err := ps.GetGapDataStream()

	if err != nil {
		if gapStream != nil {
			t.Errorf("got stream and error")
		}
		return fmt.Errorf("fetching gap stream: %w", err)
	}

	if gapStream == nil {
		if expected != nil {
			return fmt.Errorf("expected gap stream but got nil")
		}
		return nil
	}

	gapData, err := io.ReadAll(gapStream)
	if err != nil {
		return fmt.Errorf("reading gap stream:: %w", err)
	}

	if expected == nil && len(gapData) == 0 {
		return nil
	}

	if expected == nil {
		return fmt.Errorf("expected no data but got a gap stream with %d bytes", len(gapData))
	}

	gapString := string(gapData)
	if gapString != *expected {
		return fmt.Errorf("gap data expected %q but got %q", *expected, gapString)
	}

	return nil
}

func testBlock(t *testing.T, expected *PemBlock, ps *PemReader) error {
	block, err := ps.GetPemBlock()
	if err != nil {
		if block != nil {
			t.Errorf("got block and error")
		}
		return fmt.Errorf("fetching block: %w", err)
	}

	if expected == nil && block != nil {
		return fmt.Errorf("expected nil but gock block")
	}

	if expected != nil && block == nil {
		return fmt.Errorf("expected block but got nil")
	}

	if block == nil {
		return nil
	}

	assertEqual(t, expected.Type, block.Type, "type")

	assertEqual(t, len(expected.Bytes), len(block.Bytes), "block data length")
	if at := bufDivergesAt(expected.Bytes, block.Bytes); at < len(expected.Bytes) && len(expected.Bytes) == len(block.Bytes) {
		t.Errorf("block datter differes at byte %d of %d", at, expected.Bytes)
	}

	assertEqual(t, expected.Headers != nil, block.Headers != nil, "has headers")
	if expected.Headers != nil && block.Headers != nil {
		for header, expectedVal := range expected.Headers {
			if blockVal, found := block.Headers[header]; !found {
				t.Errorf("missing %s header", header)
			} else {
				assertEqual(t, string(expectedVal), string(blockVal), "header %s", header)
			}
		}
		for k, v := range block.Headers {
			if _, found := expected.Headers[k]; !found {
				t.Errorf("unnexpected header %s: %s", k, v)
			}
		}
	}

	assertEqual(t, string(expected.Indent), string(block.Indent), "indent")
	assertEqual(t, expected.PostHeaderBlankLine != nil, block.PostHeaderBlankLine != nil, "has post header blank")
	assertEqual(t, string(expected.PostHeaderBlankLine), string(block.PostHeaderBlankLine), "post header blank")
	assertEqual(t, expected.StartLine, block.StartLine, "start line")
	assertEqual(t, expected.EndLine, block.EndLine, "end line")
	assertEqual(t, expected.WinLineEndings, block.WinLineEndings, "win line endings")
	assertEqual(t, expected.B64LineLen, block.B64LineLen, "base64 line encoding length")

	return nil
}

func assertEqual[T comparable](t *testing.T, expected, actual T, format string, args ...any) {
	if expected != actual {
		args = append(args, expected, actual)
		if format != "" {
			format += ": "
		}
		t.Errorf(format+"expected %v but got %v", args...)
	}
}
