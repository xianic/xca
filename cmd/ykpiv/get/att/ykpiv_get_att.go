package att

import (
	"encoding/pem"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/get"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
	"gitlab.com/xianic/xca/yubikey"
	"os"
)

var (
	Cmd = &cobra.Command{
		Use:     "attestation",
		Short:   "get key attestation from Yubikey",
		Aliases: []string{"att"},
		RunE:    attCmd,
	}

	selectionFlags ykpiv.SelectionFlags
	showPemsOpt    bool
)

func init() {
	get.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
	fs.BoolVarP(&showPemsOpt, "show-pems", "p", false, "print attestation and intermediate certificates as PEM blocks")
}

func attCmd(_ *cobra.Command, _ []string) (err error) {
	var yk *piv.YubiKey
	var slot piv.Slot
	yk, slot, err = selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	intCert, err := yk.AttestationCertificate()
	if err != nil {
		return err
	}
	if showPemsOpt {
		_ = pem.Encode(os.Stdout, &pem.Block{
			Type:  cryptutil.PemTypeCertificate,
			Bytes: intCert.Raw,
		})
	}

	att, err := yk.Attest(slot)
	if err != nil {
		return err
	}
	if showPemsOpt {
		_ = pem.Encode(os.Stdout, &pem.Block{
			Type:  cryptutil.PemTypeCertificate,
			Bytes: att.Raw,
		})
	}

	attData, err := piv.Verify(intCert, att)
	if err != nil {
		return err
	}

	fmt.Printf(
		"serial: %d\nversion: %d.%d.%d\nslot: %02x\npin policy: %s\ntouch policy: %s\nform factor: %s\n",
		attData.Serial,
		attData.Version.Major,
		attData.Version.Minor,
		attData.Version.Patch,
		attData.Slot.Key,
		yubikey.PinPolicyName(attData.PINPolicy),
		yubikey.TouchPolicyName(attData.TouchPolicy),
		attData.Formfactor,
	)
	return nil
}
