package pkimodel

import (
	"crypto/sha256"
	"crypto/x509"
	"fmt"
)

type CertificateInFile ObjectInFile[*x509.Certificate]

type Certificate struct {
	inFile CertificateInFile
	cert   *x509.Certificate

	issuer *Certificate
	issued []*Certificate
	key    *PrivateKey
}

func NewCertificate(oif CertificateInFile) (cif *Certificate, err error) {
	cif = &Certificate{
		inFile: oif,
	}

	cif.cert, err = oif.GetObject()
	if err != nil {
		err = fmt.Errorf("parsing certificate from %s: %w", oif.String(), err)
		cif = nil
	}

	return
}

func (c *Certificate) File() DataInFile {
	return c.inFile
}

func (c *Certificate) Parsed() *x509.Certificate {
	return c.cert
}

func (c *Certificate) Update(n *x509.Certificate) error {
	if err := c.inFile.SetObject(n); err != nil {
		return err
	}

	c.cert = n
	c.issued = nil
	return nil
}

func (c *Certificate) Fingerprint() []byte {
	hash := sha256.Sum256(c.cert.Raw)
	return hash[:]
}

func (c *Certificate) FingerprintHex() string {
	return fmt.Sprintf("%X", c.Fingerprint())
}

func (c *Certificate) Issuer() *Certificate {
	return c.issuer
}

func (c *Certificate) Issued() []*Certificate {
	return c.issued
}

func (c *Certificate) Key() *PrivateKey {
	return c.key
}

func (c *Certificate) String() string {
	return fmt.Sprintf(
		"%s [%X] (%s)",
		c.cert.Subject.String(),
		c.Fingerprint()[:4],
		c.inFile.String(),
	)
}
