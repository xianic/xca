package fixed

import (
	"crypto/x509"

	"gitlab.com/xianic/xca/scanner/pkimodel"
)

type Certificate struct {
	cert       *x509.Certificate
	originName string
}

func (f *Certificate) SetRawObject(bytes []byte) {
	panic("fixed certificate cannot be updated")
}

func (f *Certificate) GetObject() (*x509.Certificate, error) {
	return f.cert, nil
}

func (f *Certificate) SetObject(certificate *x509.Certificate) error {
	panic("fixed certificate cannot be updated")
}

func (f *Certificate) ParsePrivateKeys() ([]*pkimodel.PrivateKey, error) {
	return nil, nil
}

func (f *Certificate) TieBreakPrivateKeys(cert *pkimodel.Certificate, keys []*pkimodel.PrivateKey) *pkimodel.PrivateKey {
	panic("only one cert in file, should not need a tie break")
}

func (f *Certificate) MarkUpdated() {
}

func (f *Certificate) Save() (anyChanges bool, err error) {
	return false, err
}

func NewFixedCert(cert *x509.Certificate, originName string) pkimodel.PkiFile {
	return &Certificate{
		cert:       cert,
		originName: originName,
	}
}

func (f *Certificate) String() string {
	return f.originName + " " + f.cert.Subject.String()
}

func (f *Certificate) ParseCertificates() ([]*pkimodel.Certificate, error) {
	cert, err := pkimodel.NewCertificate(f)
	if err != nil {
		return nil, err
	}

	return []*pkimodel.Certificate{
		cert,
	}, nil
}

func (f *Certificate) File() pkimodel.PkiFile {
	return f
}

func (f *Certificate) GetRawObject() []byte {
	return f.cert.Raw
}

func (f *Certificate) TieBreakCertificates(_ *pkimodel.Certificate, _ []*pkimodel.Certificate) *pkimodel.Certificate {
	panic("tie break needed for fixed certificate")
}
