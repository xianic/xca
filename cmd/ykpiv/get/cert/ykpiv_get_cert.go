package att

import (
	"encoding/pem"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/get"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
	"os"
)

var (
	Cmd = &cobra.Command{
		Use:     "certificate",
		Short:   "get certificate from Yubikey",
		Aliases: []string{"cert"},
		RunE:    certCmd,
	}

	selectionFlags ykpiv.SelectionFlags
)

func init() {
	get.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
}

func certCmd(_ *cobra.Command, _ []string) (err error) {
	var yk *piv.YubiKey
	var slot piv.Slot
	yk, slot, err = selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	cert, err := yk.Certificate(slot)
	if err != nil {
		return err
	}
	return pem.Encode(os.Stdout, &pem.Block{
		Type:  cryptutil.PemTypeCertificate,
		Bytes: cert.Raw,
	})
}
