#!/usr/bin/env bats

load helpers.bash

@test "gen key types" {
	for keytype in $KEYTYPES
	do
	    KEY="$($XCA_TOOL gen key -t $keytype)"
	    show_compare test/data/show_gen_key_$keytype.txt <<<"$KEY"
	done
}

@test "gen key default type to file" {
    KEY_FILE="$BATS_TMPDIR/default-key.pem"
	$XCA_TOOL gen key "$KEY_FILE"
	show_compare test/data/show_gen_key_P-256.txt < "$KEY_FILE"
}

@test "gen key w/ password from file minimal argon2" {
    PW="foo"
    KEY="$($XCA_TOOL gen key --argon2 0:0:0 -p test/data/password.txt)"
	show_compare test/data/show_gen_key_P-256_pw_minimal.txt <<<"$KEY"
}

@test "gen key w/ password from file 8 iterations" {
    PW="foo"
    KEY="$($XCA_TOOL gen key --argon2 8:0:0 -p test/data/password.txt)"
	show_compare test/data/show_gen_key_P-256_pw_8it.txt <<<"$KEY"
}

@test "gen key w/ password from file 256MB RAM" {
    PW="foo"
    KEY="$($XCA_TOOL gen key --argon2 0:256:0 -p test/data/password.txt)"
	show_compare test/data/show_gen_key_P-256_pw_256mb.txt <<<"$KEY"
}

@test "gen key w/ password from file 8 threads" {
    PW="foo"
    KEY="$($XCA_TOOL gen key --argon2 0:0:8 -p test/data/password.txt)"
	show_compare test/data/show_gen_key_P-256_pw_8th.txt <<<"$KEY"
}
