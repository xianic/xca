package key

import (
	"errors"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/im"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/keytype"
	"gitlab.com/xianic/xca/yubikey"
)

var (
	Cmd = &cobra.Command{
		Use:   "key [keyfile | -]",
		Short: "import and key on a Yubikey",
		Long: `This is not as secure as generating a key on a Yubikey where it is
impractical to extract it again`,
		RunE: importKeyCmd,
	}

	cryptFlags     cmd.PemDecryptionFlags
	forceFlag      ykpiv.ForceFlag
	keyFlags       ykpiv.KeyFlags
	selectionFlags ykpiv.SelectionFlags
	pinFlags       ykpiv.PinFlag
)

func init() {
	im.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	cryptFlags.Init(fs)
	forceFlag.Init(fs, "allow overwrite of existing key or certificate")
	keyFlags.Init(fs)
	selectionFlags.Init(fs)
	pinFlags.Init(fs, ".")
}

func importKeyCmd(cmd *cobra.Command, args []string) error {
	if len(args) == 0 {
		args = []string{"-"}
	}

	keyfile := cryptutil.UnmarshalFile(args[0], "private key", cryptFlags.ParseForDecryption())

	privKey, err := keyfile.ExpectPrivateKey()
	if err != nil {
		return fmt.Errorf("loading private key: %w", err)
	}

	yk, slot, err := selectionFlags.GetYubikey()
	if err != nil {
		return err
	}

	var key piv.Key
	key.PINPolicy, key.TouchPolicy, err = keyFlags.ParsePolicies()
	if err != nil {
		return err
	}

	key.Algorithm, err = yubikey.AlgoParse(keytype.TypeOf(privKey).String())
	if err != nil {
		return fmt.Errorf("%q: %w", args[0], err)
	}

	_, err = yk.Attest(slot)
	if err == nil {
		if !forceFlag {
			return fmt.Errorf("refusing to overwrite existing key, use -f to force")
		}
	} else if !errors.Is(err, piv.ErrNotFound) {
		return err
	}

	if !forceFlag {
		_, err = yk.Certificate(slot)
		if !errors.Is(err, piv.ErrNotFound) {
			return fmt.Errorf("refusing to overwrite existing certificate, use -f to force")
		}
	}

	slotCert, err := yubikey.GenerateSlotCert(
		yk,
		cryptutil.PublicKeyFor(privKey),
		privKey,
		slot,
		key.TouchPolicy,
		key.PINPolicy,
	)
	if err != nil {
		return fmt.Errorf("generating slot certificate: %w", err)
	}

	mk, _, err := pinFlags.Unlock(yk)
	if err != nil {
		return err
	}

	err = yk.SetPrivateKeyInsecure(mk, slot, privKey, key)
	if err != nil {
		return fmt.Errorf("importing private key: %w", err)
	}

	err = yk.SetCertificate(mk, slot, slotCert)
	if err != nil {
		return fmt.Errorf("importing slot certificate: %w", err)
	}

	return nil
}
