package pemblockfile

import (
	"crypto/rsa"
	"crypto/x509"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/cryptutil/pemstream"
	"gitlab.com/xianic/xca/scanner/pkimodel"
)

type PemInFile struct {
	file *PemBlockFile
	pem  *pemstream.PemBlock
}

type CertInFile struct {
	PemInFile
}

type PrivateKeyInFile struct {
	PemInFile
}

func (p *PemInFile) SetRawObject(bytes []byte) {
	p.pem.Bytes = bytes
	p.file.MarkUpdated()
}

func (p *PemInFile) File() pkimodel.PkiFile {
	return p.file
}

func (p *PemInFile) String() string {
	return p.file.String() + fmt.Sprintf(":%d-%d", p.pem.StartLine, p.pem.EndLine)
}

func (p *PemInFile) GetRawObject() []byte {
	return p.pem.Bytes
}

func (p *CertInFile) GetObject() (*x509.Certificate, error) {
	return x509.ParseCertificate(p.GetRawObject())
}

func (p *CertInFile) SetObject(cert *x509.Certificate) error {
	p.SetRawObject(cert.Raw)
	return nil
}

func (p *PrivateKeyInFile) GetObject() (any, error) {
	if p.pem.Type == cryptutil.PemTypeRsaPrivateKey {
		return x509.ParsePKCS1PrivateKey(p.GetRawObject())
	}

	return x509.ParsePKCS8PrivateKey(p.GetRawObject())
}

func (p *PrivateKeyInFile) SetObject(key any) error {
	if p.pem.Type == cryptutil.PemTypeRsaPrivateKey {
		if rsaKey, ok := key.(*rsa.PrivateKey); ok {
			p.SetRawObject(x509.MarshalPKCS1PrivateKey(rsaKey))
		} else {
			return fmt.Errorf("PKCS1 encoded private key can only be replaced by another RSA key")
		}
	} else {
		m, err := x509.MarshalPKIXPublicKey(key)
		if err != nil {
			return err
		}
		p.SetRawObject(m)
	}

	return nil
}
