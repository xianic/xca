package cmd_scan

type treePrinter struct {
	indent string
	levels []bool
}

func newTreePrinter(indent string) *treePrinter {
	return &treePrinter{
		indent: indent,
		levels: nil,
	}
}

func (t *treePrinter) Prefix(last bool) string {
	str := ""
	for _, level := range t.levels {
		if level {
			str += "|   "
		} else {
			str += "    "
		}
	}
	if t.levels != nil {
		if last {
			str += "\\-> "
		} else {
			str += "+-> "
		}
	}

	return str
}

func (t *treePrinter) AddLevel(more bool) {
	if t.levels == nil {
		t.levels = make([]bool, 0)
	} else {
		t.levels = append(t.levels, more)
	}
}

func (t *treePrinter) RemoveLevel() {
	if l := len(t.levels); l > 0 {
		t.levels = t.levels[0 : l-1]
	} else {
		t.levels = nil
	}
}
