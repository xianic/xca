package ykpiv

import (
	"fmt"
	"github.com/go-piv/piv-go/piv"
	flag "github.com/spf13/pflag"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/yubikey"
	"strings"
)

func commaJoin(strs []string) string {
	return strings.Join(strs, ", ")
}

type SelectionFlags struct {
	OptSlot   string
	OptSerial uint32
	OptYkName string
}

func (f *SelectionFlags) Init(fs *flag.FlagSet) {
	fs.StringVarP(&f.OptSlot, "piv-slot", "o", "9c", "PIV slot to use for key operations, one of "+yubikey.SlotDescriptions())
	fs.Uint32VarP(&f.OptSerial, "serial", "", 0, "serial number of Yubikey to use")
	fs.StringVarP(&f.OptYkName, "yk-name", "", "", "name of the Yubikey to use.")
}

func (f *SelectionFlags) GetYubikey() (*piv.YubiKey, piv.Slot, error) {
	slot, err := yubikey.SlotParse(f.OptSlot)

	if err != nil {
		return nil, slot, err
	}

	var yk *piv.YubiKey
	if f.OptSerial != 0 {
		yk, err = yubikey.GetCardBySerial(f.OptSerial)
	} else {
		yk, err = yubikey.GetCardByName(f.OptYkName)
	}
	if err != nil {
		return nil, slot, err
	}

	return yk, slot, nil
}

type ForceFlag bool

func (f *ForceFlag) Init(flags *flag.FlagSet, helpText string) {
	flags.BoolVarP((*bool)(f), "force", "f", false, helpText)
}

type KeyFlags struct {
	OptPinPolicy   string
	OptTouchPolicy string
}

func (f *KeyFlags) Init(fs *flag.FlagSet) {
	fs.StringVarP(&f.OptPinPolicy, "pin-policy", "", "once", "pin policy for key use, one of "+commaJoin(yubikey.PinPolicyNames()))
	fs.StringVarP(&f.OptTouchPolicy, "touch-policy", "", "cached", "touch policy for key use, one of "+commaJoin(yubikey.TouchPolicyNames()))
}

func (f *KeyFlags) ParsePolicies() (pp piv.PINPolicy, tp piv.TouchPolicy, err error) {
	pp, err = yubikey.PinPolicyParse(f.OptPinPolicy)
	if err != nil {
		return
	}

	tp, err = yubikey.TouchPolicyParse(f.OptTouchPolicy)

	return
}

type KeyGenFlags struct {
	KeyFlags
	OptAlgo string
}

func (f *KeyGenFlags) Init(fs *flag.FlagSet) {
	f.KeyFlags.Init(fs)
	fs.StringVarP(&f.OptAlgo, "algorithm", "t", "P-256", "type of key to generate, one of "+commaJoin(yubikey.AlgoNames()))
}

func (f *KeyGenFlags) PivKey() (key *piv.Key, err error) {
	key = new(piv.Key)

	if f.OptAlgo != "" {
		key.Algorithm, err = yubikey.AlgoParse(f.OptAlgo)
		if err != nil {
			return nil, err
		}
	}

	key.PINPolicy, key.TouchPolicy, err = f.ParsePolicies()
	if err != nil {
		return nil, err
	}

	return key, nil
}

type PinFlag cmd.PromptFlag

func (f *PinFlag) Init(fs *flag.FlagSet, defaultValue string) {
	(*cmd.PromptFlag)(f).Init(fs, "pin", "n", defaultValue, "Yubikey pin")
}

func (f *PinFlag) PromptFlag() *cmd.PromptFlag {
	return (*cmd.PromptFlag)(f)
}

func (f *PinFlag) Unlock(yk *piv.YubiKey) (managementKey [24]byte, pin string, err error) {
	managementKey = piv.DefaultManagementKey

	if !f.PromptFlag().IsSet() {
		return piv.DefaultManagementKey, piv.DefaultPIN, nil
	}

	pin, err = f.PivPrompt()

	var mt *piv.Metadata
	mt, err = yk.Metadata(string(pin))
	if err != nil {
		err = fmt.Errorf("unlocking ubikey with pin: %w", err)
		return
	}

	if mt.ManagementKey != nil {
		managementKey = *mt.ManagementKey
	}
	return
}

func (f *PinFlag) PivPrompt() (string, error) {
	if *f == "" {
		return piv.DefaultPIN, nil
	}

	pinBytes, err := f.PromptFlag().Prompt().Prompt("pin", false)
	if err != nil {
		err = fmt.Errorf("gitting pin: %w", err)
		return "", err
	}

	return string(pinBytes), nil
}

type PukFlag cmd.PromptFlag

func (f *PukFlag) Init(fs *flag.FlagSet, defaultValue string) {
	(*cmd.PromptFlag)(f).Init(fs, "puk", "k", defaultValue, "PUK for Yubikey")
}
