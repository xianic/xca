package cmd_scan

import (
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/scanner/pkimodel"
)

func init() {
	addReport("leaves", reportLeaves)
}

func reportLeaves(pc *pkimodel.Collection) error {
	certs := selectAndSortCerts(pc.Certificates, func(c *pkimodel.Certificate) bool {
		return len(c.Issued()) == 0 && !c.Parsed().IsCA
	}, sortById)

	for _, c := range certs {
		printLeafTree(nil, c, nil)
		fmt.Println()
	}

	return nil
}

func printLeafTree(tp *treePrinter, c *pkimodel.Certificate, suffix func(*pkimodel.Certificate) string) {
	if tp == nil {
		tp = newTreePrinter("")
	}
	suffixStr := ""
	if suffix != nil {
		suffixStr = suffix(c)
	}
	if suffixStr != "" {
		suffixStr = " " + suffixStr
	}

	key := c.Key()
	fmt.Printf("%s%s%s\n", tp.Prefix(true), c, suffixStr)
	tp.AddLevel(false)
	defer tp.RemoveLevel()
	if key != nil {
		fmt.Printf("%sKEY: %s\n", tp.Prefix(false), key)
	}

	if c.Issuer() == nil {
		if !cryptutil.CertificateIsSelfSigned(c.Parsed()) {
			if suffix == nil {
				suffixStr = ""
			} else {
				suffixStr = suffix(c.Issuer())
			}
			if suffixStr != "" {
				suffixStr = " " + suffixStr
			}
			fmt.Printf("%s%s (MISSING)%s\n", tp.Prefix(true), c.Parsed().Issuer.String(), suffixStr)
		}
	} else {
		printLeafTree(tp, c.Issuer(), suffix)
	}
}
