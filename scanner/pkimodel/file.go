package pkimodel

// PkiFile represents a file (or other RW object) which may contain a
// collection of cryptographic objects. The objects will be parsed and made
// available and may also be replaced and the modified file written back to
// disk.
type PkiFile interface {
	// String returns a reference to where to find this file, typically this
	// is an absolute path.
	String() string

	// ParseCertificates returns the parsed certificates from the file. See
	// NewCertificate.
	ParseCertificates() ([]*Certificate, error)

	// ParsePrivateKeys returns the parsed keys from a
	ParsePrivateKeys() ([]*PrivateKey, error)

	// TieBreakCertificates is called when there are multiple CA certificates
	// any of which may have issued the leaf certificate. If a certificate
	// in the file twice then the same cryptographic object may be listed more
	// than once in the issuers list. All issuers will have originated from
	// the file on which this function is called.
	TieBreakCertificates(leaf *Certificate, issuers []*Certificate) *Certificate

	TieBreakPrivateKeys(cert *Certificate, keys []*PrivateKey) *PrivateKey

	// MarkUpdated sets a flag within the object so that when Save() is called
	// the file will be written back to disk along with any updates made to
	// the objects that were parsed from it.
	MarkUpdated()

	// Save the file back to disk along with any updates. If no updates were
	// made (i.e. MarkUpdated() was not called) then this may be a no-op.
	// anyChanges will be ture if MarkUpdated() was called prior the last call
	// to Save()
	Save() (anyChanges bool, err error)
}

// DataInFile represents a chunk fo data in a PkiFile that represents a single
// cryptographic object. It also provides an interface to the decoded form of
// the object. This may represent a PEM block found within a text file.
type DataInFile interface {
	// File in which this PKI object was found
	File() PkiFile

	// String returns a reference to where this data is found withing the file.
	// Typically, this is PkiFile.String() + "#" + reference_in_file
	String() string

	// GetRawObject returns the raw data of the cryptographic object store in
	// the file. For a PEM file, this would be the DER encoding.
	GetRawObject() []byte

	// SetRawObject stores a new (updated) object in the file structure but
	// does not write it back to disk until Save() is called on the PkiFile
	// object returned by File().
	SetRawObject([]byte)
}

// ObjectInFile is an extension the DataInFIle allowing access to the parsed
// form of the cryptographic object.
type ObjectInFile[T any] interface {
	DataInFile

	GetObject() (T, error)

	SetObject(T) error
}
