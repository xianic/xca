package ykpiv

import (
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
)

var (
	Cmd = &cobra.Command{
		Use:   "ykpiv [options] command",
		Short: "commands for managing PIV on Yubikeys",
	}
)

func init() {
	cmd.RootCmd.AddCommand(Cmd)
}
