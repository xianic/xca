package file

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/pemcrypt"
	"gitlab.com/xianic/xca/prompt"
	"gitlab.com/xianic/xca/util"
	"io"
)

const (
	DriverName         = "file"
	DefaultKeyFileName = "ca.key"
)

type fileCaFactory struct {
	decryptFlags cmd.PemDecryptionFlags
}

type fileCaDriver struct {
	caConfig     *ca.Config
	driverConfig *Config
}

type Config struct {
	KeyPath string
}

func init() {
	ca.AddDriver(DriverName, new(fileCaFactory))
}

func (_ *fileCaFactory) String() string {
	return "store CA private key in a file, optionally encrypted"
}

func (_ *fileCaFactory) NewDriverConfig() any {
	return new(Config)
}

func (_ *fileCaFactory) NewDriver(caConfig *ca.Config, driverConfig any) (ca.Driver, error) {
	return &fileCaDriver{
		caConfig:     caConfig,
		driverConfig: driverConfig.(*Config),
	}, nil
}

func (f *fileCaDriver) InitPrivateKey(p prompt.PasswordPrompt) (pubKey crypto.PublicKey, certCustomiser ca.CertCustomiser, signer crypto.Signer, err error) {
	privKeyPath := f.PrivateKeyPath()
	created, err := util.InitFile(
		privKeyPath,
		0600,
		func(w io.Writer) error {
			priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
			if err != nil {
				return err
			}

			der, err := x509.MarshalPKCS8PrivateKey(priv)
			if err != nil {
				return err
			}

			block := &pem.Block{
				Type:  cryptutil.PemTypePrivateKey,
				Bytes: der,
			}

			err = pem.Encode(w, block)
			if err != nil {
				return err
			}

			pubKey = cryptutil.PublicKeyFor(priv)
			signer = priv
			return nil
		},
	)

	if !created {
		privKey, err := cryptutil.UnmarshalFile(privKeyPath, "", &pemcrypt.CryptOptions{
			PasswordSource: p,
		}).ExpectPrivateKey()
		if err != nil {
			return nil, nil, nil, err
		}

		signer, ok := privKey.(crypto.Signer)
		if !ok {
			return nil, nil, nil, fmt.Errorf("%T is not a signer", privKey)
		}

		return cryptutil.PublicKeyFor(privKey), nil, signer, nil
	}

	return
}

func (f *fileCaDriver) GetSigner(certificate *x509.Certificate, prompt prompt.PasswordPrompt) (signer crypto.Signer, err error) {
	var privateKey crypto.PrivateKey
	privateKey, err = cryptutil.UnmarshalFile(f.PrivateKeyPath(), "", &pemcrypt.CryptOptions{
		PasswordSource: prompt,
	}).ExpectPrivateKey()

	if err != nil {
		return nil, err
	}

	loadedPubKeyFp := cryptutil.PublicKeyFingerprint(cryptutil.PublicKeyFor(privateKey))
	certPubKeyFp := cryptutil.PublicKeyFingerprint(certificate.PublicKey)
	if !bytes.Equal(loadedPubKeyFp, certPubKeyFp) {
		return nil, errors.New("CA private key and CA certificate do not match")
	}

	var ok bool
	if signer, ok = privateKey.(crypto.Signer); !ok {
		return nil, fmt.Errorf("private key of type %T not usable as a signer", privateKey)
	}

	return
}

func (f *fileCaDriver) SetCertificate(_ *x509.Certificate, _ prompt.PasswordPrompt) error {
	return nil
}

func (f *fileCaDriver) PrivateKeyPath() string {
	return f.caConfig.RelConfigPath(f.driverConfig.KeyPath, DefaultKeyFileName)
}

func (_ *fileCaDriver) Close() error {
	return nil
}
