# How to get XCA

As XCA is under active development the only recommended way to install it is
to download the source code and build it from source.

1. [Download and Install](https://go.dev/doc/install) the Golang tools.
2. Download the XCA source code
   [[zip][zip] [tar.gz][tgz] [tar.bz2][tbz] [tar][tar]]
   and unpack it.
3. Open a terminal or command prompt in the directory where the source code
   was unpacked and run the command `go install`. This will build and install
   the XCA binary.

[zip]: https://gitlab.com/xianic/xca/-/archive/main/xca-main.zip
[tar]: https://gitlab.com/xianic/xca/-/archive/main/xca-main.tar
[tgz]: https://gitlab.com/xianic/xca/-/archive/main/xca-main.tar.gz
[tbz]: https://gitlab.com/xianic/xca/-/archive/main/xca-main.tar.bz2
