#!/usr/bin/env bats

load helpers.bash

function setup_file() {
    setup_keys
}

function teardown_file() {
    teardown_keys
}

@test "selfsigned from each key type" {
    for kt in $KEYTYPES
    do
        CERT="$($XCA_TOOL gen selfsigned --subject "Hello World" "$BATS_FILE_TMPDIR/key_$kt.pem" -)"
        show_compare test/data/show_gen_selfsigned_subject_simple_$kt.txt <<<"$CERT"
    done
}

@test "selfsigned subject complex" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "/CN=Common Name/SN=Surname/serialNumber=5/C=Country 1/L=Locality 1/ST=Province 1/streetAddress=Street 1/O=Org 1/OU=Org unit 1/title=Title/postalCode=Postcode 1/2.5.4.41=Name/GN=Given Name/initials=IN/generationQualifier=Generation Qualifier/dnQualifier=DN Qualifier/DC=Domain Component/emailAddress=user@example.com/userid=User ID" \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_subject_complex.txt <<<"$CERT"
}

@test "selfsigned subject from host" {
    CERT="$($XCA_TOOL gen selfsigned \
        --host www.example.com --host example.com \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_subject_from_host.txt <<<"$CERT"
}

@test "selfsigned subject and host" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "Example Certificate" --host www.example.com --host example.com \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_subject_and_host.txt <<<"$CERT"
}

@test "selfsigned demand for subject or host" {
    run $XCA_TOOL gen selfsigned "$BATS_FILE_TMPDIR/key_default.pem" /dev/null
    [ "$status" -eq 1 ]
    grep -qF "at least one of --subject or --host must be specified" <<<"$output" > /dev/null
}

@test "selfsigned from encrypted key" {
    CERT="$($XCA_TOOL gen selfsigned \
        -p test/data/password.txt \
        --subject "Hello World" \
        "$BATS_FILE_TMPDIR/key_default_enc.pem" -)"
    show_compare test/data/show_gen_selfsigned_subject_simple_P-256.txt <<<"$CERT"
}

@test "selfsigned IP SANs" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "Example Certificate" \
        --ip 198.51.100.1 \
        --ip 2001:db8:0:1::1 \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_subject_ip_sans.txt <<<"$CERT"
}

@test "selfsigned key usage specific" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "Hello World" \
        --ext-key-usage "OCSPSigning, TimeStamping" \
        --key-usage "CertSign, CRLSign" \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_key_usage.txt <<<"$CERT"
}

@test "selfsigned key usage none" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "Hello World" \
        --ext-key-usage none \
        --key-usage none \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
    show_compare test/data/show_gen_selfsigned_key_usage_none.txt <<<"$CERT"
}

@test "selfsigned constraints" {
    CERT="$($XCA_TOOL gen selfsigned \
        --subject "Hello World" \
        --ca \
        --permit-email example.com \
        --exclude-email test@example.com \
        --permit-host example.com \
        --exclude-host test.example.com \
        --permit-net 192.0.2.0/24 \
        --exclude-net 192.0.2.1/32 \
        --permit-net 2001:db8::/32 \
        --exclude-net 2001:db8:1::/40 \
        --permit-uri https://example.com/ \
        --exclude-uri https://example.com/test \
        --max-path-length 2 \
        "$BATS_FILE_TMPDIR/key_default.pem" -)"
        show_compare test/data/show_gen_selfsigned_constraints.txt <<<"$CERT"
}

@test "selfsigned temporal validity" {
    # The defaults are equivalent to --predate 5m --lifetime 3650 (10 years)
    read from_diff to_diff < <(gen_validity "$BATS_FILE_TMPDIR/key_default.pem")
    [ "$from_diff" -gt 290 -a "$from_diff" -lt 310 ] # 300 seconds = 5 minutes
    [ "$to_diff" -gt 315359990 -a "$to_diff" -lt 315360010 ] # 315360010 seconds = 10 years

    read from_diff to_diff < <(gen_validity "$BATS_FILE_TMPDIR/key_default.pem" --predate 1h)
    [ "$from_diff" -gt 3590 -a "$from_diff" -lt 3610 ] # 3600 seconds = 1 hour
    [ "$to_diff" -gt 315359990 -a "$to_diff" -lt 315360010 ] # 315360010 seconds = 10 years

    read from_diff to_diff < <(gen_validity "$BATS_FILE_TMPDIR/key_default.pem" --lifetime 180)
    [ "$from_diff" -gt 290 -a "$from_diff" -lt 310 ] # 300 seconds = 5 minutes
    [ "$to_diff" -gt 15551990 -a "$to_diff" -lt 15552010 ] # 15552000 seconds = 180 days

    read from_diff to_diff < <(gen_validity "$BATS_FILE_TMPDIR/key_default.pem" --predate 1h --lifetime 180)
    [ "$from_diff" -gt 3590 -a "$from_diff" -lt 3610 ] # 3600 seconds = 1 hour
    [ "$to_diff" -gt 15551990 -a "$to_diff" -lt 15552010 ] # 15552000 seconds = 180 days
}
