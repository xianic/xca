package cmd

var lateInit []func()

func AddLateInit(f func()) {
	if lateInit != nil {
		lateInit = make([]func(), 0, 1)
	}
	lateInit = append(lateInit, f)
}

func RunLateInit() {
	for _, f := range lateInit {
		f()
	}
	lateInit = nil
}
