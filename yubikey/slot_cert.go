package yubikey

import (
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"gitlab.com/xianic/xca/cryptutil"
	"math/big"
	"time"
)

var (
	// Defined by Yubico
	ExtIDFirmwareVersion = asn1.ObjectIdentifier([]int{1, 3, 6, 1, 4, 1, 41482, 3, 3})
	ExtIDSerialNumber    = asn1.ObjectIdentifier([]int{1, 3, 6, 1, 4, 1, 41482, 3, 7})
	ExtIDKeyPolicy       = asn1.ObjectIdentifier([]int{1, 3, 6, 1, 4, 1, 41482, 3, 8})
	ExtIDFormFactor      = asn1.ObjectIdentifier([]int{1, 3, 6, 1, 4, 1, 41482, 3, 9})

	// 1.3.6.1.4.1.61515 Xian Stannard
	// 1.3.6.1.4.1.61515.1 XCA
	// 1.3.6.1.4.1.61515.1.1 Yubikey slot a certificate is stored in
	ExtIDSlot = asn1.ObjectIdentifier([]int{1, 3, 6, 1, 4, 1, 61515, 1, 1})
)

func GenExtFirmwareVersion(v piv.Version) pkix.Extension {
	return pkix.Extension{
		Id:       ExtIDFirmwareVersion,
		Critical: false,
		Value: []byte{
			byte(v.Major),
			byte(v.Minor),
			byte(v.Patch),
		},
	}
}

func ParseExtFirmwareVersion(d []byte) (piv.Version, error) {
	if len(d) != 3 {
		return piv.Version{}, fmt.Errorf("expected 3 bytes but got %d", len(d))
	}

	return piv.Version{
		Major: int(d[0]),
		Minor: int(d[1]),
		Patch: int(d[2]),
	}, nil
}

func GenExtSerialNumber(serial uint32) pkix.Extension {
	s := int(serial)
	der, err := asn1.Marshal(s)
	if err != nil {
		panic(err) // this marshalaling is deterministic so an error here is a bug
	}
	return pkix.Extension{
		Id:       ExtIDSerialNumber,
		Critical: false,
		Value:    der,
	}
}

func ParseExtSerialNumber(d []byte) (serial uint32, err error) {
	var s int
	_, err = asn1.Unmarshal(d, &s)
	serial = uint32(s)
	return
}

func GenExtKeyPolicy(tp piv.TouchPolicy, pp piv.PINPolicy) pkix.Extension {
	ext := pkix.Extension{
		Id:       ExtIDKeyPolicy,
		Critical: false,
		Value:    make([]byte, 2),
	}

	switch pp {
	case piv.PINPolicyNever:
		ext.Value[0] = 0x01
	case piv.PINPolicyOnce:
		ext.Value[0] = 0x02
	case piv.PINPolicyAlways:
		ext.Value[0] = 0x03
	}

	switch tp {
	case piv.TouchPolicyNever:
		ext.Value[1] = 0x01
	case piv.TouchPolicyAlways:
		ext.Value[1] = 0x02
	case piv.TouchPolicyCached:
		ext.Value[1] = 0x03
	}

	return ext
}

func GenExtSlotNumber(slot uint32) pkix.Extension {
	s := int(slot)
	der, err := asn1.Marshal(s)
	if err != nil {
		panic(err) // this marshalaling is deterministic so an error here is a bug
	}
	return pkix.Extension{
		Id:       ExtIDSlot,
		Critical: false,
		Value:    der,
	}
}

func ParseExtSlotNumber(d []byte) (slot uint32, err error) {
	var s int
	_, err = asn1.Unmarshal(d, &s)
	slot = uint32(s)
	return
}

func ParseExtKeyPolicy(value []byte) (tp piv.TouchPolicy, pp piv.PINPolicy, err error) {
	if len(value) != 2 {
		return 0, 0, fmt.Errorf("expected 2 bytes but got %d", len(value))
	}

	switch value[0] {
	case 0x01:
		pp = piv.PINPolicyNever
	case 0x02:
		pp = piv.PINPolicyOnce
	case 0x03:
		pp = piv.PINPolicyAlways
	}

	switch value[1] {
	case 0x01:
		tp = piv.TouchPolicyNever
	case 0x02:
		tp = piv.TouchPolicyAlways
	case 0x03:
		tp = piv.TouchPolicyCached
	}

	return
}

func GenExtFormFactor(ff piv.Formfactor) pkix.Extension {
	return pkix.Extension{
		Id:       ExtIDFormFactor,
		Critical: false,
		Value:    []byte{byte(ff)},
	}
}

func ParseExtFormFactor(d []byte) (piv.Formfactor, error) {
	if len(d) != 1 {
		return piv.Formfactor(0), fmt.Errorf("expected 1 byte but got: %d", len(d))
	}
	return piv.Formfactor(d[0]), nil
}

// GenerateSlotCert create a certificate that is stored in a Yubikey slot to
// indicate that a key was imported into that slot. It serves as nothing more
// than a label on the outside of the tin that contains the private key and
// should only be used for information. It should not be relied upon for
// anything cryptographic. It is not possible to ask all but the most recent
// of Yubikeys for the public part of a key that was imported into a slot,
// hence the need for a certificate to be used as a label. It is possible to
// update the key and certificate in a slot independently so the presence of
// a certificate in a slot does not guarantee the presence of the
// corresponding private key in the slot, but it is the best indicator we
// have when the slot cannot produce an attestation certificate.
func GenerateSlotCert(yk *piv.YubiKey, pubKey crypto.PublicKey, signer any, slot piv.Slot, tp piv.TouchPolicy, pp piv.PINPolicy) (*x509.Certificate, error) {
	certSerialBytes := make([]byte, 20)
	if _, err := rand.Read(certSerialBytes); err != nil {
		return nil, fmt.Errorf("reading random data: %w", err)
	}

	version := yk.Version()

	ykSerial, err := yk.Serial()
	if err != nil {
		return nil, err
	}

	now := time.Now()
	signatureAlgorithm := cryptutil.SignatureAlgorithmForKey(pubKey, cryptutil.DefaultSignatureHash)
	template := &x509.Certificate{
		SerialNumber:       new(big.Int).SetBytes(certSerialBytes),
		SignatureAlgorithm: signatureAlgorithm,

		Subject: pkix.Name{
			CommonName: fmt.Sprintf("Key imported into Yubikey %d slot %02X", ykSerial, slot.Key),
		},

		// NotBefore and NotAfter are set to the same value ensuring that at
		// no point is this certificate ever valid.
		NotBefore: now,
		NotAfter:  now,

		// No key usage because... don't use this certificate.
		KeyUsage:    0,
		ExtKeyUsage: make([]x509.ExtKeyUsage, 0),

		ExtraExtensions: []pkix.Extension{
			GenExtKeyPolicy(tp, pp),
			GenExtSerialNumber(ykSerial),
			GenExtFirmwareVersion(version),
			GenExtSlotNumber(slot.Key),
		},
	}

	newCertBytes, err := x509.CreateCertificate(rand.Reader, template, template, pubKey, signer)
	if err != nil {
		return nil, fmt.Errorf("genrating certificate: %w", err)
	}

	newCert, err := x509.ParseCertificate(newCertBytes)
	if err != nil {
		return nil, fmt.Errorf("parsing new certificate: %w", err)
	}

	return newCert, nil
}

// PoliciesFromSlotCert extracts the touch and pin policies from a slot
// certificate. They are a helpful indicator to know how to set up the signer
// obtained from the private key in a slot. Getting the policy wrong is not
// insecure but may result in the user being asked for their pin more than
// necessary, or not enough times and the signer will error instead of
// producing a signature, annoying but not insecure.
//
// If either of the policies cannot be determined, zero will be returned for
// that policy. If the extension is not found, both will eb returned as zero.
func PoliciesFromSlotCert(cert *x509.Certificate) (tp piv.TouchPolicy, pp piv.PINPolicy, ff piv.Formfactor) {
	for _, ext := range cert.Extensions {
		if ext.Id.Equal(ExtIDKeyPolicy) {
			if ptp, ppp, err := ParseExtKeyPolicy(ext.Value); err == nil {
				tp, pp = ptp, ppp
			}
		}
		if ext.Id.Equal(ExtIDFormFactor) {
			if pff, err := ParseExtFormFactor(ext.Value); err == nil {
				ff = pff
			}
		}
	}

	return
}
