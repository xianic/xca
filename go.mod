module gitlab.com/xianic/xca

go 1.21

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/pbnjay/memory v0.0.0-20210728143218-7b4eea64cf58
	github.com/spf13/cobra v1.5.0
	github.com/spf13/pflag v1.0.5
	github.com/xianic/fslock v1.0.2-0.20221020232355-29a558d2b6ae
	golang.org/x/crypto v0.1.0
	golang.org/x/term v0.1.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/go-piv/piv-go v1.11.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
