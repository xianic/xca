package keytype

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rsa"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/xianic/xca/cryptutil"
)

type Type string
type Class string

const (
	KeyUnknown = Type("")
	KeyP224    = Type("P-224")
	KeyP256    = Type("P-256")
	KeyP384    = Type("P-384")
	KeyP521    = Type("P-521")
	Key25519   = Type("25519")

	ClassUnknown = Class("")
	ClassEC      = Class("P")
	Class25519   = Class("25519")
	ClassRSA     = Class("RSA")
)

var (
	ErrUnknownType = errors.New("unknown keyType")
)

func (t Type) String() string {
	return string(t)
}

func (t Type) unknownErr() error {
	return fmt.Errorf("%q: %w", t, ErrUnknownType)
}

func KeyRSA(bits uint) Type {
	return Type(fmt.Sprintf("RSA%d", bits))
}

func (t Type) IsUnknown() bool {
	return t == KeyUnknown
}

func (t Type) IsRsa() bool {
	return strings.HasPrefix(t.String(), "RSA")
}

func (t Type) IsEcdsa() bool {
	return strings.HasPrefix(t.String(), "P-")
}

func (t Type) Is25519() bool {
	return t == Key25519
}

func (t Type) IsEc() bool {
	return t.IsEcdsa() || t.Is25519()
}

func (t Type) Class() Class {
	if t.IsEc() {
		return ClassEC
	}

	if t.IsEcdsa() {
		return ClassEC
	}

	if t.Is25519() {
		return Class25519
	}

	return ClassUnknown
}

func TypeOf(key any) Type {
	if pub := cryptutil.PublicKeyFor(key); pub != nil {
		key = pub
	}

	switch t := key.(type) {
	case *ecdsa.PublicKey:
		return Type(t.Params().Name)
	case ed25519.PublicKey:
		return Key25519
	case *rsa.PublicKey:
		return Type(fmt.Sprintf("RSA%d", t.Size()*8))
	default:
		return KeyUnknown
	}
}

func (t Type) New(rand io.Reader) (pub crypto.PublicKey, priv crypto.PrivateKey, err error) {
	defer func() {
		if err == nil && pub == nil && priv != nil {
			pub = cryptutil.PublicKeyFor(priv)
		}
	}()

	switch t {
	case KeyP224:
		priv, err = ecdsa.GenerateKey(elliptic.P224(), rand)
	case KeyP256:
		priv, err = ecdsa.GenerateKey(elliptic.P256(), rand)
	case KeyP384:
		priv, err = ecdsa.GenerateKey(elliptic.P384(), rand)
	case KeyP521:
		priv, err = ecdsa.GenerateKey(elliptic.P521(), rand)
	case Key25519:
		pub, priv, err = ed25519.GenerateKey(rand)
	}
	if err != nil || priv != nil {
		return
	}

	if t.IsRsa() {
		var bits uint64
		bits, err = strconv.ParseUint(t.String()[3:], 10, 32)
		if err != nil {
			err = fmt.Errorf("parsing RSA key size: %w", err)
			return
		}
		priv, err = rsa.GenerateKey(rand, int(bits))
		return
	}

	return nil, nil, t.unknownErr()
}
