package ca

import (
	"bytes"
	"crypto/x509"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/siglog"
)

/*
LogAuditor checks a CA log to ensure it is in the correct format. Things that
are checked:
  - Message sequence: The first message must be a roll message, and if there is
    a second, then it must be the last in the sequence.
  - A closing roll message must contain all the certificates that the opening
    roll message contains and additionally all the certificate issued since the
    opening roll message.
  - This auditor is shown more than one log stream, the opening roll message
    must match the closing roll message of the previous stream.
  - Issue messages must contain a certificate followed by a CSR. The certificate
    must have been issued by the CA, and the key in the CSR matches the key in
    the certificate.
*/
type LogAuditor struct {
	CaCert      *x509.Certificate
	CertsIssued []*CertTrace

	closingRoll *siglog.Message
}

func (a *LogAuditor) CheckMessage(seq int, m *siglog.Message) error {
	if seq == 0 && m.What != LogMessageTypeRoll {
		return errors.New("first message must be of type " + string(LogMessageTypeRoll))
	}
	if a.closingRoll != nil && seq != 0 {
		return errors.New("additional message after log was closed")
	}

	switch m.What {
	case LogMessageTypeRoll:
		if err := a.checkRoll(seq, m); err != nil {
			return fmt.Errorf("checking initial audit message: %w", err)
		}
	case LogMessageTypeIssue:
		return a.checkIssue(m)
	}

	return nil
}

func (a *LogAuditor) checkRoll(seq int, m *siglog.Message) (err error) {
	if seq == 0 {
		if a.closingRoll == nil {
			err = a.checkInitialLogOpen(m)
		} else {
			err = a.checkSubsequentLogOpen(m)
		}
	} else {
		err = a.checkLogClose(m)
	}
	return
}

func (a *LogAuditor) checkInitialLogOpen(m *siglog.Message) (err error) {
	a.CaCert, a.CertsIssued, err = ParseLogRollMessage(m.Payload)
	return
}

func (a *LogAuditor) checkSubsequentLogOpen(m *siglog.Message) error {
	if !bytes.Equal(a.closingRoll.Serial, m.Serial) ||
		a.closingRoll.When != m.When ||
		!bytes.Equal(a.closingRoll.Payload, m.Payload) {
		return errors.New("last message of old log and first message of new log differ")
	}

	a.closingRoll = nil
	return nil
}

func (a *LogAuditor) checkLogClose(m *siglog.Message) error {
	a.closingRoll = m

	cert, traces, err := ParseLogRollMessage(m.Payload)
	if err != nil {
		return err
	}

	if !bytes.Equal(cert.Raw, a.CaCert.Raw) {
		return errors.New("unexpected CA certificate change")
	}

	if len(a.CertsIssued) != len(traces) {
		return fmt.Errorf("expecting %d traces but found %d", len(a.CertsIssued), len(traces))
	}

	for i := 0; i < len(a.CertsIssued); i++ {
		if !a.CertsIssued[i].Equal(traces[i]) {
			return fmt.Errorf("traces don't match: %q and %q", a.CertsIssued[i], traces[i])
		}
	}

	return nil
}

func (a *LogAuditor) checkIssue(m *siglog.Message) error {
	ps := cryptutil.NewPemUnmarshaler(cryptutil.NewPemBuffer(m.Payload))

	cert, err := ps.ExpectCertificate()
	if err != nil {
		return fmt.Errorf("parsing issued certificate: %w", err)
	}

	csr, err := ps.ExpectCSR()
	if err != nil {
		return fmt.Errorf("parsing CSR for issued certificate: %w", err)
	}

	if err = cert.CheckSignatureFrom(a.CaCert); err != nil {
		return fmt.Errorf("parent signature failure: %w", err)
	}

	if err = csr.CheckSignature(); err != nil {
		return fmt.Errorf("CSR invalid: %w", err)
	}

	if !cryptutil.KeysMatch(csr.PublicKey, cert.PublicKey) {
		return errors.New("certificate and CSR keys do not match")
	}

	if a.CertsIssued == nil {
		a.CertsIssued = make([]*CertTrace, 0, 1)
	}
	a.CertsIssued = append(a.CertsIssued, MakeTrace(cert))

	return nil
}

func (a LogAuditor) InitCA(ca *CA) error {
	if a.CaCert == nil {
		return errors.New("no CA certificate found in audit log")
	}

	if !bytes.Equal(a.CaCert.Raw, ca.certificate.Raw) {
		return errors.New("CA certificate does not match the one found in the audit log")
	}

	ca.pastCerts = a.CertsIssued

	return nil
}

func ParseLogRollMessage(payload []byte) (cert *x509.Certificate, traces []*CertTrace, err error) {
	buffer := cryptutil.NewPemBuffer(payload)
	ps := cryptutil.NewPemUnmarshaler(buffer)

	cert, err = ps.ExpectCertificate()
	if err != nil {
		return nil, nil, fmt.Errorf("parsing CA cert: %w", err)
	}

	rest, err := buffer.RemainingBytes()
	if err != nil {
		return nil, nil, fmt.Errorf("expecting data after CA cert: %w", err)
	}

	traces, err = ParseTraces(rest)
	if err != nil {
		return nil, nil, fmt.Errorf("parsing certificate traces: %w", err)
	}

	return
}
