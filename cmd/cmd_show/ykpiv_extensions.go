//go:build ykpiv
// +build ykpiv

package cmd_show

import (
	"crypto/x509/pkix"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"gitlab.com/xianic/xca/yubikey"
)

func init() {
	AddExtensionPrinter(yubikey.ExtIDFirmwareVersion, func(ext pkix.Extension) {
		fmt.Printf("Yubikey firmware:\t")
		v, err := yubikey.ParseExtFirmwareVersion(ext.Value)
		if err != nil {
			fmt.Printf("version number malformed: %s\n", err)
		} else {
			fmt.Printf("v%d.%d.%d\n", v.Major, v.Minor, v.Patch)
		}
	})

	AddExtensionPrinter(yubikey.ExtIDSerialNumber, func(ext pkix.Extension) {
		fmt.Printf("Yubikey serial number:\t")
		s, err := yubikey.ParseExtSerialNumber(ext.Value)
		if err != nil {
			fmt.Printf("malformed: %s\n", err)
		} else {
			fmt.Printf("%d\n", s)
		}
	})

	AddExtensionPrinter(yubikey.ExtIDSlot, func(ext pkix.Extension) {
		fmt.Printf("Yubikey slot number:\t")
		s, err := yubikey.ParseExtSlotNumber(ext.Value)
		if err != nil {
			fmt.Printf("malformed: %s\n", err)
		} else {
			fmt.Printf("%x\n", s)
		}
	})

	AddExtensionPrinter(yubikey.ExtIDKeyPolicy, func(ext pkix.Extension) {
		tp, pp, err := yubikey.ParseExtKeyPolicy(ext.Value)
		if err != nil {
			fmt.Printf("Yubikey key ploicy:\t malformed: %s\n", err)
			return
		}

		fmt.Printf("Yubikey pin policy:\t")
		switch pp {
		case piv.PINPolicyNever:
			fmt.Printf("never\n")
		case piv.PINPolicyOnce:
			fmt.Printf("once\n")
		case piv.PINPolicyAlways:
			fmt.Printf("always\n")
		default:
			fmt.Printf("unknown\n")
		}

		fmt.Printf("Yubikey touch policy:\t")
		switch tp {
		case piv.TouchPolicyNever:
			fmt.Printf("never\n")
		case piv.TouchPolicyAlways:
			fmt.Printf("always\n")
		case piv.TouchPolicyCached:
			fmt.Printf("cached\n")
		default:
			fmt.Printf("unknown\n")
		}
	})

	AddExtensionPrinter(yubikey.ExtIDFormFactor, func(ext pkix.Extension) {
		fmt.Printf("Yubikey form facor:\t")
		f, err := yubikey.ParseExtFormFactor(ext.Value)
		if err != nil {
			fmt.Printf("malformed: %s\n", err)
		} else {
			fmt.Printf("%s\n", f.String())
		}
	})
}
