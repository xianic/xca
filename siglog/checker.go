package siglog

/*
MessageChecker is used to add additional checks to the verification of log messages.
*/
type MessageChecker interface {
	CheckMessage(sequenceNumber int, message *Message) error
}

type MultiMessageChecker []MessageChecker

func NewMultiMessageChecker(checkers ...MessageChecker) MessageChecker {
	mmc := make(MultiMessageChecker, len(checkers))
	for i, c := range checkers {
		mmc[i] = c
	}

	return mmc
}

func (m MultiMessageChecker) CheckMessage(sequenceNumber int, message *Message) error {
	for _, c := range m {
		if c == nil {
			continue
		}
		if err := c.CheckMessage(sequenceNumber, message); err != nil {
			return err
		}
	}

	return nil
}
