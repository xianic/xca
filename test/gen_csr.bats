#!/usr/bin/env bats

load helpers.bash

function setup_file() {
    setup_keys
}

function teardown_file() {
    teardown_keys
}

@test "csr from each key type" {
    for kt in $KEYTYPES
    do
        CSR="$($XCA_TOOL gen csr --subject "Hello World" "$BATS_FILE_TMPDIR/key_$kt.pem" -)"
        show_compare test/data/show_gen_csr_subject_simple_$kt.txt <<<"$CSR"
    done
}
