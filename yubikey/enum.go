package yubikey

import (
	"fmt"
	"sort"
	"strings"

	"github.com/go-piv/piv-go/piv"
)

func parse[T any](s, name string, m map[string]T) (T, error) {
	t, found := m[s]

	if found {
		return t, nil
	}

	return t, fmt.Errorf(
		"%q is not an acceptable %s, try one of [%s]",
		s,
		name,
		strings.Join(names(m), ", "),
	)

}

func toString[T comparable](t T, m map[string]T) string {
	for k, v := range m {
		if t == v {
			return k
		}
	}

	return "unknown"
}

func names[T any](m map[string]T) []string {
	a := make([]string, 0, len(m))
	for k, _ := range m {
		a = append(a, k)
	}
	sort.Strings(a)
	return a
}

func values[T any](m map[string]T) []T {
	l := make([]T, len(m))
	for i, s := range names(m) {
		l[i] = m[s]
	}
	return l
}

var slots map[string]piv.Slot = map[string]piv.Slot{
	"9a": piv.SlotAuthentication,
	"9c": piv.SlotSignature,
	"9e": piv.SlotCardAuthentication,
	"9d": piv.SlotKeyManagement,
}

func SlotParse(s string) (piv.Slot, error) {
	return parse(s, "slot name", slots)
}

func SlotName(s piv.Slot) string {
	return toString(s, slots)
}

func SlotNames() []string {
	return names(slots)
}

func Slots() []piv.Slot {
	return values(slots)
}

func SlotDescription(s string) string {
	switch s {
	case "9a":
		return "authentication"
	case "9c":
		return "signature"
	case "9e":
		return "card authentication"
	case "9d":
		return "key management"
	default:
		return "unknown"
	}
}

func SlotDescriptions() string {
	keys := make([]string, 0, len(slots))
	for k, _ := range slots {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	list := ""
	for _, slot := range keys {
		if list != "" {
			list += ", "
		}
		list += fmt.Sprintf("%s (%s)", slot, SlotDescription(slot))
	}
	return list
}

var algos map[string]piv.Algorithm = map[string]piv.Algorithm{
	"P-256":   piv.AlgorithmEC256,
	"P-384":   piv.AlgorithmEC384,
	"25519":   piv.AlgorithmEd25519,
	"RSA1024": piv.AlgorithmRSA1024,
	"RSA2048": piv.AlgorithmRSA2048,
}

func AlgoParse(s string) (piv.Algorithm, error) {
	return parse(s, "key type", algos)
}

func AlgoName(a piv.Algorithm) string {
	return toString(a, algos)
}

func AlgoNames() []string {
	return names(algos)
}

var pinPolicies map[string]piv.PINPolicy = map[string]piv.PINPolicy{
	"never":  piv.PINPolicyNever,
	"once":   piv.PINPolicyOnce,
	"always": piv.PINPolicyAlways,
}

func PinPolicyParse(s string) (piv.PINPolicy, error) {
	return parse(s, "pin policy", pinPolicies)
}

func PinPolicyName(pp piv.PINPolicy) string {
	return toString(pp, pinPolicies)
}

func PinPolicyNames() []string {
	return names(pinPolicies)
}

var touchPolicies map[string]piv.TouchPolicy = map[string]piv.TouchPolicy{
	"never":  piv.TouchPolicyNever,
	"once":   piv.TouchPolicyAlways,
	"cached": piv.TouchPolicyCached,
}

func TouchPolicyParse(s string) (piv.TouchPolicy, error) {
	return parse(s, "touch policy", touchPolicies)
}

func TouchPolicyName(tp piv.TouchPolicy) string {
	return toString(tp, touchPolicies)
}

func TouchPolicyNames() []string {
	return names(touchPolicies)
}
