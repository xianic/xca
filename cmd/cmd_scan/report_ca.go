package cmd_scan

import (
	"fmt"

	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/scanner/pkimodel"
)

func init() {
	addReport("ca", reportCAs)
	addReport("orphan-ca", reportOrphanCAs)
	addReport("selfsigned", reportSelfSigned)
}

func reportCAs(pc *pkimodel.Collection) error {
	certs := selectAndSortCerts(pc.Certificates, func(c *pkimodel.Certificate) bool {
		return c.Parsed().IsCA && c.Issuer() == nil && len(c.Issued()) != 0
	}, sortById)

	for _, cert := range certs {
		reportCAsStep("", cert)
		fmt.Println()
	}

	return nil
}

func reportCAsStep(prefix string, c *pkimodel.Certificate) {
	fmt.Printf("%s%s\n", prefix, c.String())
	prefix += "    "
	for _, issued := range c.Issued() {
		reportCAsStep(prefix, issued)
	}
}

func reportOrphanCAs(pc *pkimodel.Collection) error {
	certs := selectAndSortCerts(pc.Certificates, func(c *pkimodel.Certificate) bool {
		return c.Parsed().IsCA && c.Issuer() == nil && len(c.Issued()) == 0
	}, sortById)

	for _, cert := range certs {
		fmt.Println(cert.String())
	}

	return nil
}

func reportSelfSigned(pc *pkimodel.Collection) error {
	certs := selectAndSortCerts(pc.Certificates, func(c *pkimodel.Certificate) bool {
		return cryptutil.CertificateIsSelfSigned(c.Parsed()) && !c.Parsed().IsCA
	}, sortById)

	for _, cert := range certs {
		fmt.Println(cert.String())
	}

	return nil
}
