package csr

import (
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/gen"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
	"os"
)

var (
	Cmd = &cobra.Command{
		Use:   "csr [options] [output file|-}",
		Short: "generate a CSR using the private key stored on a Yubikey",
		RunE:  csrCmd,
	}

	selectionFlags ykpiv.SelectionFlags
	pinFlags       ykpiv.PinFlag
	csrFlags       cmd.CertFlags
)

func init() {
	gen.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	selectionFlags.Init(fs)
	pinFlags.Init(fs, ".")
	csrFlags.Init(fs, cmd.CertFlagsCsr)
}

func csrCmd(_ *cobra.Command, args []string) (err error) {
	yk, slot, err := selectionFlags.GetYubikey()
	if err != nil {
		return err
	}
	defer util.DeferClose(yk.Close, &err)

	pubKey, err := getPubKey(yk, slot)
	if err != nil {
		return fmt.Errorf("error getting public key from Yubikey: %w", err)
	}

	pin, err := pinFlags.PivPrompt()
	if err != nil {
		return err
	}

	signer, err := yk.PrivateKey(slot, pubKey, piv.KeyAuth{
		PIN:       pin,
		PINPolicy: piv.PINPolicyAlways,
	})

	template := &x509.CertificateRequest{
		SignatureAlgorithm: cryptutil.SignatureAlgorithmForKey(pubKey, cryptutil.DefaultSignatureHash),
	}
	if err = csrFlags.PopulateCsr(template); err != nil {
		return err
	}

	csrDer, err := x509.CreateCertificateRequest(rand.Reader, template, signer)
	if err != nil {
		return fmt.Errorf("creating CSR: %w", err)
	}

	err = pem.Encode(os.Stdout, &pem.Block{
		Type:  cryptutil.PemTypeCsr,
		Bytes: csrDer,
	})

	return err
}

func getPubKey(yk *piv.YubiKey, slot piv.Slot) (crypto.PublicKey, error) {
	att, _ := yk.Attest(slot)
	if att != nil {
		return att.PublicKey, nil
	}

	cert, err := yk.Certificate(slot)
	if err != nil {
		if errors.Is(err, piv.ErrNotFound) {
			return nil, fmt.Errorf("could not find public key in either an attestation or a certificate in slot %02x, generate or import a key first", slot.Key)
		}
		return nil, err
	}

	return cert.PublicKey, nil
}
