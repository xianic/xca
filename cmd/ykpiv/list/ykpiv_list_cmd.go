package list

import (
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/keytype"
	"gitlab.com/xianic/xca/yubikey"
)

var (
	Cmd = &cobra.Command{
		Use:   "list",
		Short: "list PIV devices",
		RunE:  listCmd,
	}
)

func init() {
	ykpiv.Cmd.AddCommand(Cmd)
}

func listCmd(_ *cobra.Command, args []string) error {
	cards, err := piv.Cards()
	if err != nil {
		return err
	}

	foundUnsupported := false
	for _, cardname := range cards {
		if !yubikey.CardSupported(cardname) {
			fmt.Printf("%s*\n", cardname)
			foundUnsupported = true
			continue
		}

		info, err := yubikey.GetCardInfo(cardname)
		if err != nil {
			fmt.Printf("\terror: %s\n", err)
			continue
		}

		fmt.Printf(
			"%s, serial: %d, version: %d.%d.%d\n",
			cardname,
			info.Serial,
			info.Version.Major,
			info.Version.Minor,
			info.Version.Patch,
		)

		for _, slot := range yubikey.SlotNames() {
			slotInfo := info.Slots[slot]
			if slotInfo == nil {
				continue
			}
			fmt.Printf("\tSlot %s -", slot)

			if slotInfo.Attestation == nil {
				fmt.Printf(" key imported or not set.")
			} else {
				att, err := piv.Verify(info.AttestationCert, slotInfo.Attestation)
				if err != nil {
					fmt.Printf(" attestation error: %s", err)
				} else {
					fmt.Printf(
						" %s key, pin policy: %s, touch policy: %s.",
						keytype.TypeOf(slotInfo.Attestation.PublicKey),
						yubikey.PinPolicyName(att.PINPolicy),
						yubikey.TouchPolicyName(att.TouchPolicy),
					)
				}
			}

			if slotInfo.Certificate != nil {
				fmt.Printf(" Certificate for %s\n", slotInfo.Certificate.Subject.String())
			} else {
				fmt.Printf(" No certificate\n")
			}
		}
	}

	if foundUnsupported {
		fmt.Printf("* unsupported cards")
	}

	return nil
}
