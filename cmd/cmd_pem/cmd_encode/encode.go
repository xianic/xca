package cmd_encode

import (
	"bytes"
	"encoding/asn1"
	"encoding/pem"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/cmd_pem"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/util"
)

const autoType = "auto"

var (
	oidCurveEd25519  = asn1.ObjectIdentifier{1, 3, 101, 112}
	oidRsaEncryption = asn1.ObjectIdentifier{1, 2, 840, 113549, 1, 1, 1}
	oidEcPublicKey   = asn1.ObjectIdentifier{1, 2, 840, 10045, 2, 1}
)

var (
	Cmd = &cobra.Command{
		Use:   "encode [flags] [file [file […]]]",
		Short: "Make a PEM block",
		Long: `Make a PEM block from DER encoded objects or raw data

Reads some data and makes it into a pem block. Unless --raw is used then the
input data is assumed to be DER encoded and an attempt is made to figure out
what kind of cryptographic object the data is and create the right headers
for it. Multiple ASN.1 objects can be concatenated and passed as input and
multiple PEM blocks will be emitted (DER is a form of ASN.1).

All input files will be read and concatenated.

--type can be used to override the type PEM block emitted. Specify once for
each object passed in. If the type is 'auto' xca will take a guess as to what
the correct type is.

If --raw is used then no interpretation of the input data is made and --type
must be specified exactly once. One PEM block will be emitted containing all
of the input data.`,
		Args: util.StdInOnce,
		RunE: encodeCmdFunc,
	}

	multiple   bool
	raw        bool
	blockTypes []string
	outputPath string
	outputFile io.WriteCloser
)

func init() {
	cmd_pem.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	fs.BoolVarP(&multiple, "multiple", "m", false, "allow more than one block per file")
	fs.BoolVar(&raw, "raw", false, "don't try to guess the type of input data, requires --type other than auto, incompatible with --multiple")
	fs.StringArrayVarP(&blockTypes, "type", "t", []string{autoType}, "type of PEM block. Use 'auto' to guess from input data. Repeat for multiple PEM blocks, the last will be used for remaining blocks.")
	fs.StringVarP(&outputPath, "output", "o", "-", "output file, use '-' for standard output")
}

func encodeCmdFunc(_ *cobra.Command, args []string) (err error) {
	outputFile = util.DelayOpenWrite(outputPath, false, util.AllowStdout)
	defer util.DeferClose(outputFile.Close, &err)

	if len(args) == 0 {
		err = encodeFile("-")
	} else {
		for _, arg := range args {
			err = encodeFile(arg)
			if err != nil {
				return
			}
		}
	}

	return nil
}

func encodeFile(arg string) (err error) {
	if arg == "-" {
		return encodeStream("stdin", os.Stdin)
	}

	var file *os.File
	file, err = os.Open(arg)
	if err != nil {
		return
	}
	defer util.DeferClose(file.Close, &err)

	return encodeStream(arg, file)
}

func encodeStream(name string, in io.Reader) error {
	//TODO: use a streaming parser so that the whole PEM block does not need to reside in memory at once
	data, err := io.ReadAll(in)
	if err != nil {
		return err
	}

	offset := 0
	for len(data) > 0 {
		if raw {
			err = writePem(data, "UNKNOWN")
			if err != nil {
				return err
			}
			return nil
		}

		blockSize, blockType := findBlockAndType(data)
		if blockSize == 0 {
			return fmt.Errorf(
				"%s: offset %d: could not determin type of data, use --type and maybe --raw",
				name,
				offset,
			)
		}

		err = writePem(data[0:blockSize], blockType)
		if err != nil {
			return err
		}

		if !multiple {
			return nil
		}

		offset += blockSize
		data = data[blockSize:]
	}

	return nil
}

/*
findBlockAndType returns the length of the block DER encoded block if found
and the appropriate PEM type. If the type could not be determined blockSize
is 0 and the blockType is a string intended to help with debugging and should
not be shown to the program user.
*/
func findBlockAndType(data []byte) (blockSize int, blockType string) {
	section := new(asn1.RawValue)
	if _, err := asn1.Unmarshal(data, section); err != nil {
		return 0, fmt.Sprintf("not asn.1: %s", err)
	}

	var found bool
	found, blockType = findTypeFromAsn1(section)

	if found {
		blockSize = len(section.FullBytes)
	} else {
		blockSize = 0
	}

	return
}

func findTypeFromAsn1(outer *asn1.RawValue) (found bool, blockType string) {
	// all know objects open with a SEQUENCE
	if outer.Tag != 16 || outer.Class != 0 || outer.IsCompound != true {
		return false, "doesn't start with a ASN.1 sequence"
	}

	var nextVal asn1.RawValue
	_, err := asn1.Unmarshal(outer.Bytes, &nextVal)
	if err != nil {
		return false, err.Error()
	}

	if nextVal.Class != 0 {
		return false, fmt.Sprintf("unnexpected class %d in tag #2", nextVal.Class)
	}

	if nextVal.Tag == 2 && nextVal.IsCompound == false {
		// tag is an integer. Private keys have this, other objects do not
		return findTypeIntStart(nextVal.Bytes, outer.Bytes[len(nextVal.FullBytes):])
	} else if nextVal.Tag == 16 && nextVal.IsCompound == true {
		// another sequence.
		return findTypeSeqStart(nextVal.Bytes)
	}

	return false, fmt.Sprintf("unknown second tag: tag=%d class=%d isCompound=%t", nextVal.Tag, nextVal.Class, nextVal.IsCompound)
}

func findTypeIntStart(intVal, remainder []byte) (found bool, blockType string) {
	if len(intVal) != 1 || intVal[0] != 0 {
		return false, "int ID not zero"
	}

	// we are now looking for a sequence of OIDs but are only interested in the
	// first one. For some reason it won't parse into a []asn1.ObjectIdentifier
	// so we parse a raw tag, check that it's a sequence, and then try the
	// code that looks for an OID.
	var next asn1.RawValue
	_, err := asn1.Unmarshal(remainder, &next)
	if err != nil {
		return false, "looking for OID sequence: " + err.Error()
	}

	if next.Tag != 16 || next.Class != 0 || next.IsCompound != true {
		return false, "not a sequence of OIDs"
	}

	// The OIDs used in public keys are the same as are used in private keys
	// so if the next bit looks like the beginning of a public key then we
	// have found a private key
	found, blockType = findTypeSeqOidStart(next.Bytes)

	if found {
		if blockType == cryptutil.PemTypePublicKey {
			blockType = cryptutil.PemTypeECPrivateKey
		} else {
			found = false
		}
	}

	return
}

func findTypeSeqStart(seqContents []byte) (found bool, blockType string) {
	var next asn1.RawValue
	_, err := asn1.Unmarshal(seqContents, &next)
	if err != nil {
		return false, "2nd sequence parsing: " + err.Error()
	}

	if next.Tag == 6 && next.Class == 0 && next.IsCompound == false {
		// It is probably a public key
		return findTypeSeqOidStart(seqContents)
	}

	if next.Tag == 2 && next.Class == 0 && next.IsCompound == false &&
		bytes.Compare(next.Bytes, []byte{0}) == 0 {
		// It's a CSR
		return true, cryptutil.PemTypeCsr
	}

	if next.Tag == 0 && next.Class == 2 && next.IsCompound == true &&
		bytes.Compare(next.Bytes, []byte{2, 1, 2}) == 0 {
		// It's a certificate
		return true, cryptutil.PemTypeCertificate
	}

	return false, fmt.Sprintf("2nd sequene: tag=%d class=%d isCompound=%t, len=%d", next.Tag, next.Class, next.IsCompound, len(next.Bytes))
}

func findTypeSeqOidStart(seqContents []byte) (found bool, blockType string) {
	// we are interested in the first OID in a possible sequence
	var next asn1.ObjectIdentifier
	_, err := asn1.Unmarshal(seqContents, &next)
	if err != nil {
		return false, "looking for OID: " + err.Error()
	}

	if oidCurveEd25519.Equal(next) || oidRsaEncryption.Equal(next) || oidEcPublicKey.Equal(next) {
		return true, cryptutil.PemTypePublicKey
	}

	return false, "unknown OID " + next.String()
}

func writePem(data []byte, guessedType string) error {
	block := &pem.Block{Bytes: data}

	if len(blockTypes) > 0 {
		if blockTypes[0] == autoType {
			block.Type = guessedType
		} else {
			block.Type = blockTypes[0]
		}
	}

	if len(blockTypes) > 1 {
		blockTypes = blockTypes[1:]
	}

	return pem.Encode(outputFile, block)
}
