package yubikey

import (
	"bytes"
	"crypto"
	"crypto/x509"
	"crypto/x509/pkix"
	"errors"
	"fmt"

	"github.com/go-piv/piv-go/piv"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cryptutil"
	"gitlab.com/xianic/xca/prompt"
	"gitlab.com/xianic/xca/yubikey"
)

const DriverName = "ykpiv"

type Config struct {
	Serial uint32 `yaml:"serial"`
	Slot   uint32 `yaml:"slot"`
}

type ykCaFactory struct {
}

type ykCaDriver struct {
	caConfig     *ca.Config
	driverConfig *Config

	yk *piv.YubiKey
	mk *[24]byte
}

func init() {
	ca.AddDriver(DriverName, new(ykCaFactory))
}

func (_ *ykCaFactory) String() string {
	return "store CA private key in the PIV applet on a Yubikey "
}

func (_ *ykCaFactory) NewDriverConfig() any {
	return new(Config)
}

func (c *Config) PivSlot() (piv.Slot, error) {
	slot, found := piv.RetiredKeyManagementSlot(c.Slot)
	if found {
		return slot, nil
	}

	for _, s := range yubikey.Slots() {
		if s.Key == c.Slot {
			return s, nil
		}
	}

	return piv.Slot{}, fmt.Errorf("unknown piv slot 0x%x", c.Slot)
}

func (f *ykCaFactory) NewDriver(caConfig *ca.Config, driverConfig any) (ca.Driver, error) {
	return &ykCaDriver{
		caConfig:     caConfig,
		driverConfig: driverConfig.(*Config),
	}, nil
}

func (d *ykCaDriver) promptShim(p prompt.PasswordPrompt) func() (string, error) {
	return func() (string, error) {
		pin, err := p.Prompt(fmt.Sprintf(
			"PIN for Yubikey %d slot %x",
			d.driverConfig.Serial,
			d.driverConfig.Slot,
		), false)
		pinStr := string(pin)

		if d.mk == nil && err == nil {
			var mt *piv.Metadata
			mt, err = d.yk.Metadata(pinStr)
			if err != nil {
				err = fmt.Errorf("unlocking Yubikey with pin: %w", err)
			} else if mt.ManagementKey != nil {
				d.mk = mt.ManagementKey
			} else {
				d.mk = &piv.DefaultManagementKey
			}
		}

		return string(pin), err
	}
}

func (d *ykCaDriver) openYubiKey() (slot piv.Slot, err error) {
	if d.yk == nil {
		d.yk, err = yubikey.GetCardBySerial(d.driverConfig.Serial)
		if err != nil {
			return
		}
	}

	slot, err = d.driverConfig.PivSlot()
	return
}

func (d *ykCaDriver) findPubKey(slot piv.Slot) (crypto.PublicKey, piv.PINPolicy, ca.CertCustomiser, error) {
	version := d.yk.Version()
	pinPolicy := piv.PINPolicyAlways
	touchPolicy := piv.TouchPolicyCached
	var pubKey crypto.PublicKey
	var err error
	var serial uint32
	var ff piv.Formfactor

	serial, err = d.yk.Serial()

	if err == nil {
		var att *x509.Certificate
		att, err = d.yk.Attest(slot)
		if err == nil && att != nil {
			pubKey = att.PublicKey
			touchPolicy, pinPolicy, ff = yubikey.PoliciesFromSlotCert(att)
		}
		if errors.Is(err, piv.ErrNotFound) {
			err = nil
		}
	}

	if err == nil && pubKey == nil {
		var cert *x509.Certificate
		cert, err = d.yk.Certificate(slot)
		if err == nil && cert != nil {
			pubKey = cert.PublicKey
			touchPolicy, pinPolicy, ff = yubikey.PoliciesFromSlotCert(cert)
		}
	}

	customiser := func(cert *x509.Certificate) error {
		if cert.ExtraExtensions == nil {
			cert.ExtraExtensions = make([]pkix.Extension, 0, 1)
		}
		cert.ExtraExtensions = append(
			cert.ExtraExtensions,
			yubikey.GenExtSerialNumber(serial),
			yubikey.GenExtSlotNumber(slot.Key),
			yubikey.GenExtFirmwareVersion(version),
			yubikey.GenExtKeyPolicy(touchPolicy, pinPolicy),
		)
		if ff != 0 {
			cert.ExtraExtensions = append(
				cert.ExtraExtensions,
				yubikey.GenExtFormFactor(ff),
			)
		}
		return nil
	}

	return pubKey, pinPolicy, customiser, nil
}

func (d *ykCaDriver) InitPrivateKey(p prompt.PasswordPrompt) (pubKey crypto.PublicKey, certCustomiser ca.CertCustomiser, signer crypto.Signer, err error) {
	slot, err := d.openYubiKey()
	if err != nil {
		return nil, nil, nil, err
	}

	var pp piv.PINPolicy
	pubKey, pp, certCustomiser, err = d.findPubKey(slot)
	if err != nil && !errors.Is(err, piv.ErrNotFound) {
		return nil, nil, nil, err
	}

	var pin string
	if err != nil || pubKey == nil {
		// err is a piv.ErrNotFound indicating no public key was found,
		// make a new private key, record the public
		// part in the cert slot.
		pin, err = d.promptShim(p)()
		if err != nil {
			return nil, nil, nil, err
		}

		pp = piv.PINPolicyOnce
		pubKey, err = d.yk.GenerateKey(*d.mk, slot, piv.Key{
			Algorithm:   piv.AlgorithmEC256,
			PINPolicy:   pp,
			TouchPolicy: piv.TouchPolicyCached,
		})
		if err != nil {
			return nil, nil, nil, fmt.Errorf(
				"generating a new P-256 key on the Yubikey: %w",
				err,
			)
		}
	}

	if pp != piv.PINPolicyOnce {
		// We have queried the pin and can allow it to be reused for signing
		// operations. Here we respect the pin policy by not reusing it if
		// the policy is not "once": it is either "never" (pin is not needed
		// anyway), or "always" (in which case we would be defeating the
		// policy if we kept it).
		pin = ""
	}

	privKey, err := d.yk.PrivateKey(slot, pubKey, piv.KeyAuth{
		PIN:       pin, // If the pin was queried, use it
		PINPrompt: d.promptShim(p),
		PINPolicy: pp,
	})
	if err != nil {
		return nil, nil, nil, fmt.Errorf(
			"getting access to the private key in the Yubikey: %w",
			err,
		)
	}

	var ok bool
	if signer, ok = privKey.(crypto.Signer); !ok {
		return nil, nil, nil, fmt.Errorf(
			"%T returned by Yubikey cannot sign",
			privKey,
		)
	}

	return
}

func (d *ykCaDriver) GetSigner(certificate *x509.Certificate, p prompt.PasswordPrompt) (signer crypto.Signer, err error) {
	slot, err := d.openYubiKey()
	if err != nil {
		return nil, err
	}

	pubKey, pp, _, err := d.findPubKey(slot)
	if err != nil {
		return nil, err
	}
	if pubKey != nil {
		return nil, piv.ErrNotFound
	}

	if !bytes.Equal(cryptutil.PublicKeyFingerprint(pubKey), cryptutil.PublicKeyFingerprint(certificate)) {
		return nil, fmt.Errorf("CA key differes from Yubikey")
	}

	if pp == 0 {
		_, pp, _ = yubikey.PoliciesFromSlotCert(certificate)
	}

	if pp == 0 {
		pp = piv.PINPolicyAlways
	}

	auth := piv.KeyAuth{
		PINPrompt: d.promptShim(p),
		PINPolicy: pp,
	}

	pk, err := d.yk.PrivateKey(slot, certificate.PublicKey, auth)
	if err != nil {
		return nil, err
	}

	return pk.(crypto.Signer), nil
}

func (d *ykCaDriver) SetCertificate(certificate *x509.Certificate, p prompt.PasswordPrompt) error {
	slot, err := d.openYubiKey()
	if err != nil {
		return err
	}

	if d.mk == nil {
		pin, err := d.promptShim(p)()
		if err != nil {
			return err
		}

		mt, err := d.yk.Metadata(pin)
		if err != nil {
			return fmt.Errorf("unlocking Yubikey with pin: %w", err)
		}
		d.mk = mt.ManagementKey
	}

	err = d.yk.SetCertificate(*d.mk, slot, certificate)
	if err != nil {
		return fmt.Errorf("writing CA certificate to Yubikey: %w", err)
	}

	return nil
}

func (d *ykCaDriver) Close() error {
	if d.yk == nil {
		return nil
	}

	defer func() {
		d.yk = nil
	}()

	return d.yk.Close()
}
