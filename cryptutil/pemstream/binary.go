package pemstream

import "bytes"

// isWhiteSpace returns true if b is one of the white space characters
// permitted in PEM encoded blocks
func isWhiteSpace(b byte) bool {
	return b == '\t' || b == ' '
}

// isBase64 returns true if b is a character used in standard base64 encoding.
// It does not return true for padding characters.
func isBase64(b byte) bool {
	return (b >= 'A' && b <= 'Z') ||
		(b >= 'a' && b <= 'z') ||
		(b >= '0' && b <= '9') ||
		b == '+' || b == '/'
}

// The functions bufExpand() bufSpareCap() bufGrowLen() and bufConsume() are related. They
// are intended to be used to move data between buffers e.g.
//
//   bufExpand(&dst, len(src), 512)
//   n := copy(bufSpareCap(dst), src)
//   bufGrowLen(&dst, n)
//   bufConsume(&src, n)

// bufSpareCap returns a slice of b representing the spare capacity in b.
// if data is written to the beginning of this slice then those bytes can be
// included in b by calling bufGrowLen(&b, n)
func bufSpareCap(b []byte) []byte {
	return b[len(b):cap(b)]
}

// bufGrowLen reslices b to increase its length by additional. It does not
// allocate more space in b so it the new length would exceed its capacity
// then it will panic.
func bufGrowLen(b *[]byte, additional int) {
	if additional > 0 {
		*b = (*b)[:len(*b)+additional]
	}
}

// bufConsume reslices b to remove some bytes from the beginning.
func bufConsume(b *[]byte, remove int) {
	*b = (*b)[remove:]
}

// bufLengths returns the sum of the length of all given buffers
func bufLengths(sequences ...[]byte) (count int) {
	for _, s := range sequences {
		count += len(s)
	}

	return
}

// bufExpand increases the capacity of b until cap(*b) - len(*b) > size.
// The buffer's capacity will be baseSize * 2 ^ n where n is incremented until
// a suitable capacity is found. The returned slice is the remaining capacity
// in *b, see bufSpareCap(). The length of the buffer will remain unchanged
// if reallocated, or 0 if allocated afresh.
func bufExpand(b *[]byte, size, baseSize int) []byte {
	if *b != nil {
		if cap(*b)-len(*b) >= size {
			return bufSpareCap(*b)
		}
		size += len(*b)
	}

	alloc := baseSize
	for alloc < size {
		alloc <<= 1
	}

	if *b == nil {
		*b = make([]byte, 0, alloc)
	} else if cap(*b) < alloc {
		t := make([]byte, len(*b), alloc)
		copy(t, *b)
		*b = t
	}

	return bufSpareCap(*b)
}

// bufDivergesAt returns the offset of the first byte that differs between
// the buffers.
func bufDivergesAt(a, b []byte) int {
	to := min(len(a), len(b))

	for i := 0; i < to; i++ {
		if a[i] != b[i] {
			return i
		}
	}

	return to
}

// findCr returns the offset of the first line ending in have, and the number
// of bytes the line ending consists of. A length of 1 indicates a unix line
// ending ("\n") and a length of 2 indicates a windows line ending ("\r\n")
func findCr(have []byte) (offset, length int) {
	offset = bytes.IndexByte(have, '\n')
	switch offset {
	case -1:

	case 0:
		length = 1

	default:
		if have[offset-1] == '\r' {
			offset--
			length = 2
		} else {
			length = 1
		}
	}

	return
}

// cutMatch returns two slices of b. It finds the offset i within b of
// the first byte where test(b[i]) returns false and
// returns b[0:i], b[i:]
func cutMatch(b []byte, test func(b byte) bool) (token, remaining []byte) {
	return cutAt(b, findMatchLength(b, test))
}

// cutPrefix finds how much of prefix is available at the start of b and
// returns p without that many bytes and the length of the prefix found.
func cutPrefix(b, prefix []byte) (truncated []byte, matchLength int) {
	for i := 0; i < len(b) && i < len(prefix); i++ {
		if b[i] != prefix[i] {
			return nil, 0
		}
	}

	lenDiff := len(b) - len(prefix)
	if lenDiff < 0 {
		return nil, lenDiff
	}

	wantLen := len(prefix)
	return b[wantLen:], wantLen
}

// cutAt returns the two slices b[:offset], b[offset:]
func cutAt(b []byte, offset int) (token, remaining []byte) {
	return b[:offset], b[offset:]
}

// findMatchLength returns the offset i in p of the first byte where test(buf[i])
// returns false. If all bytes satisfy test() then len(buf) is returned.
func findMatchLength(buf []byte, test func(b byte) bool) int {
	for i := 0; i < len(buf); i++ {
		if !test(buf[i]) {
			return i
		}
	}

	return len(buf)
}
