package pkimodel

import (
	"fmt"
	"strings"

	"gitlab.com/xianic/xca/cryptutil"
)

type chainBuilder struct {
	subjectCache map[string][]*Certificate
	keys         map[string][]*PrivateKey
	erroredCerts map[*Certificate]struct{}
}

func (c *Collection) BuildChains(errorStream chan<- error) error {
	cb := &chainBuilder{
		subjectCache: make(map[string][]*Certificate),
		keys:         make(map[string][]*PrivateKey),
		erroredCerts: make(map[*Certificate]struct{}),
	}

	for _, key := range c.PrivateKeys {
		key.usedBy = make([]*Certificate, 0)
		fp := key.FingerprintHex()
		fpKeys := cb.keys[fp]
		if fpKeys == nil {
			fpKeys = make([]*PrivateKey, 0, 1)
		}
		fpKeys = append(fpKeys, key)
		cb.keys[fp] = fpKeys
	}

	for _, cert := range c.Certificates {
		cert.issuer = nil
		cert.issued = make([]*Certificate, 0)

		subject := cert.cert.Subject.String()
		if list, found := cb.subjectCache[subject]; found {
			cb.subjectCache[subject] = append(list, cert)
		} else {
			cb.subjectCache[subject] = []*Certificate{cert}
		}
	}

	for _, cert := range c.Certificates {
		err := cb.buildChain(cert)
		if err != nil {
			if errorStream != nil {
				errorStream <- err
			} else {
				return err
			}
		}
	}

	return nil
}

func (cb *chainBuilder) buildChain(cert *Certificate) error {
	keyFp := fmt.Sprintf("%X", cryptutil.PublicKeyFingerprint(cert.Parsed().PublicKey))
	if matchingKeys, found := cb.keys[keyFp]; found {
		if len(matchingKeys) == 1 {
			cert.key = matchingKeys[0]
			matchingKeys[0].usedBy = append(matchingKeys[0].usedBy, cert)
		} else if len(matchingKeys) > 1 {
			winner := tieBreak(cert, matchingKeys, cert.File().File().TieBreakPrivateKeys)
			if winner != nil {
				cert.key = winner
			} else {
				strMatches := make([]string, len(matchingKeys))
				for i, m := range matchingKeys {
					strMatches[i] = m.String()
				}
				return fmt.Errorf(
					"%s cannot diffrentiate between keys: %s",
					cert,
					strings.Join(strMatches, "; "),
				)
			}
		}
	}

	if cryptutil.CertificateIsSelfSigned(cert.cert) {
		return nil
	}

	if _, found := cb.erroredCerts[cert]; found {
		return nil
	}

	potential := cb.subjectCache[cert.cert.Issuer.String()]
	if len(potential) == 0 {

		return nil
	}

	matches := make([]*Certificate, 0, len(potential))
	for _, p := range potential {
		err := cert.cert.CheckSignatureFrom(p.cert)
		if err == nil {
			matches = append(matches, p)
		}
	}

	if len(matches) > 1 {
		winner := tieBreak(cert, potential, cert.File().File().TieBreakCertificates)
		if winner != nil {
			matches = []*Certificate{winner}
		}
	}

	switch len(matches) {
	case 0:
		return nil

	case 1:
		matches[0].issued = append(matches[0].issued, cert)
		cert.issuer = matches[0]

	default:
		cb.erroredCerts[cert] = struct{}{}
		strMatches := make([]string, len(matches))
		for i, m := range matches {
			strMatches[i] = m.String()
		}
		return fmt.Errorf(
			"%s cannot diffrentiate between issuers: %s",
			cert,
			strings.Join(strMatches, "; "),
		)
	}

	return nil
}

func tieBreak[T IsInFile](target *Certificate, potential []T, tieBreak func(*Certificate, []T) T) T {
	certsInSameFile := make([]T, 0, len(potential))
	for _, p := range potential {
		if p.File().File() == target.File().File() {
			certsInSameFile = append(certsInSameFile, p)
		}
	}

	switch len(certsInSameFile) {
	case 0:
		// continue on to next tie-break
		break

	case 1:
		return certsInSameFile[0]

	default:
		return tieBreak(target, certsInSameFile)
	}

	targetFileName := target.File().File().String()
	longestMatchLength := 0
	longestMatchOffset := -1
	numberOfMatches := 0
	for i, p := range potential {
		pFileName := p.File().String()
		matchLength := 0
		for matchLength < len(targetFileName) && matchLength < len(pFileName) {
			if pFileName[matchLength] == targetFileName[matchLength] {
				matchLength++
			} else {
				break
			}
		}

		if matchLength > longestMatchLength {
			longestMatchLength = matchLength
			longestMatchOffset = i
			numberOfMatches = 1
		} else if matchLength == longestMatchLength {
			numberOfMatches++
		}
	}

	if numberOfMatches == 1 {
		return potential[longestMatchOffset]
	}

	return *new(T)
}
