package reset

import (
	"crypto/rand"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/yubikey"
	"io"
	"math/big"
	"os"

	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
)

var (
	Cmd = &cobra.Command{
		Use:   "reset",
		Short: "reset the PIV part of a Yubikey",
		Long: `Reset resets the YubiKey PIV applet to its factory settings, wiping all
slots. This does not affect data on other applets, such as GPG or U2F. If
--leave-on-defaults the Yubikey is left with the PIN, PUK, and management
key set to the factory defaults. If this switch is not given then a new random
PIN, PUK, and management key are generated and the PIN and PUK will be printed
on standard output. To set up a Yubikey with a custom PIN and random PUK that
is not recorded anywhere, use the switches -f -n . -q`,
		RunE: resetCmd,
	}

	pinOpt             ykpiv.PinFlag
	pukOpt             cmd.PromptFlag
	leaveOnDefaultsOpt bool
	forceOpt           bool
	quiet              bool
)

func init() {
	ykpiv.Cmd.AddCommand(Cmd)

	flags := Cmd.Flags()
	flags.BoolVarP(&leaveOnDefaultsOpt, "leave-on-defaults", "", false, "leave the Yubikey with factory defaults and don't set a new PIN, PUK and management key")
	pinOpt.Init(flags, "@")
	pukOpt.Init(flags, "puk", "k", "@", "Yubikey PUK")
	flags.BoolVarP(&forceOpt, "force", "f", false, "actually reset the Yubikey")
	flags.BoolVarP(&quiet, "quiet", "q", false, "no dot print any output or the generated PIN/PUK")
}

func resetCmd(cmd *cobra.Command, args []string) error {
	if !forceOpt {
		return fmt.Errorf("use -f to reset, -h for help")
	}

	if quiet {
		os.Stdout = nil
	}

	yk, err := yubikey.GetCardByName("")
	if err != nil {
		return err
	}

	var newPin, newPuk string
	if leaveOnDefaultsOpt {
		newPin = piv.DefaultPIN
		newPuk = piv.DefaultPUK
		fmt.Printf("New pin: %s.\nNew puk: %s.\n", newPin, newPuk)
	} else {
		newPin, err = getPin(*pinOpt.PromptFlag(), "pin", 6)
		if err != nil {
			return err
		}

		newPuk, err = getPin(pukOpt, "puk", 8)
		if err != nil {
			return err
		}
	}

	err = yk.Reset()
	if err != nil {
		return err
	}
	fmt.Printf("Yubikey reset.\n")

	if !leaveOnDefaultsOpt {
		err = setNewKeys(yk, newPin, newPuk)
		if err != nil {
			return err
		}
		fmt.Printf("PIN and PUK set.\n")
	}

	return nil
}

func setNewKeys(yk *piv.YubiKey, pin, puk string) error {
	var newKey [24]byte
	if _, err := io.ReadFull(rand.Reader, newKey[:]); err != nil {
		return err
	}

	// Set all values to a new value.
	if err := yk.SetManagementKey(piv.DefaultManagementKey, newKey); err != nil {
		return err
	}
	if err := yk.SetPUK(piv.DefaultPUK, puk); err != nil {
		return err
	}
	if err := yk.SetPIN(piv.DefaultPIN, pin); err != nil {
		return err
	}

	m := piv.Metadata{ManagementKey: &newKey}
	if err := yk.SetMetadata(newKey, &m); err != nil {
		return err
	}

	return nil
}

func getPin(p cmd.PromptFlag, name string, digits int) (pin string, err error) {
	if p.IsSet() {
		pin, err = askPin(p, name, digits)
	} else {
		pin, err = makePin(digits)
	}
	if err != nil {
		return "", err
	}

	if !p.IsSet() {
		fmt.Printf("New %s: %s.\n", name, pin)
	}

	return pin, nil
}

func askPin(p cmd.PromptFlag, name string, digits int) (string, error) {
	pin, err := p.Prompt().Prompt(fmt.Sprintf("enter %d digit %s", digits, name), true)
	if err != nil {
		return "", fmt.Errorf("getting %s: %w", name, err)
	}

	if len(pin) != digits {
		return "", fmt.Errorf("%s is not %d digits long", name, digits)
	}

	for _, d := range pin {
		if d < '0' || d > '9' {
			return "", errors.New("%s contains digits other than 0 1 2 3 4 5 6 7 8 and 9")
		}
	}

	return string(pin), nil
}

func makePin(digits int) (string, error) {
	maxRand := new(big.Int).Exp(big.NewInt(10), big.NewInt(int64(digits)), nil)
	newPINInt, err := rand.Int(rand.Reader, maxRand)
	if err != nil {
		return "", err
	}
	newPinStr := newPINInt.String()
	for len(newPinStr) < digits {
		newPinStr = "0" + newPinStr
	}
	return newPinStr, nil
}
