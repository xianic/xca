package siglog

import (
	"crypto"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
)

const (
	PemTypePrivateKey    = "PRIVATE KEY"
	PemTypeECPrivateKey  = "EC PRIVATE KEY"
	PemTypeRsaPrivateKey = "RSA PRIVATE KEY"
	PemTypePublicKey     = "PUBLIC KEY"
	PemTypeCsr           = "CERTIFICATE REQUEST"
)

func DeferClose(c func() error, e *error) {
	err := c()
	if e != nil && *e == nil {
		*e = err
	}
}

func GetAuditKey(path string) (key crypto.PrivateKey, err error) {
	key, err = initKey(path)
	if key != nil || err != nil {
		return
	}

	key, err = loadKey(path)
	return
}

func initKey(path string) (key crypto.PrivateKey, err error) {
	defer func() {
		if err != nil {
			_ = os.Remove(path)
		}
	}()

	var file *os.File
	file, err = os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
	if file != nil {
		defer DeferClose(file.Close, &err)
	}
	if err != nil {
		if errors.Is(err, os.ErrExist) {
			return nil, nil
		}
		return nil, err
	}

	_, key, err = ed25519.GenerateKey(rand.Reader)
	if err != nil {
		key = nil
		return
	}

	der, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		key = nil
		return
	}

	_, err = file.Write(
		pem.EncodeToMemory(&pem.Block{
			Type:  PemTypePrivateKey,
			Bytes: der,
		}),
	)
	if err != nil {
		key = nil
		return
	}

	return
}

func loadKey(path string) (crypto.PrivateKey, error) {
	file, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(file)
	if block == nil {
		return nil, fmt.Errorf("no PEM block found in key file: %s", path)
	}

	switch block.Type {
	case PemTypePrivateKey:
		fallthrough
	case PemTypeECPrivateKey:
		fallthrough
	case PemTypeRsaPrivateKey:
		break
	default:
		return nil, fmt.Errorf("expecting private key PEM block but got %q", block.Type)
	}

	key, err := UnmarshalPrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return key, err
}

func UnmarshalPrivateKey(data []byte) (crypto.PrivateKey, error) {
	var key crypto.PrivateKey
	var errPKCS1, errPKCS8, errEC error

	key, errPKCS1 = x509.ParsePKCS1PrivateKey(data)
	if errPKCS1 == nil {
		return key, nil
	}

	key, errPKCS8 = x509.ParsePKCS8PrivateKey(data)
	if errPKCS8 == nil {
		return key, nil
	}

	key, errEC = x509.ParseECPrivateKey(data)
	if errEC == nil {
		return key, nil
	}

	return nil, fmt.Errorf(
		"could not parese private key as PKCS1 [%v], as PKCS8 [%v], or as EC [%v]",
		errPKCS1,
		errPKCS8,
		errEC,
	)
}
