package key

import (
	"errors"
	"fmt"
	"github.com/go-piv/piv-go/piv"
	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/ykpiv"
	"gitlab.com/xianic/xca/cmd/ykpiv/gen"
)

var (
	Cmd = &cobra.Command{
		Use:   "key",
		Short: "generate a key on a Yubikey",
		RunE:  genKeyCmd,
	}

	forceFlag      ykpiv.ForceFlag
	keyGenFlags    ykpiv.KeyGenFlags
	selectionFlags ykpiv.SelectionFlags
	pinFlag        ykpiv.PinFlag
)

func init() {
	gen.Cmd.AddCommand(Cmd)

	fs := Cmd.Flags()
	forceFlag.Init(fs, "allow overwrite of existing key and certificate")
	keyGenFlags.Init(fs)
	selectionFlags.Init(fs)
	pinFlag.Init(fs, ".")
}

func genKeyCmd(cmd *cobra.Command, args []string) error {
	yk, slot, err := selectionFlags.GetYubikey()
	if err != nil {
		return err
	}

	key, err := keyGenFlags.PivKey()
	if err != nil {
		return err
	}

	_, err = yk.Attest(slot)
	if err == nil {
		if !forceFlag {
			return fmt.Errorf("refusing to overwrite existing key, use -f to force")
		}
	} else if !errors.Is(err, piv.ErrNotFound) {
		return err
	}

	if !forceFlag {
		_, err = yk.Certificate(slot)
		if !errors.Is(err, piv.ErrNotFound) {
			return fmt.Errorf("refusing to replace key in a slot with a certificate, use -f to force")
		}
	}

	mk, _, err := pinFlag.Unlock(yk)
	if err != nil {
		return err
	}

	_, err = yk.GenerateKey(mk, slot, *key)
	if err != nil {
		return err
	}

	return nil
}
