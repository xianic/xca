===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		3FBDE39C7E96034BF1E4457BD475E58509E4DDC4F711D2766119D4E5C02ED824 (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		CB2D40EE91B40C99
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	D35F29FBCF9B9F537DA5E7C96E7AD73B3F29518CDD0432B354C17D87CDE13223
Version: 		3
Issuer:
	/CN=www.example.com
	Common name:	www.example.com
Subject:
	/CN=www.example.com
	Common name:	www.example.com
SANS:	(2 total)
	dns:		www.example.com
	dns:		example.com
Validity:
	Not before:	2023-09-02T19:57:03Z (5 minutes ago)
	Not after:	2033-08-30T20:02:03Z (10 years from now)
	Is CA cert:	false
	MaxPathLen:	unset
Key usage:		DigitalSignature, KeyEncipherment
Extended key usage:	ServerAuth, ClientAuth
Name constraints:	0 non-critical
