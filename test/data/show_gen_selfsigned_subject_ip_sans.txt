===== CERTIFICATE #1 standard input:[line numbers] =====
Fingerprint:		774F948AC9370B4D922B66EA5740F1E8D9F17D346F553F1710C51C72FD6D611F (SHA256 of DER encoding)
Signature algorithm:	ECDSAWithSHA512
Self signed:		true
Serial no.		B85E87C3E1010941
EC public key:
	Type:		P-256
	Size:		256 bits
	Fingerprint:	9E98A40D09DF79E458CA74289B4E854DE542EF27EA9143DEAA02978227D559C3
Version: 		3
Issuer:
	/CN=Example Certificate
	Common name:	Example Certificate
Subject:
	/CN=Example Certificate
	Common name:	Example Certificate
SANS:	(2 total)
	ip:		198.51.100.1
	ip:		2001:db8:0:1::1
Validity:
	Not before:	2023-09-02T22:24:07Z (5 minutes ago)
	Not after:	2033-08-30T22:29:07Z (10 years from now)
	Is CA cert:	false
	MaxPathLen:	unset
Key usage:		DigitalSignature, KeyEncipherment
Extended key usage:	ServerAuth, ClientAuth
Name constraints:	0 non-critical
