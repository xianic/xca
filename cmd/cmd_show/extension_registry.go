package cmd_show

import (
	"crypto/x509/pkix"
	"encoding/asn1"
)

type ExtensionPrinter func(extension pkix.Extension)

var extensionRegistry map[string]ExtensionPrinter

func AddExtensionPrinter(oid asn1.ObjectIdentifier, p ExtensionPrinter) {
	if extensionRegistry == nil {
		extensionRegistry = make(map[string]ExtensionPrinter)
	}

	extensionRegistry[oid.String()] = p
}
