package util

import "fmt"

func DeferClose(c func() error, e *error) {
	err := c()

	// If the close func errored, and we have a non-nil pointer to a nil error
	if e != nil && *e == nil {
		// then set the error from the close fun to be returned
		*e = err
	}
}

func DeferWrap(e *error, format string, args ...interface{}) {
	if *e != nil {
		*e = fmt.Errorf(format, append(args, *e)...)
	}
}
