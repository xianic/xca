package cmd_ca

import (
	"context"
	flag "github.com/spf13/pflag"
	"gitlab.com/xianic/xca/prompt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/ca"
	"gitlab.com/xianic/xca/cmd"
	"gitlab.com/xianic/xca/siglog"
)

var (
	Cmd = &cobra.Command{
		Use:   "ca [command]",
		Short: "Certificate Authority commands",
	}

	OptDir         string
	OptOpenTimeout time.Duration
)

func init() {
	cmd.RootCmd.AddCommand(Cmd)

	fs := Cmd.PersistentFlags()
	fs.StringVarP(&OptDir, "ca-path", "C", ".", "path to CA directory or config file")
	fs.DurationVarP(&OptOpenTimeout, "ca-open-timeout", "", time.Duration(5)*time.Minute, "time to wait for the exclusive lock on the audit log")
}

func LoadCa(p prompt.PasswordPrompt, auditor siglog.MessageChecker) (*ca.CA, error) {
	ctx, cancel := context.WithTimeout(context.Background(), OptOpenTimeout)
	defer cancel()

	c, err := ca.LoadCA(ctx, OptDir, auditor, p)
	if err != nil {
		return nil, err
	}

	return c, nil
}

type CaPrompt struct {
	cmd.PromptFlag
}

func (c *CaPrompt) Init(fs *flag.FlagSet) {
	c.PromptFlag.Init(fs, "password", "p", ".", "passphrase or PIN for CA")
}

func (c *CaPrompt) Prompt() prompt.PasswordPrompt {
	return c.PromptFlag.Prompt()
}
