#!/bin/bash

XCA_TOOL=./xca-noykpiv
KEYTYPES="P-224 P-256 P-384 P-521 25519 RSA2048 RSA3072 RSA4096"

function show_compare() {
    diff -u \
        --label expected \
        --label actual \
        <(test/bin/homogenise $1) \
        <($XCA_TOOL show - | test/bin/homogenise)
    return $?
}

function setup_keys() {
    if [ -z "$BATS_FILE_TMPDIR" ]
    then
        export BATS_FILE_TMPDIR=$(mktemp -d)
        export BATS_FILE_TMPDIR_MANUAL=true
    fi

    declare -A KEYS_PLAIN

    $XCA_TOOL gen key "$BATS_FILE_TMPDIR/key_default.pem" &
    $XCA_TOOL gen key --argon2 0:0:0 -p test/data/password.txt "$BATS_FILE_TMPDIR/key_default_enc.pem" &

    for t in ${KEYTYPES[@]}
    do
        $XCA_TOOL gen key -t $t "$BATS_FILE_TMPDIR/key_$t.pem" &
    done

    wait
}

function teardown_keys() {
    rm -f ${KEYS_PLAIN[@]}
    if [ "$BATS_FILE_TMPDIR_MANUAL" == true ]
    then
        rm -rf -- "$BATS_FILE_TMPDIR"
    fi
}

function gen_validity() {
    now="$(date +%s)"
    read from to < <($XCA_TOOL gen selfsigned --subject hello "$@" - \
        | $XCA_TOOL show - \
        | grep -Eo '[0-9]{4}(-[0-9]{2}){2}T[0-9]{2}(:[0-9]{2}){2}Z' \
        | tr T ' ' | tr -d Z \
        |  while IFS= read -r line
        do
            date -u -d "$line" +%s
        done \
        | xargs)
    echo "$((now - from)) $((to - now))"
}
