package cmd_decode

import (
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/xianic/xca/cmd/cmd_pem"
	"gitlab.com/xianic/xca/util"
	"golang.org/x/term"
)

var (
	Cmd = &cobra.Command{
		Use:   "decode [flags] [file [file […]]]",
		Short: "Decode PEM to DER",
		Long: `Decode PEM block(s) and emit DER version

Read a PEM block and emit the decoded data. Usually the data is DER encoded so
this command can be used to convert PEM encoded certificates, keys, and other
objects to their DER version.
Without --multiple only the first PEM block found will be decoded. Files will
be read until a PEM block is found. With --multiple all PEM blocks in all
files will be decoded and the output concatenated`,
		Args: util.StdInOnce,
		RunE: decodeCmdFunc,
	}

	multipleFLag bool

	// this is not an error but used to terminate the search for PEM blocks
	foundBlock = errors.New("PEM block found")
)

func init() {
	cmd_pem.Cmd.AddCommand(Cmd)

	Cmd.Flags().BoolVarP(&multipleFLag, "multiple", "m", false, "decodeFile multiple PEM blocks")
}

func decodeCmdFunc(cms *cobra.Command, args []string) (err error) {
	if len(args) == 0 {
		err = decodeFile("-")
	} else {
		for _, arg := range args {
			err = decodeFile(arg)
			if err != nil {
				break
			}
		}
	}

	if err == foundBlock {
		err = nil
	}

	return nil
}

func decodeFile(arg string) (err error) {
	if arg == "-" {
		if term.IsTerminal(int(os.Stdin.Fd())) {
			_, _ = fmt.Fprintf(os.Stderr, "Enter PEM block(s) and end with Enter, Ctrl-D\n")
		}
		return decodeStream(os.Stdin)
	}

	var file *os.File
	file, err = os.Open(arg)
	if err != nil {
		return
	}
	defer util.DeferClose(file.Close, &err)

	return decodeStream(file)
}

func decodeStream(in io.Reader) error {
	//TODO: use a streaming parser so that the whole PEM block does not need to reside in memory at once
	data, err := io.ReadAll(in)
	if err != nil {
		return err
	}

	var block *pem.Block
	for {
		block, data = pem.Decode(data)
		if block == nil {
			return nil
		}

		_, err := os.Stdout.Write(block.Bytes)
		if err != nil {
			return err
		}

		if !multipleFLag {
			return foundBlock
		}
	}
}
