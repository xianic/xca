package ca

import (
	"bufio"
	"bytes"
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/xianic/xca/cryptutil"
	"strings"
	"time"
)

/*
CertTrace is just enough info about a certificate to tell if a CA really did
issue it and when, and to generate a CRL.
*/
type CertTrace struct {
	Description string    `json:"description"`
	NotAfter    time.Time `json:"not_after"`
	Serial      []byte    `json:"serial"`
	Sha512_256  []byte    `json:"sha512_256"`
}

func MakeTrace(cert *x509.Certificate) *CertTrace {
	sum := sha512.Sum512_256(cert.Raw)
	return &CertTrace{
		Description: cryptutil.DescribeCertificate(cert),
		NotAfter:    cert.NotAfter,
		Serial:      cert.SerialNumber.Bytes(),
		Sha512_256:  sum[:],
	}
}

func (ct *CertTrace) String() string {
	return fmt.Sprintf(
		"%X %s %s %s",
		ct.Serial,
		ct.NotAfter.Format(time.RFC3339),
		base64.StdEncoding.EncodeToString(ct.Sha512_256),
		ct.Description,
	)
}

func (ct *CertTrace) Equal(ot *CertTrace) bool {
	return bytes.Equal(ct.Serial, ot.Serial) &&
		ct.NotAfter == ot.NotAfter &&
		bytes.Equal(ct.Sha512_256, ot.Sha512_256) &&
		ct.Description == ot.Description
}

func ParseTrace(line string) (*CertTrace, error) {
	localSplitFunc := bufio.ScanWords

	r := strings.NewReader(line)
	scanner := bufio.NewScanner(r)
	scanner.Split(func(data []byte, atEOF bool) (int, []byte, error) {
		return localSplitFunc(data, atEOF)
	})
	trace := new(CertTrace)
	for i := 0; i <= 3; i++ {
		var err error
		if scanner.Scan() {
			switch i {
			case 0:
				trace.Serial, err = hex.DecodeString(scanner.Text())
			case 1:
				trace.NotAfter, err = time.Parse(time.RFC3339, scanner.Text())
			case 2:
				trace.Sha512_256, err = base64.StdEncoding.DecodeString(scanner.Text())
				localSplitFunc = bufio.ScanLines
			case 3:
				trace.Description = scanner.Text()
			}
		} else {
			err = errors.New("unnexpected end of line")
		}
		if err != nil {
			return nil, fmt.Errorf("parsing certificate trace in field %d: %w", i, err)
		}
	}

	return trace, nil
}

func ParseTraces(lines []byte) ([]*CertTrace, error) {
	scanner := bufio.NewScanner(bytes.NewBuffer(lines))
	scanner.Split(bufio.ScanLines)
	traces := make([]*CertTrace, 0)
	for scanner.Scan() {
		trace, err := ParseTrace(scanner.Text())
		if err != nil {
			return nil, err
		}
		traces = append(traces, trace)
	}

	return traces, nil
}
