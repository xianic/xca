package cryptutil

import (
	"encoding/pem"

	"gitlab.com/xianic/xca/pemcrypt"
	"gitlab.com/xianic/xca/util"
)

const (
	PemTypePrivateKey    = "PRIVATE KEY"
	PemTypeECPrivateKey  = "EC PRIVATE KEY"
	PemTypeRsaPrivateKey = "RSA PRIVATE KEY"
	PemTypePublicKey     = "PUBLIC KEY"
	PemTypeECPublicKey   = "EC PUBLIC KEY"
	PemTypeRsaPublicKey  = "RSA PUBLIC KEY"
	PemTypeCsr           = "CERTIFICATE REQUEST"
	PemTypeCertificate   = "CERTIFICATE"
)

var (
	PemTypesPrivateKeys = []string{
		PemTypePrivateKey, PemTypeECPrivateKey, PemTypeRsaPrivateKey,
	}
	PemTypesPublicKeys = []string{
		PemTypePublicKey, PemTypeECPublicKey, PemTypeRsaPublicKey,
	}
	PemTypesPublicKeyExtractable = append(
		append(
			PemTypesPublicKeys,
			PemTypesPrivateKeys...,
		),
		PemTypeCertificate,
		PemTypeCsr,
	)
)

// PemParser is an object that can source a series of PEM blocks
type PemParser interface {
	// NextPem returns a PEM block or and error while attempting to retrieve
	// a PEM block. When no more blocks are available nil should be returned
	// for both the block and the error.
	NextPem() (*pem.Block, error)
}

// UnmarshalFile is a utility function to use a delayed open file reader and
// a PemBuffer to read and optionally decrypt some PEM blocks. Errors are
// delayed until a call the unmarshaler.
func UnmarshalFile(filePath, stdInPrompt string, cryptOptions *pemcrypt.CryptOptions) *PemUnmarshaler {
	po := make([]PemUnmarshalerOption, 0, 2)
	do := make([]util.DelayOpenReadOption, 0, 1)

	if stdInPrompt != "" {
		do = append(do, util.AllowStdInPrompt(stdInPrompt))
		if filePath == "-" {
			po = append(po, WithUserContext("standard input"))
		} else {
			po = append(po, WithUserContext(filePath))
		}
	} else {
		po = append(po, WithUserContext(filePath))
	}

	if cryptOptions != nil {
		po = append(po, WithDecryption(cryptOptions))
	}

	return NewPemUnmarshaler(NewPemBufferFromStream(util.DelayOpenRead(filePath, do...)), po...)
}
