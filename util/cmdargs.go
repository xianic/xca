package util

import (
	"errors"

	"github.com/spf13/cobra"
)

func CombineCmdArgsFuncs(funcs ...func(cmd *cobra.Command, args []string) error) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		for _, f := range funcs {
			err := f(cmd, args)
			if err != nil {
				return err
			}
		}

		return nil
	}
}

func StdInOnce(cmd *cobra.Command, args []string) error {
	stdInUsed := false
	for _, arg := range args {
		if arg == "-" {
			if stdInUsed {
				return errors.New("standard input can only be used as an argument once")
			} else {
				stdInUsed = true
			}
		}
	}

	return nil
}
